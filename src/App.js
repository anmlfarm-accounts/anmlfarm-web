import React from 'react';
import './App.css';
import EntryPage from './pages/entry-page/entry-page.component';
import { Route, Switch, Redirect } from 'react-router-dom'
import Onboarding from './page-collections/onboarding/onboarding.component';
import LibraryPage from './pages/library-page/library-page.component';
import SignInAndSignUpPage from './pages/sign-in-and-sign-up/sign-in-and-sign-up.component';
import {connect} from 'react-redux';
import AnimalPage from './pages/animal-page/animal-page.component';
import FarmPage from './pages/farm-page/farm-page.component';
import CampPage from './pages/camp-page/camp-page.component';
import { checkUserSession } from './redux/users/user.actions';
import { createStructuredSelector } from 'reselect';
import { selectCurrentUser } from './redux/users/user.selectors';
import HomePage from './pages/home-page/home-page.component';
import ProfilePage from './pages/profile-page/profile-page.component';
import ProductPage from './pages/product-page/product-page.component';
import ToolsPage from './pages/tools-page/tools-page.component';


class App extends React.Component{

  componentDidMount(){
    const { checkUserSession } = this.props;
    checkUserSession();
  }

  render(){
    const { currentUser } = this.props;
    return (
      <div className='app'>
        <Switch>
          <Route 
            path='/signin' 
            render={() => 
              (currentUser == null ? 
              <SignInAndSignUpPage /> : 
              <Redirect to='/' />)} 
          />
          <Route path='/products' component={ProductPage} />
          <Route path='/tools' component={ToolsPage} />
          <Route path='/profile' component={ProfilePage} />
          <Route path='/library' component={LibraryPage} />
          <Route path='/farms' component={FarmPage}/>
          <Route path='/camps' component={CampPage}/>
          <Route path='/animals' component={AnimalPage}/>
          <Route path='/onboarding' component={Onboarding} />
          <Route path='/home' component={HomePage}/>
          <Route path='/' component={EntryPage} />
        </Switch>
      </div>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  currentUser: selectCurrentUser
})

const mapDispacthToProps = dispatch => ({
  checkUserSession: () => dispatch(checkUserSession())
})

export default connect(mapStateToProps, mapDispacthToProps)(App);
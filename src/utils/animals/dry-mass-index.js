import { convertObjectToArray } from "../array/mapper";

export const calculateDryMassIndex = (weight) => {
    if(!weight) return 0.0;
    else {
        let convertedWeight = parseInt(weight)
        return round((Math.pow(convertedWeight, 0.75)) )* 0.1;
    }
}

export const calculateAnimalDryMassIndex = (animal) => {
    if(!animal?.weightData) return 0.0; //update with breed average
    let allWeightAsArray = convertObjectToArray({...animal?.weightData});
    allWeightAsArray.sort((a,b) => a.id < b.id ? 1 : -1);
    const currentWeight = allWeightAsArray[allWeightAsArray.length-1];
    let convertedWeight = parseInt(currentWeight?.weight)
    return round((Math.pow(convertedWeight, 0.75)) )* 0.1;
}

export const calculateCombinedAnimalDryMassIndex = (animals) => {
    let totalWeight = 0;
    if(!animals || animals?.length===0) return 0.0; 
    animals.forEach(animal => {
        totalWeight += calculateAnimalDryMassIndex(animal);
    });
    return round(totalWeight);
}

function round(num, decimalPlaces = 2) {
    num = Math.round(num + "e" + decimalPlaces);
    return Number(num + "e" + -decimalPlaces);
}
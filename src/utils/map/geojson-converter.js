export const convertDeviceDataToGeoJson = (deviceEventData) => {
    let muatbleData = [...deviceEventData];
    const geodata = 
    {
        type: "FeatureCollection",
        features: []
    }
    if(deviceEventData) {
        muatbleData = muatbleData.sort((a,b)=> a.id > b.id ? 1 : -1);
        muatbleData = muatbleData.sort((a,b)=> Date.parse(b.dateUTC) - Date.parse(a.dateUTC));
        muatbleData.forEach(event => {
            if(event.dateUTC)
            geodata.features.push({
                type: "Feature",
                properties : {
                    id: `${event.id}`,
                    time: Date.parse(event.dateUTC),
                    date: Date.parse(event.dateUTC)
                },
                geometry: {
                    type: "Point",
                    coordinates: [
                        event.geolocation.longitude,
                        event.geolocation.latitude
                        // event.geolocation.altitude
                    ]
                }
                
            });
        });
    }
    
    geodata.features = geodata.features.sort((a,b)=>b.properties.time - a.properties.time);

    return geodata;
}
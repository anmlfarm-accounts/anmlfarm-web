export const calculateOrderTotalItemPrice = (order) => {
    if(!order || !order.items) return 0;
    let price = order.items.reduce((a,c)=>(a+(c.quantity*c.price)),0)
    price += order.delivery?.cost ?? 0;
    return price;
}

export const calculateOrderTotalSubscriptionPrice = (order) => {
    if(!order || !order.subscriptions) return 0;
    return order.subscriptions.reduce((a,c)=>(a+(300)),0)
}

export const getOrderSubscriptionFirstBilling = (order) => {
    if(!order || !order.subscriptions) return 0;
    return order.subscriptions.reduce((a,c)=>((new Date(c?.trialEndDate?.seconds*1000) > a ? new Date(c?.trialEndDate?.seconds*1000) : a )), new Date())
}
import { createSelector } from 'reselect';
import { convertObjectToArray } from '../../utils/array/mapper';
import { selectCurrentSelectedAnimal } from '../animals/animal.selectors';
import { selectCurrentFarm } from '../farms/farm.selectors';

export const selectDocumentState = (state) => state.documents;

export const selectDocuments = createSelector(
  [selectDocumentState],
  (docs) => convertObjectToArray(docs.documents),
);

export const selectAllDocumentsAsArray = createSelector(
  [selectDocuments],
  (docs) => convertObjectToArray(docs),
);

export const selectUploadingDocumentsAsArray = createSelector(
  [selectDocuments],
  (docs) => convertObjectToArray(docs.uploadingDocuments),
);

  
export const selectCurrentAnimalDocuments = createSelector(
  [selectCurrentSelectedAnimal, selectAllDocumentsAsArray],
  (animal, docs) => docs.filter((d) => d.animalIds?.includes(animal?.id??animal?.animalId)),
);

export const selectCurrentFarmDocuments = createSelector(
  [selectCurrentFarm, selectAllDocumentsAsArray],
  (farm, docs) => docs.filter((d) => d.farmIds?.includes(farm?.id??farm?.farmId)),
);

export const selectUploadingAnimalDocuments = createSelector(
  [selectCurrentSelectedAnimal, selectUploadingDocumentsAsArray],
  (animal, docs) => docs.filter((d) => d.animalIds?.includes(animal?.id??animal?.animalId)),
);

export const selectUploadingFarmDocuments = createSelector(
  [selectCurrentFarm, selectUploadingDocumentsAsArray],
  (farm, docs) => docs.filter((d) => d.farmIds?.includes(farm?.id??farm?.farmId)),
);

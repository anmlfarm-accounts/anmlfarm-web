import { all, call, put, takeEvery, takeLatest } from "redux-saga/effects";
import { firestore } from "../../firebase/firebase.utils";
import documentActionTypes from "./document.action-types";
import { documentFetchFailed, documentFetchSuccess, documentUploadFailed, documentUploadSuccess } from "./document.actions";
import { convertCollectionsSnapshotToMap, uploadFileToStorage } from "../../firebase/firebase.functions";
import farmActionTypes from "../farms/farm.action-types";
import animalActionTypes from "../animals/animal.action-types";
import UserActionTypes from "../users/user.types";

export function* createNewDocumentAsync(action) {
    const data = action.payload.data;
    let fileInfo = {...action.payload};
    delete fileInfo.data;
    try{
        const collectionRef = firestore.collection('documents');
        const addedDocumentRef = yield collectionRef.add(fileInfo);
        if(data){
            try {
                let mimeType = fileInfo.type ?? data.match(/[^:]\w+\/[\w-+\d.]+(?=;|,)/)[0];
                let fileType = fileInfo.path?.split('.')?.pop() ?? data.match(/[^:/]\w+(?=;|,)/)[0];
                if(!mimeType || !fileType) return;
                
                let documentUploadRef = yield uploadFileToStorage(data, `${mimeType}/${addedDocumentRef.id}.${fileType}`);
                let documentURL = yield documentUploadRef.ref.getDownloadURL();
                yield addedDocumentRef.update({linkUrl: documentURL, preview: documentURL});
                fileInfo.linkUrl = documentURL;
                fileInfo.preview = documentURL;
            } catch (error) {
                yield addedDocumentRef.update({data});
            }
        }
        yield put(documentUploadSuccess({documentId: addedDocumentRef.id, ...fileInfo}));
    } catch(error) {
        yield put(documentUploadFailed(error));
    }
}

export function* fetchFarmDocuments(action){
    const farmId = action.payload;
    if(!farmId) return;
    try{
        const collectionRef = firestore.collection('documents').where('farmIds', "array-contains", farmId);
        const snapshot = yield collectionRef.get();
        const collectionsMap = yield call(convertCollectionsSnapshotToMap, snapshot);
        yield put(documentFetchSuccess(collectionsMap))
    } catch(error) {
        yield put(documentFetchFailed(error.message))
    }
}


export function* fetchAnimalDocuments(action){
    const animalId = action.payload;
    if(!animalId) return;
    try{
        const collectionRef = firestore.collection('documents').where('animalIds', "array-contains", animalId);
        const snapshot = yield collectionRef.get();
        const collectionsMap = yield call(convertCollectionsSnapshotToMap, snapshot);
        yield put(documentFetchSuccess(collectionsMap))
    } catch(error) {
        yield put(documentFetchFailed(error.message))
    }
}

export function* fetchUserDocuments(action){
    const user = action.payload;
    const userId = user.id ?? user.uid;
    if(!userId) return;
    try{
        const collectionRef = firestore.collection('documents').where('userIds', "array-contains", userId);
        const snapshot = yield collectionRef.get();
        const collectionsMap = yield call(convertCollectionsSnapshotToMap, snapshot);
        yield put(documentFetchSuccess(collectionsMap))
    } catch(error) {
        yield put(documentFetchFailed(error.message))
    }
}

export function* onDocumentUploadStart() {
    yield takeEvery(
        documentActionTypes.START_DOCUMENT_UPLOAD, 
        createNewDocumentAsync
    );
}

export function* onFarmSelect() {
    yield takeLatest(
        farmActionTypes.SELECT_FARM, 
        fetchFarmDocuments
    );
}

export function* onAnimalSelect() {
    yield takeLatest(
        animalActionTypes.SET_SELECTED_ANIMAL, 
        fetchAnimalDocuments
    );
}

export function* onUserSelect() {
    yield takeLatest(
        UserActionTypes.SIGN_IN_SUCCESS, 
        fetchUserDocuments
    );
}

export function* documentSagas(){
    yield all([
        call(onDocumentUploadStart),
        call(onFarmSelect),
        call(onAnimalSelect),
        call(onUserSelect)
    ])
}
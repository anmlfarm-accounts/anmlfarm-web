import documentActionTypes from './document.action-types'

export const startDocumentUpload = documentInfo => ({
    type: documentActionTypes.START_DOCUMENT_UPLOAD,
    payload: documentInfo
})

export const documentUploadSuccess = documentInfo => ({
    type: documentActionTypes.DOCUMENT_UPLOAD_SUCCESS,
    payload: documentInfo
})

export const documentUploadFailed = error => ({
    type: documentActionTypes.DOCUMENT_UPLOAD_FAILED,
    payload: error
})

export const startDocumentFetch = documentId => ({
    type: documentActionTypes.START_DOCUMENT_FETCH,
    payload: documentId
})

export const documentFetchSuccess = documentInfo => ({
    type: documentActionTypes.DOCUMENT_FETCH_SUCCESS,
    payload: documentInfo
})

export const documentFetchFailed = error => ({
    type: documentActionTypes.DOCUMENT_FETCH_FAILED,
    payload: error
})
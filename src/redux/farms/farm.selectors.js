import { createSelector } from 'reselect';
import { convertObjectToArray } from '../../utils/array/mapper';
import { selectCurrentUser } from '../users/user.selectors';

export const selectFarmCollection = (state) => state.farms;

export const selectVisibleFarms = createSelector(
  [selectFarmCollection],
  (farms) => farms.visibleFarms,
);

export const selectVisibleFarmsAsArray = createSelector(
  [selectVisibleFarms],
  (allFarms) => convertObjectToArray(allFarms).filter((f) => !f.deleted),
);

export const selectCurrentUserOwnedFarmsAsArray = createSelector(
  [selectVisibleFarms, selectCurrentUser],
  (allFarms, user) =>
    convertObjectToArray(allFarms).filter(
      (f) => f.creatingUserId === user?.id ?? user?.uid,
    ),
);

export const selectUserOwnedFarmsAsArray = (user) =>
  createSelector([selectVisibleFarms], (allFarms) =>
    convertObjectToArray(allFarms).filter(
      (f) => f.creatingUserId === user?.id ?? user?.uid,
    ),
  );

export const selectCurrentUserManagingFarmsAsArray = createSelector(
  [selectVisibleFarms, selectCurrentUser],
  (allFarms, user) =>
    convertObjectToArray(allFarms).filter((f) =>
      f.managingUsers?.includes(user?.id ?? user?.uid),
    ),
);

export const selectManagingFarmsAsArray = (user) =>
  createSelector([selectVisibleFarms], (allFarms) =>
    convertObjectToArray(allFarms).filter((f) =>
      f.managingUsers?.includes(user?.id ?? user?.uid),
    ),
  );

export const selectCurrentUserWorkingFarmsAsArray = createSelector(
  [selectVisibleFarms, selectCurrentUser],
  (allFarms, user) =>
    convertObjectToArray(allFarms).filter((f) =>
      f.workingUsers?.includes(user?.id ?? user?.uid),
    ),
);

export const selectWorkingFarmsAsArray = (user) =>
  createSelector([selectVisibleFarms], (allFarms) =>
    convertObjectToArray(allFarms).filter((f) =>
      f.workingUsers?.includes(user?.id ?? user?.uid),
    ),
  );

export const selectCurrentUserFollowingFarmsAsArray = createSelector(
  [selectVisibleFarms, selectCurrentUser],
  (allFarms, user) =>
    convertObjectToArray(allFarms).filter((f) =>
      f.followingUsers?.includes(user?.id ?? user?.uid),
    ),
);

export const selectFollowingFarmsAsArray = (user) =>
  createSelector([selectVisibleFarms], (allFarms) =>
    convertObjectToArray(allFarms).filter((f) =>
      f.followingUsers?.includes(user?.id ?? user?.uid),
    ),
  );

export const selectCurrentUserInvitedFarmsAsArray = createSelector(
  [selectVisibleFarms, selectCurrentUser],
  (allFarms, user) =>
    convertObjectToArray(allFarms).filter((f) =>
      f.invitedUsers?.includes(user?.id ?? user?.uid),
    ),
);

export const selectInvitedFarmsAsArray = (user) =>
  createSelector([selectVisibleFarms], (allFarms) =>
    convertObjectToArray(allFarms).filter((f) =>
      f.invitedUsers?.includes(user?.id ?? user?.uid),
    ),
  );

export const selectCurrentFarm = createSelector(
  [selectFarmCollection],
  (farms) => farms?.visibleFarms[farms.selectedFarmId],
);

export const selectCurrentSelectedInvitedWorkersArray = createSelector(
  [selectCurrentFarm],
  (farm) => {
    let returnWorker = farm?.invitedUsers ? farm?.invitedUsers.reverse() : [];
    return returnWorker;
  },
);

export const selectCurrentSelectedFarmWorkersArray = createSelector(
  [selectCurrentFarm],
  (farm) => {
    let returnWorker = farm?.workingUsers ? farm?.workingUsers?.reverse() : [];
    return returnWorker;
  },
);

export const selectUserCanModifyFarm = createSelector(
  [selectCurrentUser, selectCurrentFarm],
  (user, farm) => {
    if (!user || !farm) return false;
    if (
      farm.managingUsers?.includes(user.id ?? user.uid) ||
      farm.creatingUserId === (user.id ?? user.uid)
    )
      return true;
    else return false;
  },
);

export const selectUserCanEditFarm = createSelector(
  [selectCurrentUser, selectCurrentFarm],
  (user, farm) => {
    if (!user || !farm) return false;
    if (farm.creatingUserId === (user.id ?? user.uid)) return true;
    else return false;
  },
);

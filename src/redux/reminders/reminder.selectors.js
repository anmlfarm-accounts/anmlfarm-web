import { createSelector } from 'reselect';
import {
  addDaysToStringDate,
  calculateCurrentAge,
  calculateCurrentAgeInDays
} from '../../utils/date/calculations';
import { selectCurrentCamp } from '../camps/camp.selectors';
import { selectCurrentSelectedAnimal } from '../animals/animal.selectors';
import { convertObjectToArray } from '../../utils/array/mapper';

export const reminderSelector = (state) => state.reminders;

export const farmRemindersSelector = createSelector(
  [reminderSelector],
  (reminderDetails) => reminderDetails.visibleReminders,
);

export const selectCurrentSelectedReminder = createSelector(
  [farmRemindersSelector, reminderSelector],
  (reminders, reminderDetails) => reminders[reminderDetails.selectedReminderId],
);

export const selectCurrentSelectedReminderAge = createSelector(
  [selectCurrentSelectedReminder],
  (reminder) => calculateCurrentAge(reminder.reminderBirthday),
);

export const selectAllRemindersAsArray = createSelector(
  [farmRemindersSelector],
  (reminders) => convertObjectToArray(reminders),
);

export const selectCurrentCampReminders = createSelector(
  [selectAllRemindersAsArray, selectCurrentCamp],
  (reminders, camp) =>
    reminders.filter((reminder) => reminder.campId === (camp?.id ?? camp?.campId)),
);

export const selectCurrentCampRemindersAsArray = createSelector(
  [selectCurrentCampReminders],
  (reminders) => convertObjectToArray(reminders),
);

export const selectCurrentActiveCampRemindersAsArray = createSelector(
  [selectCurrentCampRemindersAsArray],
  (reminders) => reminders.filter((a) => !a?.deleted),
);

export const selectAnimalReminders = createSelector(
  [selectAllRemindersAsArray, selectCurrentSelectedAnimal],
  (reminders, animal) => reminders.filter(r=>r.animalIds?.includes(animal?.id ?? animal?.animalId)),
);

export const generateAnimalWeightReminder = createSelector(
  [selectCurrentSelectedAnimal],
  (animal) => {
    const daysAlive = calculateCurrentAgeInDays(animal?.animalBirthday ?? animal?.animalTeeth * 180);
    const startDateTime = animal?.animalBirthday ? new Date(animal?.animalBirthday) : new Date();
    let reminder = {
          title:  'Weight Reminder',
          startDate: startDateTime.toISOString().substr(0,10),
          startTime: (new Date()).toTimeString().substr(0,5),
          isOnce: false,
          isRecurring: true,
          isDaily:false,
          isWeekly:false,
          isMonthly:true,
          isYearly:false,
          isCustom: false,
          animalIds: [animal.id??animal.animalId]
        }
    if(daysAlive < 100) {
      reminder.startDate = addDaysToStringDate(reminder.startDate, 100);
      reminder.title = "100 Days Weight Reminder"
      reminder.isOnce = true;
      reminder.isMonthly = false;
      reminder.isRecurring = false;
    } else if(daysAlive < 250) {
      reminder.startDate = addDaysToStringDate(reminder.startDate, 250);
      reminder.title = "250 Days Weight Reminder"
      reminder.isOnce = true;
      reminder.isMonthly = false;
      reminder.isRecurring = false;
    } else if(daysAlive < 365) {
      reminder.startDate = addDaysToStringDate(reminder.startDate, 365);
      reminder.title = "365 Days Weight Reminder"
      reminder.isOnce = true;
      reminder.isMonthly = false;
      reminder.isRecurring = false;
    };
    return reminder;
  }
);

export const generateAnimalHeatReminder = createSelector(
  [selectCurrentSelectedAnimal],
  (animal) => {
    switch(animal?.animalType?.toLowerCase()){
      case 'sheep':
        return {
          title: 'Sheep Heat Cycle Reminder',
          startDate: (new Date()).toISOString().substr(0,10),
          startTime: (new Date()).toTimeString().substr(0,5),
          isOnce: false,
          isRecurring: true,
          isDaily:false,
          isWeekly:false,
          isMonthly:false,
          isYearly:false,
          isCustom: true,
          customDays: 17
        }

      case 'skaap':
        return {
          title: 'Sheep Heat Cycle Reminder',
          startDate: (new Date()).toISOString().substr(0,10),
          startTime: (new Date()).toTimeString().substr(0,5),
          isOnce: false,
          isRecurring: true,
          isDaily:false,
          isWeekly:false,
          isMonthly:false,
          isCustom: true,
          customDays: 17
        }

      case 'pig':
        return {
          title: 'Pig Heat Cycle Reminder',
          startDate: (new Date()).toISOString().substr(0,10),
          startTime: (new Date()).toTimeString().substr(0,5),
          isOnce: false,
          isRecurring: true,
          isDaily:false,
          isWeekly:false,
          isMonthly:false,
          isCustom: true,
          customDays: 21
        }

      case 'vark':
        return {
          title: 'Pig Heat Cycle Reminder',
          startDate: (new Date()).toISOString().substr(0,10),
          startTime: (new Date()).toTimeString().substr(0,5),
          isOnce: false,
          isRecurring: true,
          isDaily:false,
          isWeekly:false,
          isMonthly:false,
          isCustom: true,
          customDays: 21
        }

      case 'cattle':
        return {
          title: 'Cattle Heat Cycle Reminder',
          startDate: (new Date()).toISOString().substr(0,10),
          startTime: (new Date()).toTimeString().substr(0,5),
          isOnce: false,
          isRecurring: true,
          isDaily:false,
          isWeekly:false,
          isMonthly:false,
          isCustom: true,
          customDays: 21
        }

      case 'beef':
        return {
          title: 'Beef Heat Cycle Reminder',
          startDate: (new Date()).toISOString().substr(0,10),
          startTime: (new Date()).toTimeString().substr(0,5),
          isOnce: false,
          isRecurring: true,
          isDaily:false,
          isWeekly:false,
          isMonthly:false,
          isCustom: true,
          customDays: 21
        }

      default:
        return {
          title: 'Heat Cycle Reminder',
          startDate: (new Date()).toISOString().substr(0,10),
          startTime: (new Date()).toTimeString().substr(0,5),
          isOnce: false,
          isRecurring: true,
          isDaily:false,
          isWeekly:false,
          isMonthly:false,
          isCustom: true,
          customDays: 21
        }
    }
  }
);
const REMINDER_INITIAL_STATE = {
  messageListener: null,
  selectedReminderId: '',
  visibleReminders: {}
};
  
export default REMINDER_INITIAL_STATE;
  
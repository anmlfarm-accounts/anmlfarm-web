import { createSelector } from 'reselect';
import { convertObjectToArray } from '../../utils/array/mapper';

export const selectSubscriptionState = (state) => state.subscriptions;

export const selectSubscriptions = createSelector(
  [selectSubscriptionState],
  (s) => s.subscriptions,
);

export const selectAllSubscriptionsAsArray = createSelector(
  [selectSubscriptions],
  (subscriptions) => convertObjectToArray(subscriptions),
);

export const selectActiveSubscriptionsAsArray = createSelector(
  [selectSubscriptions],
  (subscriptions) => subscriptions.filter(o=>(o.paymentSuccess && !o.paymentFailure)),
);

import subscriptionActionTypes from './subscription.action-types'

export const startSubscriptionCreate = subscriptionInfo => ({
    type: subscriptionActionTypes.START_SUBSCRIPTION_CREATE,
    payload: subscriptionInfo
})

export const subscriptionCreateSuccess = subscriptionInfo => ({
    type: subscriptionActionTypes.SUBSCRIPTION_CREATE_SUCCESS,
    payload: subscriptionInfo
})

export const subscriptionCreateFailed = error => ({
    type: subscriptionActionTypes.SUBSCRIPTION_CREATE_FAILED,
    payload: error
})

export const startSubscriptionFetch = subscriptionId => ({
    type: subscriptionActionTypes.START_SUBSCRIPTION_FETCH,
    payload: subscriptionId
})

export const subscriptionFetchSuccess = subscriptionInfo => ({
    type: subscriptionActionTypes.SUBSCRIPTION_FETCH_SUCCESS,
    payload: subscriptionInfo
})

export const subscriptionFetchFailed = error => ({
    type: subscriptionActionTypes.SUBSCRIPTION_FETCH_FAILED,
    payload: error
})

export const deleteSubscriptionStart = (subscriptionInfo) => ({
    type: subscriptionActionTypes.DELETE_SUBSCRIPTION_START,
    payload: subscriptionInfo,
  });
  
  export const deleteSubscriptionSuccess = (subscriptionInfo) => ({
    type: subscriptionActionTypes.DELETE_SUBSCRIPTION_SUCCESS,
    payload: subscriptionInfo,
  });
  
  export const deleteSubscriptionFailure = (error) => ({
    type: subscriptionActionTypes.DELETE_SUBSCRIPTION_FAILURE,
    payload: error,
  });
import { all, call, put, takeEvery } from "redux-saga/effects";
import { firestore } from "../../firebase/firebase.utils";
import { deleteSubscriptionSuccess, subscriptionCreateFailed, subscriptionCreateSuccess, subscriptionFetchFailed, subscriptionFetchSuccess } from "./subscription.actions";
import farmActionTypes from "../farms/farm.action-types";
import { convertObjectToArray } from "../../utils/array/mapper";
import { getCurrentUser } from "../../firebase/firebase.functions";

export function* createNewFarmSubscriptionAsync(action) {
    const farmInfo = action.payload;
    if(!farmInfo) return
    if(!farmInfo?.subscriptionId || farmInfo?.subscriptionId?.length < 6) {
        try{
            let subscriptionInfo = {
                farmId: farmInfo?.farmId ?? farmInfo.id,
                createdDate: new Date(),
                trialStartDate: new Date() < new Date('1 Sept 2021') ? new Date('1 Sept 2021') : new Date(),
                active: true
            }
            let trialEndDate = new Date(subscriptionInfo.trialStartDate);
            trialEndDate.setDate(trialEndDate.getDate() + 30);
            subscriptionInfo.trialEndDate = trialEndDate;
            const collectionRef = firestore.collection('subscriptions');
            const addedSubscriptionRef = yield collectionRef.add({...subscriptionInfo});
            yield put(subscriptionCreateSuccess({id:addedSubscriptionRef.id, subscriptionId: addedSubscriptionRef.id, ...subscriptionInfo}));
        } catch(error) {
            yield put(subscriptionCreateFailed(error));
        }
    }
}

export function* fetchFarmSubscription(action){
    const farmInfo = action.payload;
    if(!farmInfo.subscriptionId) {
        yield call(createNewFarmSubscriptionAsync, farmInfo);
    }
    const subscriptionId = farmInfo.subscriptionId;
    try{
        const subscriptionRef = firestore.doc(
            `subscriptions/${subscriptionId}`,
        );
        const snapShot = yield subscriptionRef.get();
        if (snapShot.exists) {
            const subscriptionDoc = yield snapShot.data();
            yield put(subscriptionFetchSuccess({[subscriptionId]:{id: subscriptionId, subscriptionId, ...subscriptionDoc}}));
        } else {
            yield put(deleteSubscriptionSuccess({[subscriptionId]:{id: subscriptionId, subscriptionId}}))
        }
    } catch (error) {
        console.log('Error fetching farm subscription', error);
        yield put(subscriptionFetchFailed(error));
    }
}

export function* fetchAllFarms(action) {
    const userAuth = yield getCurrentUser();
    if (!userAuth) return;
    const fetchedFarm = convertObjectToArray(action.payload).find(f=>f.subscriptionId);
    if(fetchedFarm) {
        yield call(fetchFarmSubscription, {payload: fetchedFarm})
    }
    const unsubFarm = convertObjectToArray(action.payload).find(f=>((!f.subscriptionId) && (f.creatingUserId === userAuth.uid)));
    if(unsubFarm) {
        yield call(createNewFarmSubscriptionAsync, {payload: unsubFarm})
    }
}

export function* onFarmFetchSuccess() {
    yield takeEvery(
        farmActionTypes.FETCH_FARMS_SUCCESS, 
        fetchAllFarms
    );
}

export function* onFarmCreateSuccess() {
    yield takeEvery(
        farmActionTypes.CREATE_FARM_SUCCESS, 
        createNewFarmSubscriptionAsync
    );
}

export function* subscriptionSagas(){
    yield all([
        call(onFarmFetchSuccess),
        call(onFarmCreateSuccess),
    ])
}
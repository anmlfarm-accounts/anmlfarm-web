import { createSelector } from 'reselect';
import { convertObjectToArray } from '../../utils/array/mapper';
import {
  selectFarmCollection,
} from '../farms/farm.selectors';

export const selectEvents = (state) => state.events;

export const selectCurrentEvent = createSelector(
  [selectEvents],
  (eventDetais) => eventDetais.visibleEvents[eventDetais.selectedEvent],
);

export const selectVisibleEvents = createSelector(
  [selectEvents],
  (events) => events.visibleEvents
);

export const selectVisibleEventsArray = createSelector(
  [selectVisibleEvents],
  (events) => convertObjectToArray(events),
);

export const selectPublicEvents = createSelector(
  [selectVisibleEventsArray],
  (events) => events.filter((c) => c.type === 'public-event')
);

export const selectDeviceEvents = (device) => createSelector(
  [selectVisibleEventsArray],
  (events) => events.filter((c) => c.type === 'device-event' && c.deviceIds.includes(device.id))
);

export const selectCurrentFarmEvents = createSelector(
  [selectFarmCollection, selectVisibleEventsArray],
  (farm, events) => events.filter((c) => c.farmId === farm.selectedFarmId),
);

export const selectEventById = (eventId) =>
  createSelector([selectVisibleEvents], (events) =>
    events ? events[eventId] : null,
  );

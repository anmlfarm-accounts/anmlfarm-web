import { createSelector } from 'reselect';
import { convertObjectToArray } from '../../utils/array/mapper';
import {
  selectFarmCollection,
} from '../farms/farm.selectors';

export const selectNotifications = (state) => state.notifications;

export const selectCurrentNotification = createSelector(
  [selectNotifications],
  (notificationDetais) => notificationDetais.visibleNotifications[notificationDetais.selectedNotification],
);

export const selectVisibleNotifications = createSelector(
  [selectNotifications],
  (notifications) => notifications.visibleNotifications
);

export const selectVisibleNotificationsArray = createSelector(
  [selectVisibleNotifications],
  (notifications) => convertObjectToArray(notifications),
);

// export const selectPublicNotifications = createSelector(
//   [selectVisibleNotificationsArray],
//   (notifications) => notifications.filter((c) => c.type === 'public-notification')
// );

// export const selectDeviceNotifications = (device) => createSelector(
//   [selectVisibleNotificationsArray],
//   (notifications) => notifications.filter((c) => c.type === 'device-notification' && c.deviceIds.includes(device.id))
// );

export const selectCurrentFarmNotifications = createSelector(
  [selectFarmCollection, selectVisibleNotificationsArray],
  (farm, notifications) => notifications.filter((c) => c.farmIds?.includes(farm?.selectedFarmId)),
);

export const selectNotificationById = (notificationId) =>
  createSelector([selectVisibleNotifications], (notifications) =>
    notifications ? notifications[notificationId] : null,
  );

import { all, call, put, takeLatest } from "redux-saga/effects";
import { convertCollectionsSnapshotToMap } from "../../firebase/firebase.functions";
import { firestore } from "../../firebase/firebase.utils";
import productActionTypes from "../products/product.action-types";
import { fetchProductFailure, fetchProductSuccess} from "./product.actions";

export function* fetchAllProducts(){
    try{
        const collectionRef = firestore.collection('products');
        const snapshot = yield collectionRef.get();
        const collectionsMap = yield call(convertCollectionsSnapshotToMap, snapshot);
        yield put(fetchProductSuccess(collectionsMap))
    } catch(error) {
        yield put(fetchProductFailure(error.message))
    }
}


export function* onProductFetch() {
    yield takeLatest(
        productActionTypes.FETCH_PRODUCT_START, 
        fetchAllProducts
    );
}

export function* productSagas(){
    yield all([
        call(onProductFetch)
    ])
}
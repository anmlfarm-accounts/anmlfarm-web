import { createSelector } from 'reselect';
import { convertObjectToArray } from '../../utils/array/mapper';
import {selectCurrentSelectedAnimal
} from '../animals/animal.selectors';

export const selectProducts = (state) => state.products;

export const selectCurrentProduct = createSelector(
  [selectProducts],
  (productDetais) => productDetais.visibleProducts[productDetais.selectedProduct],
);

export const selectVisibleProducts = createSelector(
  [selectProducts],
  (products) => products.visibleProducts,
);

export const selectConnectedProductsArray = createSelector(
  [selectVisibleProducts],
  (products) => convertObjectToArray(products),
);

export const selectProductCategories = createSelector(
  [selectConnectedProductsArray],
  (products) => { 
    if(!products) return [];
    const acc = [];
    products.forEach(product => {
      if(product.categories){
        product.categories.forEach(c=>{
          if(!acc.includes(c)) acc.push(c);
        });
      }
    });
    acc.sort();
    return acc;
  },
);

export const selectCurrentAnimalProducts = createSelector(
  [selectCurrentSelectedAnimal, selectConnectedProductsArray],
  (animal, products) => products.filter((d) => d.animalIds.includes(animal?.id ?? animal?.animalId)),
);

export const selectProductById = (productId) =>
  createSelector([selectVisibleProducts], (products) =>
    products ? products[productId] : null,
  );

  
export const selectProductsByCategory = (category) =>
createSelector([selectConnectedProductsArray], (products) =>
  products ? products.filter(p=>p.categories?.includes(category)) : [],
);

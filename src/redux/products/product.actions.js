import productActionTypes from './product.action-types';

export const selectProduct = (productId) => ({
  type: productActionTypes.SELECT_PRODUCT,
  payload: productId,
});

export const clearProducts = () => ({
  type: productActionTypes.CLEAR_PRODUCTS,
});

export const fetchProducts = () => ({
  type: productActionTypes.FETCH_PRODUCT_START
});

export const fetchProductSuccess = (productDetails) => ({
  type: productActionTypes.FETCH_PRODUCT_SUCCESS,
  payload: productDetails,
});

export const fetchProductFailure = (error) => ({
  type: productActionTypes.FETCH_PRODUCT_FAILURE,
  payload: error,
});

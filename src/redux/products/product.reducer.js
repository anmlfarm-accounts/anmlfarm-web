import productActionTypes from "./product.action-types";

const INITIAL_STATE = {
    error: undefined,
    processing: false,
    selectedProduct: '',
    visibleProducts: {
    }
    
}

const productsReducer = (state = INITIAL_STATE, action) => {
    const {type, payload} = action;
    switch(type){

        case productActionTypes.SELECT_PRODUCT:
            return {
                ...state,
                selectedProduct: payload
            }

        case productActionTypes.CLEAR_PRODUCTS:
            return {
                ...state,
                selectedProduct: '',
                visibleProducts: {}
            }

        case productActionTypes.FETCH_PRODUCT_START:
            return {
                ...state,
                processing: true
            }

        case productActionTypes.FETCH_PRODUCT_FAILURE:
            return {
                ...state,
                error: payload,
                processing: false
            }

        case productActionTypes.FETCH_PRODUCT_SUCCESS:
            return {
                ...state,
                processing: false,
                visibleProducts: {
                    ...state.visibleProducts,
                    ...payload
                }
            }

        default:
            return state;
    }
}

export default productsReducer;
import { createSelector } from 'reselect';
import { convertObjectToArray } from '../../utils/array/mapper';

export const selectImages = (state) => state.images;

export const selectAllImages = createSelector(
  [selectImages],
  (images) => images.images,
);

export const selectAllImagesAsArray = createSelector(
  [selectAllImages],
  (images) => convertObjectToArray(images),
);

export const selectImagesByAnimalId = (animalId) =>
  createSelector([selectAllImagesAsArray], (images) =>
    images ? images.filter(i => i.animalIds? i.animalIds.includes(animalId):false) : [],
  );
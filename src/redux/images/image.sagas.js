import { all, call, put, takeEvery } from 'redux-saga/effects';
import { firestore } from '../../firebase/firebase.utils';
import imageActionTypes from './image.action-types';
import farmActionTypes from '../farms/farm.action-types';
import {
  imageUploadFailed,
  imageUploadSuccess,
  startImageUpload,
} from './image.actions';
import { uploadFileToStorage } from '../../firebase/firebase.functions';
import animalActionTypes from '../animals/animal.action-types';
import {
  addAnimalImagesSuccess,
  deleteAnimalSuccess,
} from '../animals/animal.actions';
import { updateFarmImagesSuccess } from '../farms/farm.actions';
import campActionTypes from '../camps/camp.action-types';
import { updateCampImagesSuccess } from '../camps/camp.actions';

export function* createNewImageAsync(action) {
  const data = action?.payload?.data;
  const fileInfo = { ...action?.payload };
  delete fileInfo.data;
  try {
    if (data) {
      if (data.includes('https:')) return;
      const collectionRef = firestore.collection('images');
      let uploadInfo = { ...fileInfo, createdAt: new Date() };
      if (uploadInfo.animalDetails) delete uploadInfo.animalDetails;
      if (uploadInfo.farmDetails) delete uploadInfo.farmDetails;
      const addedImageRef = yield collectionRef.add(uploadInfo);

      const mimeType = data.match(/[^:]\w+\/[\w-+\d.]+(?=;|,)/)[0];
      const fileType = data.match(/[^:/]\w+(?=;|,)/)[0];
      if (!mimeType || !fileType) return;
      let imageUploadRef = yield uploadFileToStorage(
        data,
        `${mimeType}/${addedImageRef.id}.${fileType}`,
      );
      let imageURL = yield imageUploadRef.ref.getDownloadURL();
      yield addedImageRef.update({ linkUrl: imageURL, updatedAt: new Date() });

      yield put(
        imageUploadSuccess({ imageId: addedImageRef.id, ...action.payload }),
      );
      try {
        if (fileInfo.animalIds && fileInfo.animalIds.length > 0) {
          const animalRef = firestore.doc(`animals/${fileInfo.animalIds[0]}`);
          const snapShot = yield animalRef.get();
          if (snapShot.exists) {
            const animalDoc = yield snapShot.data();
            let animalImages =
              fileInfo?.animalDetails?.animalImages ?? animalDoc?.animalImages;
            if (!animalImages) animalImages = [];
            animalImages = animalImages.filter((i) => i !== data);
            animalImages = animalImages.filter((i) => i !== imageURL);
            animalImages.push(imageURL);
            yield animalRef.update({ animalImages });
            yield put(
              addAnimalImagesSuccess({
                animalId: snapShot?.id,
                ...animalDoc,
                animalImages,
              }),
            );
          } else {
            yield put(
              deleteAnimalSuccess({
                id: fileInfo.animalIds[0],
                animalId: fileInfo.animalIds[0],
              }),
            );
          }
        }

        if (fileInfo.farmIds && fileInfo.farmIds.length > 0) {
          const farmRef = firestore.doc(`farms/${fileInfo.farmIds[0]}`);
          const farmShot = yield farmRef.get();
          if (farmShot.exists) {
            const farmDoc = yield farmShot.data();
            let farmImages =
              [fileInfo?.farmDetails?.farmImage] ??
              fileInfo?.farmDetails?.farmImages;
            if (!farmImages)
              farmImages = farmDoc?.farmImages ?? [farmDoc?.farmImage];
            if (!farmImages) farmImages = [];
            farmImages = farmImages.filter((i) => i !== data);
            farmImages = farmImages.filter((i) => i !== imageURL);
            farmImages.push(imageURL);
            yield farmRef.update({ farmImages, farmImage: imageURL });
            yield put(
              updateFarmImagesSuccess({
                farmId: farmShot?.id,
                ...farmDoc,
                farmImages,
                farmImage: imageURL,
              }),
            );
          }
        }

        if (fileInfo.campIds && fileInfo.campIds.length > 0) {
          const campRef = firestore.doc(`camps/${fileInfo.campIds[0]}`);
          const campShot = yield campRef.get();
          if (campShot.exists) {
            const campDoc = yield campShot.data();
            let campImages =
              [fileInfo?.campDetails?.campImage] ??
              fileInfo?.campDetails?.campImages;
            if (!campImages)
              campImages = campDoc?.campImages ?? [campDoc?.campImage];
            if (!campImages) campImages = [];
            campImages = campImages.filter((i) => i !== data);
            campImages = campImages.filter((i) => i !== imageURL);
            campImages.push(imageURL);
            yield campRef.update({ campImages, campImage: imageURL });
            yield put(
              updateCampImagesSuccess({
                campId: campShot?.id,
                ...campDoc,
                campImages,
                campImage: imageURL,
              }),
            );
          }
        }
      } catch (error) {
        console.log('ERROR', error);
      }
    }
  } catch (error) {
    console.log('ERROR', error);
    yield put(imageUploadFailed(error));
  }
}

export function* createNewImageFromAnimalDataAsync(action) {
  const animalDetails = action.payload.animalDetails ?? action.payload;
  if (animalDetails?.animalImages && animalDetails?.animalImages?.length > 0) {
    const animalImage = animalDetails?.animalImages.find(
      (i) => !i.includes('https:'),
    );
    if (animalImage)
      yield put(
        startImageUpload({
          animalIds: [animalDetails.animalId ?? animalDetails.id],
          data: animalImage,
          animalDetails,
        }),
      );
  }
}

export function* createNewImageFromFarmDataAsync(action) {
  const farmDetails = action.payload.farmDetails ?? action.payload;
  if (farmDetails?.farmImages && farmDetails?.farmImages?.length > 0) {
    const farmImage = farmDetails?.farmImages.find(
      (i) => !i.includes('https:'),
    );
    if (farmImage)
      yield put(
        startImageUpload({
          farmIds: [farmDetails.farmId ?? farmDetails.id],
          data: farmImage,
          farmDetails,
        }),
      );
  }
  if (farmDetails?.farmImage) {
    if (!farmDetails?.farmImage.includes('https'))
      yield put(
        startImageUpload({
          farmIds: [farmDetails.farmId ?? farmDetails.id],
          data: farmDetails?.farmImage,
          farmDetails,
        }),
      );
  }
}

export function* createNewImageFromCampDataAsync(action) {
  const campDetails = action.payload.campDetails ?? action.payload;
  if (campDetails?.campImages && campDetails?.campImages?.length > 0) {
    const campImage = campDetails?.campImages.find(
      (i) => !i.includes('https:'),
    );
    if (campImage)
      yield put(
        startImageUpload({
          farmIds: [campDetails.farmId ?? campDetails.id],
          data: campImage,
          campDetails,
        }),
      );
  }
  if (campDetails?.campImage) {
    if (!campDetails?.campImage.includes('https:'))
      yield put(
        startImageUpload({
          farmIds: [campDetails.farmId ?? campDetails.id],
          data: campDetails?.campImage,
          campDetails,
        }),
      );
  }
}

export function* onImageUploadStart() {
  yield takeEvery(imageActionTypes.START_IMAGE_UPLOAD, createNewImageAsync);
}

export function* onAnimalCreateSuccess() {
  yield takeEvery(
    animalActionTypes.CREATE_ANIMAL_SUCCESS,
    createNewImageFromAnimalDataAsync,
  );
}

export function* onAnimalUpdateSuccess() {
  yield takeEvery(
    animalActionTypes.UPDATE_ANIMAL_SUCCESS,
    createNewImageFromAnimalDataAsync,
  );
}

export function* onFarmCreateSuccess() {
  yield takeEvery(
    farmActionTypes.CREATE_FARM_SUCCESS,
    createNewImageFromFarmDataAsync,
  );
}

export function* onFarmUpdateSuccess() {
  yield takeEvery(
    farmActionTypes.UPDATE_FARM_SUCCESS,
    createNewImageFromFarmDataAsync,
  );
}
export function* onCampCreateSuccess() {
  yield takeEvery(
    campActionTypes.CREATE_CAMP_SUCCESS,
    createNewImageFromFarmDataAsync,
  );
}

export function* onCampUpdateSuccess() {
  yield takeEvery(
    campActionTypes.UPDATE_CAMP_SUCCESS,
    createNewImageFromFarmDataAsync,
  );
}

export function* imageSagas() {
  yield all([
    call(onImageUploadStart),
    call(onAnimalUpdateSuccess),
    call(onAnimalCreateSuccess),
    call(onFarmCreateSuccess),
    call(onFarmUpdateSuccess),
    call(onCampCreateSuccess),
    call(onCampUpdateSuccess),
  ]);
}

import { all, call} from 'redux-saga/effects'
import { animalSagas } from './animals/animal.sagas'
import { campSagas } from './camps/camp.sagas'
import { farmSagas } from './farms/farm.sagas'
import { imageSagas } from './images/image.sagas'
import { userSagas } from './users/user.sagas'
import { deviceSagas } from './devices/device.sagas'
import { eventSagas } from './events/event.sagas'
import { notificationSagas } from './notifications/notification.sagas'
import { reminderSagas } from './reminders/reminder.sagas'
import { documentSagas } from './documents/document.sagas'
import { productSagas } from './products/product.sagas'
import { cartSagas } from './cart/cart.sagas'
import { orderSagas } from './orders/order.sagas'
import { subscriptionSagas } from './subscriptions/subscription.sagas'

export default function* rootSaga() {
    yield all([
        call(userSagas),
        call(farmSagas),
        call(campSagas),
        call(animalSagas),
        call(imageSagas),
        call(deviceSagas),
        call(eventSagas),
        call(notificationSagas),
        call(reminderSagas),
        call(documentSagas),
        call(productSagas),
        call(cartSagas),
        call(orderSagas),
        call(subscriptionSagas),
    ])
}
import { omit } from 'lodash-es';
import campActionTypes from './camp.action-types';

const INITIAL_STATE = {
  error: undefined,
  processing: false,
  selectedCamp: '',
  visibleCamps: {},
};

const campsReducer = (state = INITIAL_STATE, action) => {
  const { type, payload } = action;
  switch (type) {
    case campActionTypes.SELECT_CAMP:
      return {
        ...state,
        selectedCamp: payload,
      };

    case campActionTypes.CLEAR_CAMPS:
      return {
        ...state,
        selectedCamp: '',
        visibleCamps: {},
      };

    case campActionTypes.CREATE_CAMP_START:
      return {
        ...state,
        processing: true,
      };

    case campActionTypes.CREATE_CAMP_FAILURE:
      return {
        ...state,
        error: payload,
        processing: false,
      };

    case campActionTypes.FETCH_CAMP_START:
      return {
        ...state,
        processing: true,
      };

    case campActionTypes.FETCH_CAMP_FAILURE:
      return {
        ...state,
        error: payload,
        processing: false,
      };

    case campActionTypes.CREATE_CAMP_SUCCESS:
      return {
        ...state,
        processing: false,
        visibleCamps: {
          ...state.visibleCamps,
          [payload.campId]: payload,
        },
        selectedCamp: payload?.campId ?? state.selectedCamp,
      };

    case campActionTypes.UPDATE_CAMP_SUCCESS:
      return {
        ...state,
        processing: false,
        visibleCamps: {
          ...state.visibleCamps,
          [payload.campId]: payload,
        },
        selectedCamp: payload?.campId ?? state.selectedCamp,
      };

    case campActionTypes.ADD_CAMP_FEED_SUCCESS:
      return {
        ...state,
        processing: false,
        visibleCamps: {
          ...state.visibleCamps,
          [payload.campDetails?.id ?? payload.campDetails?.campId]: {
            ...state.visibleCamps[
              payload.campDetails?.id ?? payload.campDetails?.campId
            ],
            feedData: payload.feedDetails,
          },
        },
        selectedCamp: payload?.campId ?? state.selectedCamp,
      };

    case campActionTypes.DELETE_CAMP_SUCCESS:
      return {
        ...state,
        processing: false,
        visibleCamps: {
          ...omit(state.visibleCamps, payload.campId),
        },
        selectedCamp: state.visibleCamps[0] ? state.visibleCamps[0]?.id : '',
      };

    case campActionTypes.RESTORE_CAMP_SUCCESS:
      return {
        ...state,
        processing: false,
        visibleCamps: {
          ...state.visibleCamps,
          [payload.campId]: payload,
        },
        // selectedCamp: payload?.campId ?? state.selectedCamp,
      };

    case campActionTypes.FETCH_CAMP_SUCCESS:
    case campActionTypes.FETCH_SELECTED_CAMP_SUCCESS:
      return {
        ...state,
        processing: false,
        visibleCamps: {
          ...state.visibleCamps,
          ...payload,
        },
      };

    case campActionTypes.DELETE_CAMP_SUCCESS:
      return {
        ...state,
        processing: false,
        visibleCamps: {
          ...omit(state.visibleCamps, payload?.campId ?? ''),
        },
        selectedCamp: '',
      };

    case campActionTypes.UPDATE_CAMP_SUCCESS:
    case campActionTypes.UPDATE_CAMP_IMAGES_SUCCESS:
      return {
        ...state,
        processing: false,
        visibleCamps: {
          ...state.visibleCamps,
          [payload?.campId ?? payload?.id]: payload,
        },
        selectedCamp: payload?.campId ?? payload?.id,
      };

    default:
      return state;
  }
};

export default campsReducer;

import { all, call, put, takeLatest } from 'redux-saga/effects';
import { convertCollectionsSnapshotToMap } from '../../firebase/firebase.functions';
import { firestore } from '../../firebase/firebase.utils';
import farmDetailsActionTypes from '../farms/farm.action-types';
import campActionTypes from './camp.action-types';
import {
  fetchCampFailure,
  fetchCampSuccess,
  createCampSuccess,
  createCampFailure,
  updateCampFailure,
  updateCampSuccess,
  addCampFeedSuccess,
  addCampFeedFailure,
  fetchSelectedCampSuccess,
  deleteCampSuccess,
  deleteCampFailure,
  restoreCampSuccess,
  restoreCampFailure,
} from './camp.actions';

export function* createNewCampAsync(action) {
  const campDetails = action.payload;
  const campImage = action.payload.campImage;
  delete campDetails.campImage;
  if (!campDetails) return;
  try {
    const collectionRef = firestore.collection('camps');
    const addedCampRef = yield collectionRef.add(campDetails);
    yield put(
      createCampSuccess({
        campId: addedCampRef.id,
        ...action.payload,
        campImage,
      }),
    );
  } catch (error) {
    yield put(createCampFailure(error));
  }
}

export function* updateCampDetailsAsync(action) {
  const campDetails = action.payload;
  const campImage = action.payload.campImage;
  if (!campDetails) return;
  try {
    const campRef = firestore.doc(
      `camps/${campDetails?.id ?? campDetails?.campId}`,
    );
    const updatedCampRef = yield campRef.get();
    if (updatedCampRef.exists) {
      const updatedCampDetails = { ...campDetails };
      delete updatedCampDetails.id;
      delete updatedCampDetails.campId;
      delete updatedCampDetails.campImage;
      yield campRef.update(updatedCampDetails);
      yield put(
        updateCampSuccess({
          campId: campDetails?.id ?? campDetails?.campId,
          ...campDetails,
          campImage,
        }),
      );
    }
  } catch (error) {
    yield put(updateCampFailure(error));
  }
}

export function* addCampFeedAsync(action) {
  const { campDetails, feedDetails } = action.payload;
  if (!campDetails || !feedDetails) return;

  try {
    const campRef = firestore.doc(
      `camps/${campDetails?.id ?? campDetails?.campId}`,
    );
    const snapShot = yield campRef.get();
    if (snapShot.exists) {
      const campDoc = yield snapShot.data();
      const feedData = {
        ...campDoc.feedData,
        [feedDetails.dateTime.toISOString()]: feedDetails,
      };
      yield campRef.update({ feedData });
      yield put(addCampFeedSuccess(campDetails, feedData));
    }
  } catch (error) {
    yield put(addCampFeedFailure(error));
  }
}

export function* deleteLatestCampFeedAsync(action) {
  const { campDetails } = action.payload;
  if (!campDetails) return;

  try {
    const campRef = firestore.doc(
      `camps/${campDetails.id ?? campDetails.campId}`,
    );
    const snapShot = yield campRef.get();
    const feedKeysAsArray = Object.keys(campDetails.feedData);
    feedKeysAsArray.sort().pop();
    const feedData = Object.keys(campDetails.feedData)
      .filter((key) => feedKeysAsArray.includes(key))
      .reduce((feedObject, key) => {
        return {
          ...feedObject,
          [key]: campDetails.feedData[key],
        };
      }, {});
    if (snapShot.exists) {
      yield campRef.update({ feedData });
      yield put(addCampFeedSuccess(campDetails, feedData));
    }
  } catch (error) {
    yield put(addCampFeedFailure(error));
  }
}

export function* deleteCampDetailsAsync(action) {
  const campDetails = action.payload;
  if (!campDetails) return;
  try {
    const campRef = firestore.doc(
      `camps/${campDetails?.id ?? campDetails?.campId}`,
    );
    const updatedCampRef = yield campRef.get();
    if (updatedCampRef.exists) {
      const updatedCampDetails = { deleted: true };
      delete updatedCampRef.id;
      yield campRef.update(updatedCampDetails);
      yield put(
        deleteCampSuccess({
          campId: campDetails?.id ?? campDetails?.campId,
          ...campDetails,
        }),
      );
    }
  } catch (error) {
    yield put(deleteCampFailure(error));
  }
}

export function* restoreCampDetailsAsync(action) {
  const campDetails = action.payload;
  if (!campDetails) return;
  try {
    const campRef = firestore.doc(
      `camps/${campDetails?.id ?? campDetails?.campId}`,
    );
    const updatedCampRef = yield campRef.get();
    if (updatedCampRef.exists) {
      delete updatedCampRef.id;
      delete campDetails.deleted;
      yield campRef.set(campDetails);
      yield put(
        restoreCampSuccess({
          campId: campDetails?.id ?? campDetails?.campId,
          ...campDetails,
        }),
      );
    }
  } catch (error) {
    yield put(restoreCampFailure(error));
  }
}

export function* fetchFarmCamps(action) {
  const farmId = action.payload;
  if (!farmId) return;
  try {
    const collectionRef = firestore
      .collection('camps')
      .where('farmId', '==', farmId);
    const snapshot = yield collectionRef.get();
    const collectionsMap = yield call(
      convertCollectionsSnapshotToMap,
      snapshot,
    );
    yield put(fetchSelectedCampSuccess(collectionsMap));
  } catch (error) {
    yield put(fetchCampFailure(error.message));
  }
}

export function* fetchSelectedCamp(action) {
  const campId = action.payload;
  if (!campId) return;
  try {
    const campRef = firestore.doc(`camps/${campId}`);
    const snapShot = yield campRef.get();
    if (snapShot.exists) {
      const campDoc = yield snapShot.data();
      yield put(
        fetchCampSuccess({ [campId]: { id: campId, campId, ...campDoc } }),
      );
    }
  } catch (error) {
    yield put(fetchCampFailure(error.message));
  }
}

export function* onFarmSelected() {
  yield takeLatest(farmDetailsActionTypes.SELECT_FARM, fetchFarmCamps);
}

export function* onCampCreate() {
  yield takeLatest(campActionTypes.CREATE_CAMP_START, createNewCampAsync);
}

export function* onCampFieldUpdate() {
  yield takeLatest(campActionTypes.UPDATE_CAMP_START, updateCampDetailsAsync);
}

export function* onCampFeedAdd() {
  yield takeLatest(campActionTypes.ADD_CAMP_FEED_START, addCampFeedAsync);
}

export function* onCampSelected() {
  yield takeLatest(campActionTypes.SELECT_CAMP, fetchSelectedCamp);
}

export function* onCampFeedLatestDelete() {
  yield takeLatest(
    campActionTypes.DELETE_LATEST_CAMP_FEED_START,
    deleteLatestCampFeedAsync,
  );
}

export function* onCampDelete() {
  yield takeLatest(campActionTypes.DELETE_CAMP_START, deleteCampDetailsAsync);
}

export function* onCampRestore() {
  yield takeLatest(campActionTypes.RESTORE_CAMP_START, restoreCampDetailsAsync);
}

export function* campSagas() {
  yield all([
    call(onFarmSelected),
    call(onCampDelete),
    call(onCampCreate),
    call(onCampFieldUpdate),
    call(onCampFeedAdd),
    call(onCampFeedLatestDelete),
    call(onCampDelete),
    call(onCampRestore),
  ]);
}

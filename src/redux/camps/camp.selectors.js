import { createSelector } from 'reselect';
import { convertObjectToArray } from '../../utils/array/mapper';
import { convertStringToDateTimeSeconds } from '../../utils/date/calculations';
import {
  selectFarmCollection,
  selectVisibleFarmsAsArray,
} from '../farms/farm.selectors';
import { selectCurrentUser } from '../users/user.selectors';

export const selectCamps = (state) => state.camps;

export const selectCurrentCamp = createSelector(
  [selectCamps],
  (campDetais) => campDetais.visibleCamps[campDetais.selectedCamp],
);

export const selectVisibleCamps = createSelector(
  [selectCamps],
  (camps) => camps.visibleCamps,
);

export const selectVisibleCampsAsArray = createSelector(
  [selectVisibleCamps],
  (camps) => convertObjectToArray(camps),
);

export const selectCurrentFarmCamps = createSelector(
  [selectFarmCollection, selectVisibleCampsAsArray],
  (farm, camps) =>
    camps.filter((c) => (c.farmId ?? c.id) === farm?.selectedFarmId),
);

export const selectCurrentActiveCampsAsArray = createSelector(
  [selectCurrentFarmCamps],
  (camps) => camps.filter((a) => !a.deleted),
);

export const selectRecycleBinCampsAsArray = createSelector(
  [selectCurrentFarmCamps],
  (camps) => camps.filter((a) => a.deleted),
);

export const selectCurrentFarmCampsCount = createSelector(
  [selectCurrentActiveCampsAsArray],
  (camps) => camps.length,
);

export const selectCampById = (campId) =>
  createSelector([selectVisibleCamps], (camps) =>
    camps ? camps[campId] : null,
  );

export const selectCurrentSelectedCampFeedsAsArray = createSelector(
  [selectCurrentCamp],
  (camp) => {
    let returnFeed = camp?.feedData
      ? Object.keys(camp.feedData)
          .map((key) => camp.feedData[key])
          .filter((f) => f?.feed?.length > 0)
      : [];
    returnFeed.sort(
      (a, b) =>
        convertStringToDateTimeSeconds(a?.feedDate) -
        convertStringToDateTimeSeconds(b?.feedDate),
    );
    return returnFeed;
  },
);

export const selectUserCanModifyCamp = createSelector(
  [selectCurrentUser, selectCurrentCamp, selectVisibleFarmsAsArray],
  (user, camp, farms) => {
    if (!user || !camp || !farms) return false;
    const campFarms = farms.filter((f) => camp?.farmId === (f.id ?? f.farmId));
    const userModifyCamps = campFarms.filter(
      (f) =>
        f.workingUsers?.includes(user.id ?? user.uid) ||
        f.managingUsers?.includes(user.id ?? user.uid) ||
        f.creatingUserId === (user.id ?? user.uid),
    );

    if (!userModifyCamps || userModifyCamps.length === 0) return false;

    return true;
  },
);

export const selectUserCanEditCamp = createSelector(
  [selectCurrentUser, selectCurrentCamp, selectVisibleFarmsAsArray],
  (user, camp, farms) => {
    if (!user || !camp || !farms) return false;
    const campFarms = farms.filter((f) => camp?.farmId === (f.id ?? f.farmId));
    const userEditCamps = campFarms.filter(
      (f) =>
        f.managingUsers?.includes(user?.id ?? user.uid) ||
        f.creatingUserId === (user.id ?? user.uid),
    );

    if (!userEditCamps || userEditCamps.length === 0) return false;

    return true;
  },
);

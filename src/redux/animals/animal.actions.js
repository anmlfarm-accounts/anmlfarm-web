import animalActionTypes from './animal.action-types';

export const clearAnimalFilters = () => ({
  type: animalActionTypes.CLEAR_FILTERS,
});

export const setProcessingStart = () => ({
  type: animalActionTypes.SET_PROCESSING_START,
});

export const updateAnimalFilters = (filterString) => ({
  type: animalActionTypes.UPDATE_FILTERS,
  payload: filterString,
});

export const updateAnimalStart = (animalDetails) => ({
  type: animalActionTypes.UPDATE_ANIMAL_START,
  payload: animalDetails,
});
export const updateAnimalSuccess = (animalDetails) => ({
  type: animalActionTypes.UPDATE_ANIMAL_SUCCESS,
  payload: animalDetails,
});

export const updateAnimalFailure = (error) => ({
  type: animalActionTypes.UPDATE_ANIMAL_FAILURE,
  payload: error,
});

export const deleteAnimalStart = (animalDetails) => ({
  type: animalActionTypes.DELETE_ANIMAL_START,
  payload: animalDetails,
});

export const deleteAnimalSuccess = (animalDetails) => ({
  type: animalActionTypes.DELETE_ANIMAL_SUCCESS,
  payload: animalDetails,
});

export const deleteAnimalFailure = (error) => ({
  type: animalActionTypes.DELETE_ANIMAL_FAILURE,
  payload: error,
});

export const addAnimalWeightStart = (animalDetails, weightDetails) => ({
  type: animalActionTypes.ADD_ANIMAL_WEIGHT_START,
  payload: { animalDetails, weightDetails },
});

export const addAnimalWeightSuccess = (animalDetails, weightDetails) => ({
  type: animalActionTypes.ADD_ANIMAL_WEIGHT_SUCCESS,
  payload: { animalDetails, weightDetails },
});

export const addAnimalImagesSuccess = (animalDetails) => ({
  type: animalActionTypes.ADD_ANIMAL_IMAGES_SUCCESS,
  payload: animalDetails,
});

export const addAnimalWeightFailure = (error) => ({
  type: animalActionTypes.ADD_ANIMAL_WEIGHT_FAILURE,
  payload: error,
});

export const addAnimalMedicationStart = (animalDetails, medicationDetails) => ({
  type: animalActionTypes.ADD_ANIMAL_MEDICATION_START,
  payload: { animalDetails, medicationDetails },
});

export const addAnimalMedicationSuccess = (
  animalDetails,
  medicationDetails,
) => ({
  type: animalActionTypes.ADD_ANIMAL_MEDICATION_SUCCESS,
  payload: { animalDetails, medicationDetails },
});

export const addAnimalMedicationFailure = (error) => ({
  type: animalActionTypes.ADD_ANIMAL_MEDICATION_FAILURE,
  payload: error,
});

export const addAnimalMilkStart = (animalDetails, milkDetails) => ({
  type: animalActionTypes.ADD_ANIMAL_MILK_START,
  payload: { animalDetails, milkDetails },
});

export const addAnimalMilkSuccess = (animalDetails, milkDetails) => ({
  type: animalActionTypes.ADD_ANIMAL_MILK_SUCCESS,
  payload: { animalDetails, milkDetails },
});

export const addAnimalMilkFailure = (error) => ({
  type: animalActionTypes.ADD_ANIMAL_MILK_FAILURE,
  payload: error,
});

export const addAnimalHeatStart = (animalDetails, heatDetails) => ({
  type: animalActionTypes.ADD_ANIMAL_HEAT_START,
  payload: { animalDetails, heatDetails },
});

export const addAnimalHeatSuccess = (animalDetails, heatDetails) => ({
  type: animalActionTypes.ADD_ANIMAL_HEAT_SUCCESS,
  payload: { animalDetails, heatDetails },
});

export const addAnimalHeatFailure = (error) => ({
  type: animalActionTypes.ADD_ANIMAL_HEAT_FAILURE,
  payload: error,
});

export const endAnimalStart = (animalDetails, endDetails) => ({
  type: animalActionTypes.END_ANIMAL_START,
  payload: { animalDetails, endDetails },
});

export const endAnimalSuccess = (animalDetails, endDetails) => ({
  type: animalActionTypes.END_ANIMAL_SUCCESS,
  payload: { animalDetails, endDetails },
});

export const endAnimalFailure = (error) => ({
  type: animalActionTypes.END_ANIMAL_FAILURE,
  payload: error,
});

export const deleteLatestAnimalWeightStart = (animalDetails) => ({
  type: animalActionTypes.DELETE_LATEST_ANIMAL_WEIGHT_START,
  payload: { animalDetails },
});

export const deleteLatestAnimalMedicationStart = (animalDetails) => ({
  type: animalActionTypes.DELETE_LATEST_ANIMAL_MEDICATION_START,
  payload: { animalDetails },
});

export const deleteLatestAnimalMilkStart = (animalDetails) => ({
  type: animalActionTypes.DELETE_LATEST_ANIMAL_MILK_START,
  payload: { animalDetails },
});

export const deleteLatestAnimalHeatStart = (animalDetails) => ({
  type: animalActionTypes.DELETE_LATEST_ANIMAL_HEAT_START,
  payload: { animalDetails },
});

export const deleteLatestAnimalCampHistoryStart = (animalDetails, campId) => ({
  type: animalActionTypes.DELETE_LATEST_ANIMAL_CAMP_HISTORY_START,
  payload: { animalDetails, campId },
});

export const deleteLatestAnimalCampHistorySuccess = (
  animalDetails,
  campHistory,
) => ({
  type: animalActionTypes.DELETE_LATEST_ANIMAL_CAMP_HISTORY_SUCCESS,
  payload: { animalDetails, campHistory },
});

export const deleteLatestAnimalCampHistoryFailure = (error) => ({
  type: animalActionTypes.DELETE_LATEST_ANIMAL_CAMP_HISTORY_FAILURE,
  payload: error,
});

export const setSelectedAnimal = (animalId) => ({
  type: animalActionTypes.SET_SELECTED_ANIMAL,
  payload: animalId,
});

export const createAnimalStart = (animalDetails) => ({
  type: animalActionTypes.CREATE_ANIMAL_START,
  payload: animalDetails,
});

export const createAnimalSuccess = (animalDetails) => ({
  type: animalActionTypes.CREATE_ANIMAL_SUCCESS,
  payload: animalDetails,
});

export const createAnimalFailure = (error) => ({
  type: animalActionTypes.CREATE_ANIMAL_FAILURE,
  payload: error,
});

export const transferAnimalCampStart = (animalDetails, campDetails) => ({
  type: animalActionTypes.TRANSFER_ANIMAL_CAMP_START,
  payload: { animalDetails, campDetails },
});

// export const createNewAnimalAsync = (animalData) => {
//     return dispatch => {
//         dispatch(createAnimalStart())
//         firestore.collection('animals').add({...animalData}).then(resp =>
//             {
//                 animalData.animalId = resp.id;
//                 dispatch(createAnimalSuccess(animalData))
//             })
//         .catch (error => {
//             dispatch(createAnimalFailure(error.message));
//         })
//     }
// }

export const fetchAnimalStart = () => ({
  type: animalActionTypes.FETCH_ANIMAL_START,
});

export const fetchAnimalSuccess = (animals) => ({
  type: animalActionTypes.FETCH_ANIMAL_SUCCESS,
  payload: animals,
});


export const fetchCampAnimalSuccess = (animals) => ({
  type: animalActionTypes.FETCH_CAMP_ANIMAL_SUCCESS,
  payload: animals,
});

export const fetchAnimalFailure = (error) => ({
  type: animalActionTypes.FETCH_ANIMAL_FAILURE,
  payload: error,
});

export const fetchAnimalFamilyStart = (animals) => ({
  type: animalActionTypes.FETCH_ANIMAL_FAMILY_START,
  payload: animals,
});
// export const fetchCampAnimalsAsync = (campId) => {
//     return dispatch => {
//         dispatch(fetchAnimalStart())
//         firestore.collection('animals').where('campId', "==", campId).get()
//         .then(resp =>
//             {
//                 // dispatch(clearAnimals())
//                 return resp.docs.map(doc => {
//                     const dat = doc.data();
//                     dat.animalId = doc.id;
//                     return dat;
//                 })
//             }).then(docs => {
//                 docs.forEach(doc => {
//                     dispatch(createAnimalSuccess(doc))
//                 });
//                 return;
//             })
//         .catch (error => {
//             dispatch(fetchAnimalFailure(error.message));
//         })
//     }
// }

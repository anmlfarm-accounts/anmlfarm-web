import { createSelector } from 'reselect';
import { convertObjectToArray } from '../../utils/array/mapper';
import {
  calculateCurrentAge,
  convertStringToDateTimeSeconds,
} from '../../utils/date/calculations';
import {
  selectCurrentCamp,
  selectVisibleCampsAsArray,
} from '../camps/camp.selectors';
import { selectVisibleFarmsAsArray } from '../farms/farm.selectors';
import { selectCurrentUser } from '../users/user.selectors';

export const animalSelector = (state) => state.animals;

export const farmAnimalsSelector = createSelector(
  [animalSelector],
  (animalDetails) => animalDetails.visibleAnimals,
);

export const animalLoading = createSelector(
  [animalSelector],
  (animalDetails) => animalDetails.processing ?? false,
);

export const selectCurrentSelectedAnimal = createSelector(
  [farmAnimalsSelector, animalSelector],
  (animals, animalDetails) => animals[animalDetails.selectedAnimalId],
);

export const selectCurrentSelectedAnimalAge = createSelector(
  [selectCurrentSelectedAnimal],
  (animal) => calculateCurrentAge(animal.animalBirthday),
);

export const selectCurrentSelectedAnimalWeightsAsArray = createSelector(
  [selectCurrentSelectedAnimal],
  (animal) => {
    let returnWeights = animal?.weightData
      ? Object.keys(animal.weightData)
          .map((key) => animal.weightData[key])
          .filter((w) => w.weight.length > 0)
      : [];
    returnWeights.sort(
      (a, b) =>
        convertStringToDateTimeSeconds(a.weightDate) -
        convertStringToDateTimeSeconds(b.weightDate),
    );
    return returnWeights;
  },
);

export const selectCurrentSelectedAnimalMedicationsAsArray = createSelector(
  [selectCurrentSelectedAnimal],
  (animal) => {
    let returnMedication = animal?.medicationData
      ? Object.keys(animal.medicationData)
          .map((key) => animal.medicationData[key])
          .filter((m) => m.medication.length > 0)
      : [];
    returnMedication.sort(
      (a, b) =>
        convertStringToDateTimeSeconds(b.medicationDate) -
        convertStringToDateTimeSeconds(a.medicationDate),
    );
    return returnMedication;
  },
);

export const selectCurrentSelectedAnimalMilkAsArray = createSelector(
  [selectCurrentSelectedAnimal],
  (animal) => {
    let returnMilk = animal?.milkData
      ? Object.keys(animal.milkData)
          .map((key) => animal.milkData[key])
          .filter((m) => m.milk.length > 0)
      : [];
    returnMilk.sort(
      (a, b) =>
        convertStringToDateTimeSeconds(a.milkDate) -
        convertStringToDateTimeSeconds(b.milkDate),
    );
    return returnMilk;
  },
);

export const selectCurrentSelectedAnimalHeatAsArray = createSelector(
  [selectCurrentSelectedAnimal],
  (animal) => {
    let returnHeat = animal?.heatData
      ? Object.keys(animal.heatData)
          .map((key) => animal.heatData[key])
          .filter((h) => h.heatDate.length > 0)
      : [];
    returnHeat.sort(
      (a, b) =>
        convertStringToDateTimeSeconds(b.heatDate) -
        convertStringToDateTimeSeconds(a.heatDate),
    );
    return returnHeat;
  },
);

export const selectCurrentSelectedAnimalCampHistoryAsArray = createSelector(
  [selectCurrentSelectedAnimal],
  (animal) => {
    let returnCampHistory = animal?.campHistory
      ? Object.keys(animal.campHistory)
          .map((key) => animal.campHistory[key])
          .filter((c) => c.movedOn.length > 0)
      : [];
    returnCampHistory.sort(
      (a, b) =>
        convertStringToDateTimeSeconds(b.dateTime) -
        convertStringToDateTimeSeconds(a.dateTime),
    );
    return returnCampHistory;
  },
);

export const selectAllAnimalsAsArray = createSelector(
  [farmAnimalsSelector],
  (animals) => convertObjectToArray(animals).filter((a) => !a.deleted),
);

export const selectCurrentSelectedAnimalMotherAsArray = createSelector(
  [selectCurrentSelectedAnimal, selectAllAnimalsAsArray],
  (animal, animals) =>
    animal && animals
      ? animals.filter(
          (f) =>
            f.tagNumber === animal.animalMother ||
            f.displayName === animal.animalMother,
        )
      : [],
);

export const selectCurrentSelectedAnimalFatherAsArray = createSelector(
  [selectCurrentSelectedAnimal, selectAllAnimalsAsArray],
  (animal, animals) =>
    animal && animals
      ? animals.filter(
          (f) =>
            f.tagNumber === animal.animalFather ||
            f.displayName === animal.animalFather,
        )
      : [],
);

export const selectCurrentSelectedAnimalSiblingsAsArray = createSelector(
  [selectCurrentSelectedAnimal, selectAllAnimalsAsArray],
  (animal, animals) =>
    animal && animals
      ? animals.filter(
          (f) =>
            ((f.animalFather === animal.animalFather && f.animalFather) ||
              (f.animalMother === animal.animalMother && f.animalMother)) &&
            f.id !== (animal?.id ?? animal?.animalId),
        )
      : [],
);

export const selectCurrentSelectedAnimalChildrenAsArray = createSelector(
  [selectCurrentSelectedAnimal, selectAllAnimalsAsArray],
  (animal, animals) =>
    animal && animals
      ? animals.filter(
          (f) =>
            f.animalFather === animal.displayName ||
            f.animalMother === animal.displayName ||
            f.animalFather === animal.tagNumber ||
            f.animalMother === animal.tagNumber,
        )
      : [],
);

export const selectCurrentCampAnimals = createSelector(
  [selectAllAnimalsAsArray, selectCurrentCamp],
  (animals, camp) =>
    animals.filter((animal) => animal.campId === (camp?.id ?? camp?.campId)),
);

export const selectCurrentCampAnimalsAsArray = createSelector(
  [selectCurrentCampAnimals],
  (animals) => convertObjectToArray(animals),
);

export const selectCurrentActiveCampAnimalsAsArray = createSelector(
  [selectCurrentCampAnimalsAsArray],
  (animals) => animals.filter((a) => !a.deleted),
);

export const selectUserCanModifyAnimal = createSelector(
  //selectActiveSubscriptionsAsArray
  [
    selectCurrentUser,
    selectCurrentSelectedAnimal,
    selectVisibleCampsAsArray,
    selectVisibleFarmsAsArray,
  ],
  (user, animal, camps, farms) => {
    if (!user || !animal || !camps || !farms) return false;
    const animalCamp = camps.filter(
      (c) => animal.campId === (c.id ?? c.campId),
    );
    if (!animalCamp || animalCamp.lenth === 0) return false;
    const animalFarms = farms.filter(
      (f) => animalCamp[0]?.farmId === (f.id ?? f.farmId),
    );
    const userAnimalModifyFarms = animalFarms.filter(
      (f) =>
        f.workingUsers?.includes(user.id ?? user.uid) ||
        f.managingUsers?.includes(user.id ?? user.uid) ||
        f.creatingUserId === (user.id ?? user.uid),
    );

    if (!userAnimalModifyFarms || userAnimalModifyFarms.length === 0)
      return false;

    return true;
  },
);

export const selectUserCanEditAnimal = createSelector(
  //selectActiveSubscriptionsAsArray
  [
    selectCurrentUser,
    selectCurrentSelectedAnimal,
    selectVisibleCampsAsArray,
    selectVisibleFarmsAsArray,
  ],
  (user, animal, camps, farms) => {
    if (!user || !animal || !camps || !farms) return false;
    const animalCamp = camps.filter(
      (c) => animal?.campId === (c.id ?? c.campId),
    );
    if (!animalCamp || animalCamp.lenth === 0) return false;
    const animalFarms = farms.filter(
      (f) => animalCamp[0]?.farmId === (f.id ?? f.farmId),
    );
    const userAnimalModifyFarms = animalFarms.filter(
      (f) =>
        f.managingUsers?.includes(user?.id ?? user.uid) ||
        f.creatingUserId === (user?.id ?? user.uid),
    );

    if (!userAnimalModifyFarms || userAnimalModifyFarms.length === 0)
      return false;

    return true;
  },
);

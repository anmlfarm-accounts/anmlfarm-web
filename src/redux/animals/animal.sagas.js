import { eventChannel } from '@redux-saga/core';
import { all, call, put, takeLatest, take, takeLeading } from 'redux-saga/effects';
import { convertCollectionsSnapshotToMap } from '../../firebase/firebase.functions';
import { firestore } from '../../firebase/firebase.utils';
import campActionTypes from '../camps/camp.action-types';
import { selectCamp } from '../camps/camp.actions';
// import { startImageUpload } from '../images/image.actions';
import animalActionTypes from './animal.action-types';
import {
  addAnimalHeatFailure,
  addAnimalHeatSuccess,
  addAnimalMedicationFailure,
  addAnimalMedicationSuccess,
  addAnimalMilkFailure,
  addAnimalMilkSuccess,
  addAnimalWeightFailure,
  addAnimalWeightSuccess,
  createAnimalFailure,
  createAnimalSuccess,
  deleteAnimalFailure,
  deleteAnimalSuccess,
  deleteLatestAnimalCampHistoryFailure,
  deleteLatestAnimalCampHistorySuccess,
  endAnimalFailure,
  endAnimalSuccess,
  fetchAnimalFailure,
  fetchAnimalFamilyStart,
  fetchAnimalSuccess,
  fetchCampAnimalSuccess,
  setProcessingStart,
  updateAnimalFailure,
  updateAnimalSuccess,
} from './animal.actions';

export function* createNewAnimalAsync(action) {
  const animalDetails = action.payload;
  if (!animalDetails) return;
  try {
    const collectionRef = firestore.collection('animals');
    let addedAnimalRef = null;
    try {
      let updatedAnimalDetails = { ...animalDetails };
      delete updatedAnimalDetails.animalImages;
      addedAnimalRef = yield collectionRef.add(updatedAnimalDetails);
    } catch (error) {
      yield put(createAnimalFailure(error));
    } finally {
      yield put(
        createAnimalSuccess({
          animalId: addedAnimalRef?.id ?? addedAnimalRef.animalId,
          ...animalDetails,
        }),
      );
    }
  } catch (error) {
    yield put(createAnimalFailure(error));
  }
}

export function* updateAnimalDetailsAsync(action) {
  const animalDetails = action.payload;
  if (!animalDetails) return;
  try {
    const animalRef = firestore.doc(
      `animals/${animalDetails?.id ?? animalDetails?.animalId}`,
    );
    const updatedAnimalRef = yield animalRef.get();
    if (updatedAnimalRef.exists) {
      const updatedAnimalDetails = { ...animalDetails };
      delete updatedAnimalDetails.id;
      try {
        delete updatedAnimalDetails.animalImages;
        yield animalRef.update(updatedAnimalDetails);
      } catch (error) {
        yield put(updateAnimalFailure(error));
      } finally {
        yield put(
          updateAnimalSuccess({
            animalId: animalDetails?.id ?? animalDetails?.animalId,
            ...animalDetails,
          }),
        );
      }
    } else {
      yield put(deleteAnimalSuccess(animalDetails));
    }
  } catch (error) {
    yield put(updateAnimalFailure(error));
  }
}

export function* deleteAnimalDetailsAsync(action) {
  const animalDetails = action.payload;
  if (!animalDetails) return;
  try {
    const animalRef = firestore.doc(
      `animals/${animalDetails?.id ?? animalDetails?.animalId}`,
    );
    const updatedAnimalRef = yield animalRef.get();
    if (updatedAnimalRef.exists) {
      const updatedAnimalDetails = { deleted: true };
      delete updatedAnimalDetails.id;
      yield animalRef.update(updatedAnimalDetails);
      yield put(
        deleteAnimalSuccess({
          animalId: animalDetails?.id ?? animalDetails?.animalId,
          ...animalDetails,
          ...updatedAnimalDetails,
        }),
      );
    }
  } catch (error) {
    yield put(deleteAnimalFailure(error));
  }
}

export function* addAnimalWeightAsync(action) {
  const { animalDetails, weightDetails } = action.payload;
  if (!animalDetails || !weightDetails) return;

  try {
    const animalRef = firestore.doc(
      `animals/${animalDetails?.id ?? animalDetails?.animalId}`,
    );
    const snapShot = yield animalRef.get();
    if (snapShot.exists) {
      const animalDoc = yield snapShot.data();
      const weightData = {
        ...animalDoc.weightData,
        [weightDetails.dateTime.toISOString()]: weightDetails,
      };
      yield animalRef.update({
        weightData,
      });
      yield put(addAnimalWeightSuccess(animalDetails, weightData));
    }
  } catch (error) {
    yield put(addAnimalWeightFailure(error));
  }
}

export function* addAnimalMedicationAsync(action) {
  const { animalDetails, medicationDetails } = action.payload;
  if (!animalDetails || !medicationDetails) return;

  try {
    const animalRef = firestore.doc(
      `animals/${animalDetails?.id ?? animalDetails?.animalId}`,
    );
    const snapShot = yield animalRef.get();
    if (snapShot.exists) {
      const animalDoc = yield snapShot.data();
      const medicationData = {
        ...animalDoc.medicationData,
        [medicationDetails.dateTime.toISOString()]: medicationDetails,
      };
      yield animalRef.update({ medicationData });
      yield put(addAnimalMedicationSuccess(animalDetails, medicationData));
    }
  } catch (error) {
    yield put(addAnimalMedicationFailure(error));
  }
}

export function* addAnimalMilkAsync(action) {
  const { animalDetails, milkDetails } = action.payload;
  if (!animalDetails || !milkDetails) return;

  try {
    const animalRef = firestore.doc(
      `animals/${animalDetails?.id ?? animalDetails?.animalId}`,
    );
    const snapShot = yield animalRef.get();
    if (snapShot.exists) {
      const animalDoc = yield snapShot.data();
      const milkData = {
        ...animalDoc.milkData,
        [milkDetails.dateTime.toISOString()]: milkDetails,
      };
      yield animalRef.update({ milkData });
      yield put(addAnimalMilkSuccess(animalDetails, milkData));
    }
  } catch (error) {
    yield put(addAnimalMilkFailure(error));
  }
}

export function* addAnimalHeatAsync(action) {
  const { animalDetails, heatDetails } = action.payload;
  if (!animalDetails || !heatDetails) return;

  try {
    const animalRef = firestore.doc(
      `animals/${animalDetails?.id ?? animalDetails?.animalId}`,
    );
    const snapShot = yield animalRef.get();
    if (snapShot.exists) {
      const animalDoc = yield snapShot.data();
      const heatData = {
        ...animalDoc.heatData,
        [heatDetails.dateTime.toISOString()]: heatDetails,
      };
      yield animalRef.update({ heatData });
      yield put(addAnimalHeatSuccess(animalDetails, heatData));
    }
  } catch (error) {
    yield put(addAnimalHeatFailure(error));
  }
}

export function* endAnimalAsync(action) {
  const { animalDetails, endDetails } = action.payload;
  if (!animalDetails || !endDetails) return;
  try {
    const animalRef = firestore.doc(
      `animals/${animalDetails?.id ?? animalDetails?.animalId}`,
    );
    const snapShot = yield animalRef.get();
    if (snapShot.exists) {
      const animalEnd = { ...endDetails };
      Object.keys(animalEnd).forEach(
        (a) => animalEnd[a] === '' && delete animalEnd[a],
      );
      yield animalRef.update({ animalEnd });
      yield put(endAnimalSuccess(animalDetails, animalEnd));
    }
  } catch (error) {
    yield put(endAnimalFailure(error));
  }
}

export function* deleteLatestAnimalWeightAsync(action) {
  const { animalDetails } = action.payload;
  if (!animalDetails) return;

  try {
    const animalRef = firestore.doc(
      `animals/${animalDetails.id ?? animalDetails.animalId}`,
    );
    const snapShot = yield animalRef.get();
    const weightKeysAsArray = Object.keys(animalDetails.weightData);
    weightKeysAsArray.sort().pop();
    const weightData = Object.keys(animalDetails.weightData)
      .filter((key) => weightKeysAsArray.includes(key))
      .reduce((weightObject, key) => {
        return {
          ...weightObject,
          [key]: animalDetails.weightData[key],
        };
      }, {});
    if (snapShot.exists) {
      yield animalRef.update({ weightData });
      yield put(addAnimalWeightSuccess(animalDetails, weightData));
    }
  } catch (error) {
    yield put(addAnimalWeightFailure(error));
  }
}

export function* deleteLatestAnimalMedicationAsync(action) {
  const { animalDetails } = action.payload;
  if (!animalDetails) return;

  try {
    const animalRef = firestore.doc(
      `animals/${animalDetails.id ?? animalDetails.animalId}`,
    );
    const snapShot = yield animalRef.get();
    const medicationKeysAsArray = Object.keys(animalDetails.medicationData);
    medicationKeysAsArray.sort().pop();
    const medicationData = Object.keys(animalDetails.medicationData)
      .filter((key) => medicationKeysAsArray.includes(key))
      .reduce((medicationObject, key) => {
        return {
          ...medicationObject,
          [key]: animalDetails.medicationData[key],
        };
      }, {});
    if (snapShot.exists) {
      yield animalRef.update({ medicationData });
      yield put(addAnimalMedicationSuccess(animalDetails, medicationData));
    }
  } catch (error) {
    yield put(addAnimalMedicationFailure(error));
  }
}

export function* deleteLatestAnimalMilkAsync(action) {
  const { animalDetails } = action.payload;
  if (!animalDetails) return;

  try {
    const animalRef = firestore.doc(
      `animals/${animalDetails.id ?? animalDetails.animalId}`,
    );
    const snapShot = yield animalRef.get();
    const milkKeysAsArray = Object.keys(animalDetails.milkData);
    milkKeysAsArray.sort().pop();
    const milkData = Object.keys(animalDetails.milkData)
      .filter((key) => milkKeysAsArray.includes(key))
      .reduce((milkObject, key) => {
        return {
          ...milkObject,
          [key]: animalDetails.milkData[key],
        };
      }, {});
    if (snapShot.exists) {
      yield animalRef.update({ milkData });
      yield put(addAnimalMilkSuccess(animalDetails, milkData));
    }
  } catch (error) {
    yield put(addAnimalMilkFailure(error));
  }
}

export function* deleteLatestAnimalHeatAsync(action) {
  const { animalDetails } = action.payload;
  if (!animalDetails) return;

  try {
    const animalRef = firestore.doc(
      `animals/${animalDetails.id ?? animalDetails.animalId}`,
    );
    const snapShot = yield animalRef.get();
    const heatKeysAsArray = Object.keys(animalDetails.heatData);
    heatKeysAsArray.sort().pop();
    const heatData = Object.keys(animalDetails.heatData)
      .filter((key) => heatKeysAsArray.includes(key))
      .reduce((heatObject, key) => {
        return {
          ...heatObject,
          [key]: animalDetails.heatData[key],
        };
      }, {});
    if (snapShot.exists) {
      yield animalRef.update({ heatData });
      yield put(addAnimalHeatSuccess(animalDetails, heatData));
    }
  } catch (error) {
    yield put(addAnimalHeatFailure(error));
  }
}

export function* deleteLatestAnimalCampHistoryAsync(action) {
  const { animalDetails, campId } = action.payload;
  if (!animalDetails || !campId) return;

  try {
    const animalRef = firestore.doc(
      `animals/${animalDetails.id ?? animalDetails.animalId}`,
    );
    const snapShot = yield animalRef.get();
    const campHistoryKeysAsArray = Object.keys(animalDetails.campHistory);
    campHistoryKeysAsArray.sort().pop();
    const campHistory = Object.keys(animalDetails.campHistory)
      .filter((key) => campHistoryKeysAsArray.includes(key))
      .reduce((campHistoryObject, key) => {
        return {
          ...campHistoryObject,
          [key]: animalDetails.campHistory[key],
        };
      }, {});
    if (snapShot.exists) {
      yield animalRef.update({ campHistory, campId });
      yield put(
        deleteLatestAnimalCampHistorySuccess(animalDetails, campHistory),
      );
    }
  } catch (error) {
    yield put(deleteLatestAnimalCampHistoryFailure(error));
  }
}

export function* transferAnimalCampAsync(action) {
  const { animalDetails, campDetails } = action.payload;
  if (!animalDetails || !campDetails) return;

  try {
    const animalRef = firestore.doc(
      `animals/${animalDetails.id ?? animalDetails.animalId}`,
    );
    const snapShot = yield animalRef.get();
    if (snapShot.exists) {
      const animalDoc = yield snapShot.data();
      const campId = campDetails.id ?? campDetails.campId;
      const campHistory = {
        ...animalDoc.campHistory,
        [campDetails.dateTime.toISOString()]: { ...campDetails },
      };
      yield animalRef.update({
        campId,
        campHistory,
      });
      yield put(updateAnimalSuccess({ ...animalDetails, campId, campHistory }));
    }
  } catch (error) {
    yield put(updateAnimalFailure(error));
  }
}

export function* fetchCampAnimals(action) {
  yield put(setProcessingStart());
  const campId = action.payload;
  if (!campId) return;
  try {
    const collectionRef = firestore
      .collection('animals')
      .where('campId', '==', campId);
    const snapshot = yield collectionRef.get();
    const collectionsMap = yield call(
      convertCollectionsSnapshotToMap,
      snapshot,
    );
    yield put(fetchCampAnimalSuccess(collectionsMap));
    
    const animalChannel = eventChannel(emit => collectionRef.onSnapshot(emit))

    try {
      while (true) {
        const data = yield take(animalChannel)
        const dataMap = yield call(
          convertCollectionsSnapshotToMap,
          data,
        );
        yield put(fetchCampAnimalSuccess(dataMap))
      }
    } catch (err) {
      yield put(fetchAnimalFailure(err))
    }


  } catch (error) {
    yield put(fetchAnimalFailure(error.message));
  }
}

export function* fetchCurrentAnimal(action) {
  yield put(setProcessingStart());
  const animalId = action.payload;
  if (!animalId) return;
  try {
    const animalRef = firestore.doc(`animals/${animalId}`);
    const snapShot = yield animalRef.get();
    if (snapShot.exists) {
      const animalDoc = yield snapShot.data();

      yield put(
        fetchAnimalSuccess({
          [animalId]: { ...animalDoc, id: animalId, animalId: animalId },
        }),
      );
      yield put(
        fetchAnimalFamilyStart({
          [animalId]: { ...animalDoc, id: animalId, animalId: animalId },
        }),
      );
      yield put(selectCamp(animalDoc.campId));
    }
  } catch (error) {
    yield put(fetchAnimalFailure(error.message));
  }
}

export function* fetchAnimalMotherAsync(action) {
  const animalDetails = action.payload;
  if (!animalDetails) return;
  if (!animalDetails.animalMother) return;
  try {
    let collectionRef = firestore
      .collection('animals')
      .where('tagNumber', '==', animalDetails.animalMother);
    let snapshot = yield collectionRef.get();
    if (snapshot.empty) {
      collectionRef = firestore
        .collection('animals')
        .where('displayName', '==', animalDetails.animalMother);

      snapshot = yield collectionRef.get();
    }
    const collectionsMap = yield call(
      convertCollectionsSnapshotToMap,
      snapshot,
    );
    yield put(fetchAnimalSuccess(collectionsMap));
  } catch (error) {
    yield put(fetchAnimalFailure(error.message));
  }
}

export function* fetchAnimalFatherAsync(action) {
  const animalDetails = action.payload;
  if (!animalDetails) return;
  if (!animalDetails.animalFather) return;
  try {
    let collectionRef = firestore
      .collection('animals')
      .where('tagNumber', '==', animalDetails.animalFather);
    let snapshot = yield collectionRef.get();
    if (snapshot.empty) {
      collectionRef = firestore
        .collection('animals')
        .where('displayName', '==', animalDetails.animalFather);

      snapshot = yield collectionRef.get();
    }
    const collectionsMap = yield call(
      convertCollectionsSnapshotToMap,
      snapshot,
    );
    yield put(fetchAnimalSuccess(collectionsMap));
  } catch (error) {
    yield put(fetchAnimalFailure(error.message));
  }
}

export function* fetchAnimalSiblingsAsync(action) {
  const animalDetails = action.payload;
  if (!animalDetails) return;
  if (!animalDetails.animalFather && !animalDetails.animalMother) return;
  try {
    if (animalDetails.animalMother) {
      const collectionMotherRef = firestore
        .collection('animals')
        .where('animalMother', '==', animalDetails.animalMother);
      const motherSnapshot = yield collectionMotherRef.get();
      const motherCollectionsMap = yield call(
        convertCollectionsSnapshotToMap,
        motherSnapshot,
      );
      yield put(fetchAnimalSuccess(motherCollectionsMap));
    }
    if (animalDetails.animalFather) {
      const collectionFatherRef = firestore
        .collection('animals')
        .where('animalFather', '==', animalDetails.animalFather);
      const fatherSnapshot = yield collectionFatherRef.get();
      const fatherCollectionsMap = yield call(
        convertCollectionsSnapshotToMap,
        fatherSnapshot,
      );
      yield put(fetchAnimalSuccess(fatherCollectionsMap));
    }
  } catch (error) {
    yield put(fetchAnimalFailure(error.message));
  }
}

export function* fetchAnimalFamily(action) {
  const animalDetails = action.payload;
  if (!animalDetails) return;
  yield call(fetchAnimalMotherAsync, animalDetails);
  yield call(fetchAnimalFatherAsync, animalDetails);
  yield call(fetchAnimalSiblingsAsync, animalDetails);
}

export function* onCampSelected() {
  yield takeLatest(campActionTypes.SELECT_CAMP, fetchCampAnimals);
}

export function* onAnimalCreate() {
  yield takeLatest(animalActionTypes.CREATE_ANIMAL_START, createNewAnimalAsync);
}
export function* onAnimalSelected() {
  yield takeLatest(animalActionTypes.SET_SELECTED_ANIMAL, fetchCurrentAnimal);
}

export function* onAnimalFieldUpdate() {
  yield takeLatest(
    animalActionTypes.UPDATE_ANIMAL_START,
    updateAnimalDetailsAsync,
  );
}

export function* onAnimalWeightAdd() {
  yield takeLatest(
    animalActionTypes.ADD_ANIMAL_WEIGHT_START,
    addAnimalWeightAsync,
  );
}

export function* onAnimalMedicationAdd() {
  yield takeLatest(
    animalActionTypes.ADD_ANIMAL_MEDICATION_START,
    addAnimalMedicationAsync,
  );
}

export function* onAnimalMilkAdd() {
  yield takeLatest(animalActionTypes.ADD_ANIMAL_MILK_START, addAnimalMilkAsync);
}

export function* onAnimalHeatAdd() {
  yield takeLatest(animalActionTypes.ADD_ANIMAL_HEAT_START, addAnimalHeatAsync);
}

export function* onAnimalEnd() {
  yield takeLatest(animalActionTypes.END_ANIMAL_START, endAnimalAsync);
}

export function* onAnimalWeightLatestDelete() {
  yield takeLatest(
    animalActionTypes.DELETE_LATEST_ANIMAL_WEIGHT_START,
    deleteLatestAnimalWeightAsync,
  );
}

export function* onAnimalMedicationLatestDelete() {
  yield takeLatest(
    animalActionTypes.DELETE_LATEST_ANIMAL_MEDICATION_START,
    deleteLatestAnimalMedicationAsync,
  );
}

export function* onAnimalMilkLatestDelete() {
  yield takeLatest(
    animalActionTypes.DELETE_LATEST_ANIMAL_MILK_START,
    deleteLatestAnimalMilkAsync,
  );
}

export function* onAnimalHeatLatestDelete() {
  yield takeLatest(
    animalActionTypes.DELETE_LATEST_ANIMAL_HEAT_START,
    deleteLatestAnimalHeatAsync,
  );
}

export function* onAnimalCampHistoryLatestDelete() {
  yield takeLatest(
    animalActionTypes.DELETE_LATEST_ANIMAL_CAMP_HISTORY_START,
    deleteLatestAnimalCampHistoryAsync,
  );
}

export function* onAnimalCampTransfer() {
  yield takeLatest(
    animalActionTypes.TRANSFER_ANIMAL_CAMP_START,
    transferAnimalCampAsync,
  );
}

export function* onAnimalFamilyFetch() {
  yield takeLatest(
    animalActionTypes.FETCH_ANIMAL_FAMILY_START,
    fetchAnimalFamily,
  );
}

export function* onAnimalDelete() {
  yield takeLatest(
    animalActionTypes.DELETE_ANIMAL_START,
    deleteAnimalDetailsAsync,
  );
}

export function* animalSagas() {
  yield all([
    call(onCampSelected),
    call(onAnimalSelected),
    call(onAnimalCreate),
    call(onAnimalWeightAdd),
    call(onAnimalMedicationAdd),
    call(onAnimalMilkAdd),
    call(onAnimalHeatAdd),
    call(onAnimalEnd),
    call(onAnimalWeightLatestDelete),
    call(onAnimalMedicationLatestDelete),
    call(onAnimalMilkLatestDelete),
    call(onAnimalHeatLatestDelete),
    call(onAnimalCampHistoryLatestDelete),
    call(onAnimalCampTransfer),
    call(onAnimalFieldUpdate),
    call(onAnimalDelete),
  ]);
}

import { omit } from "lodash";
import animalActionTypes from "./animal.action-types";
import ANIMAL_DETAILS_INITIAL_STATE from "./animal.state";
import { mapStringToFilters } from "./animal.utils";
import { replaceCampAnimals } from "./animal.utils";

const animalReducer = (state = ANIMAL_DETAILS_INITIAL_STATE, action) => {
  const { type, payload } = action;
  const {
    animalDetails,
    weightDetails,
    medicationDetails,
    milkDetails,
    heatDetails,
    campHistory,
    endDetails,
  } = payload ?? {
    animalDetails: null,
    weightDetails: null,
    medicationDetails: null,
    milkDetails: null,
    heatDetails: null,
    campHistory: null,
    endDetails: null,
  };
  let animalId = animalDetails?.id ?? animalDetails?.animalId;
  if (!animalId) animalId = payload?.animalId ?? payload?.id;

  switch (type) {
    case animalActionTypes.SET_PROCESSING_START:
      return {
        ...state,
        processing: true,
      };

    case animalActionTypes.CLEAR_FILTERS:
      return {
        ...state,
        animalFilters: {},
      };

    case animalActionTypes.SET_SELECTED_ANIMAL:
      return {
        ...state,
        selectedAnimalId: payload,
      };

    case animalActionTypes.UPDATE_FILTERS:
      return {
        ...state,
        animalFilters: mapStringToFilters(payload),
      };

    case animalActionTypes.CREATE_ANIMAL_START:
      return {
        ...state,
        processing: true,
      };

    case animalActionTypes.CREATE_ANIMAL_FAILURE:
      return {
        ...state,
        error: payload,
        processing: false,
      };

    case animalActionTypes.FETCH_ANIMAL_START:
    case animalActionTypes.UPDATE_ANIMAL_START:
    case animalActionTypes.ADD_ANIMAL_WEIGHT_START:
    case animalActionTypes.ADD_ANIMAL_HEAT_START:
      return {
        ...state,
        processing: true,
      };

    case animalActionTypes.FETCH_ANIMAL_FAILURE:
    case animalActionTypes.ADD_ANIMAL_WEIGHT_FAILURE:
      return {
        ...state,
        error: payload,
        processing: false,
      };

    case animalActionTypes.CREATE_ANIMAL_SUCCESS:
      return {
        ...state,
        processing: false,
        visibleAnimals: {
          ...state.visibleAnimals,
          [animalId]: payload,
        },
        selectedAnimal: animalId ?? state.selectedAnimal,
      };

    case animalActionTypes.UPDATE_ANIMAL_SUCCESS:
    case animalActionTypes.ADD_ANIMAL_IMAGES_SUCCESS:
      return {
        ...state,
        processing: false,
        visibleAnimals: {
          ...state.visibleAnimals,
          [animalId]: payload,
        },
        selectedAnimal: animalId ?? state.selectedAnimal,
      };

    case animalActionTypes.ADD_ANIMAL_WEIGHT_SUCCESS:
      return {
        ...state,
        processing: false,
        visibleAnimals: {
          ...state.visibleAnimals,
          [animalId]: {
            ...state.visibleAnimals[animalId],
            weightData: weightDetails,
          },
        },
        selectedAnimal: animalId ?? state.selectedAnimal,
      };

    case animalActionTypes.ADD_ANIMAL_MEDICATION_SUCCESS:
      return {
        ...state,
        processing: false,
        visibleAnimals: {
          ...state.visibleAnimals,
          [animalId]: {
            ...state.visibleAnimals[animalId],
            medicationData: medicationDetails,
          },
        },
        selectedAnimal: animalId ?? state.selectedAnimal,
      };

    case animalActionTypes.ADD_ANIMAL_MILK_SUCCESS:
      return {
        ...state,
        processing: false,
        visibleAnimals: {
          ...state.visibleAnimals,
          [animalId]: {
            ...state.visibleAnimals[animalId],
            milkData: milkDetails,
          },
        },
        selectedAnimal: animalId ?? state.selectedAnimal,
      };

    case animalActionTypes.ADD_ANIMAL_HEAT_SUCCESS:
      return {
        ...state,
        processing: false,
        visibleAnimals: {
          ...state.visibleAnimals,
          [animalId]: {
            ...state.visibleAnimals[animalId],
            heatData: heatDetails,
          },
        },
        selectedAnimal: animalId ?? state.selectedAnimal,
      };

    case animalActionTypes.END_ANIMAL_SUCCESS:
      return {
        ...state,
        processing: false,
        visibleAnimals: {
          ...state.visibleAnimals,
          [animalId]: {
            ...state.visibleAnimals[animalId],
            animalEnd: endDetails,
          },
        },
        selectedAnimal: animalId ?? state.selectedAnimal,
      };

    case animalActionTypes.DELETE_LATEST_ANIMAL_CAMP_HISTORY_SUCCESS:
      return {
        ...state,
        processing: false,
        visibleAnimals: {
          ...state.visibleAnimals,
          [animalId]: {
            ...state.visibleAnimals[animalId],
            campHistory: campHistory,
          },
        },
        selectedAnimal: animalId ?? state.selectedAnimal,
      };

    case animalActionTypes.FETCH_ANIMAL_SUCCESS:
      return {
        ...state,
        processing: false,
        visibleAnimals: {
          ...state.visibleAnimals,
          ...payload,
        },
      };

    case animalActionTypes.FETCH_CAMP_ANIMAL_SUCCESS:
      return {
        ...state,
        processing: false,
        visibleAnimals: replaceCampAnimals(state.visibleAnimals, payload),
      };

    case animalActionTypes.DELETE_ANIMAL_SUCCESS:
      return {
        ...state,
        processing: false,
        visibleAnimals: {
          ...omit(state.visibleAnimals, animalId),
        },
        selectedAnimal: state.visibleAnimals[0]
          ? state.visibleAnimals[0]?.id
          : "",
      };

    default:
      return state;
  }
};

export default animalReducer;

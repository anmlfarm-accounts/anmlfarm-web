import { takeLatest, put, all, call, takeLeading } from 'redux-saga/effects';

import UserActionTypes from './user.types';
import { firestore } from '../../firebase/firebase.utils';
import { auth, googleProvider } from '../../firebase/firebase.utils';
import {
  convertCollectionsSnapshotToMap,
  createUserProfileDocument,
  getCurrentUser,
} from '../../firebase/firebase.functions';
import {
  emailSignUpFailed,
  signInFailed,
  signInSuccess,
  signOutFailed,
  signOutSuccess,
  updateUserFailure,
  updateUserSuccess,
  visibleUserFetchFailure,
  visibleUserFetchSuccess,
} from './user.actions';

export function* getSnapshotFromUserAuth(userAuth) {
  try {
    const userProfileRef = yield call(createUserProfileDocument, userAuth);
    const userSnapshot = yield userProfileRef.get();
    yield put(signInSuccess({ id: userSnapshot.id, ...userSnapshot.data() }));
  } catch (error) {
    yield put(signInFailed(error));
  }
}

export function* signInWithGoogle() {
  try {
    const userRef = yield auth.signInWithPopup(googleProvider);
    yield getSnapshotFromUserAuth(userRef.user);
  } catch (error) {
    yield put(signInFailed(error));
  }
}

export function* onGoogleSignInStart() {
  yield takeLatest(UserActionTypes.GOOGLE_SIGN_IN_START, signInWithGoogle);
}

export function* signInWithEmailAndPassword({ payload: { email, password } }) {
  try {
    const userRef = yield auth.signInWithEmailAndPassword(email, password);
    yield getSnapshotFromUserAuth(userRef.user);
  } catch (error) {
    yield put(signInFailed(error));
  }
}

export function* onEmailSignInStart() {
  yield takeLatest(
    UserActionTypes.EMAIL_SIGN_IN_START,
    signInWithEmailAndPassword,
  );
}

export function* signUpWithEmailAndPassword({
  payload: { email, password, displayName },
}) {
  try {
    const userRef = yield auth.createUserWithEmailAndPassword(email, password);
    yield getSnapshotFromUserAuth({ ...userRef.user, displayName });
  } catch (error) {
    yield put(emailSignUpFailed(error));
  }
}

export function* onEmailSignUpStart() {
  yield takeLatest(
    UserActionTypes.EMAIL_SIGN_UP_START,
    signUpWithEmailAndPassword,
  );
}

export function* checkUserAuthenticated() {
  try {
    const userAuth = yield getCurrentUser();
    if (!userAuth) return;
    yield getSnapshotFromUserAuth(userAuth);
  } catch (error) {
    yield put(signInFailed(error));
  }
}

export function* onUserSessionCheck() {
  yield takeLeading(UserActionTypes.CHECK_USER_SESSION, checkUserAuthenticated);
}

export function* signOutUser() {
  try {
    yield auth.signOut();
    yield put(signOutSuccess());
  } catch (error) {
    yield put(signOutFailed(error));
  }
}

export function* onSignOutStart() {
  yield takeLatest(UserActionTypes.SIGN_OUT_START, signOutUser);
}

export function* updateUserDetailsAsync(action) {
  const userDetails = action.payload;
  if (!userDetails) return;
  try {
    const userRef = firestore.doc(`users/${userDetails.id}`);
    const updatedUserRef = yield userRef.get();
    if (updatedUserRef.exists) {
      const updatedUserDetails = { ...userDetails };
      delete updatedUserDetails.userId;
      delete updatedUserDetails.id;
      yield userRef.update(updatedUserDetails);
      yield put(updateUserSuccess({ userId: userDetails.id, ...userDetails }));
    }
  } catch (error) {
    yield put(updateUserFailure(error));
  }
}

export function* addUserTokenAsync(action) {
  const { user, token } = action.payload;
  if (!user || !token) return;
  if (user.messageTokens && user.messageTokens.includes(token)) return;
  try {
    const userRef = firestore.doc(`users/${user?.id ?? user?.uid}`);
    const snapShot = yield userRef.get();
    if (snapShot.exists) {
      const userDoc = yield snapShot.data();
      const messageTokens = userDoc.messageTokens
        ? [...userDoc.messageTokens]
        : [];
      if (!messageTokens.includes(token)) messageTokens.push(token);
      yield userRef.update({
        messageTokens,
      });
      yield put(
        updateUserSuccess({ userId: snapShot.id, ...user, messageTokens }),
      );
    }
  } catch (error) {
    yield put(updateUserFailure(error));
  }
}

export function* onUserUpdate() {
  yield takeLatest(
    UserActionTypes.PROFILE_UPDATE_START,
    updateUserDetailsAsync,
  );
}

export function* fetchUserByEmail(action) {
  const email = action.payload;
  if (!email) return;
  try {
    const collectionRef = firestore
      .collection('users')
      .where('email', '==', email.toLowerCase());
    const snapshot = yield collectionRef.get();
    const collectionsMap = yield call(
      convertCollectionsSnapshotToMap,
      snapshot,
    );
    yield put(visibleUserFetchSuccess(collectionsMap));
  } catch (error) {
    yield put(visibleUserFetchFailure(error.message));
  }
}

export function* onUserEmailSearch() {
  yield takeLatest(UserActionTypes.CHECK_USER_EMAIL_EXISTS, fetchUserByEmail);
}

export function* onTokenUpdate() {
  yield takeLatest(UserActionTypes.TOKEN_UPDATE_START, addUserTokenAsync);
}

export function* userSagas() {
  yield all([
    call(onGoogleSignInStart),
    call(onEmailSignInStart),
    call(onUserSessionCheck),
    call(onSignOutStart),
    call(onEmailSignUpStart),
    call(onUserUpdate),
    call(onUserEmailSearch),
    call(onTokenUpdate),
  ]);
}

import { createSelector } from 'reselect'
import { convertObjectToArray } from '../../utils/array/mapper'

const selectUser = state => state.user

export const selectCurrentUser = createSelector(
    [selectUser],
    (user) => (user?.currentUser)
)

export const selectRepCode = createSelector(
    [selectUser],
    (user) => (user?.currentUser?.repCode ?? user?.repCode)
)

export const selectVisibleUsers = createSelector(
    [selectUser],
    (user) => (user?.visibleUsers)
)

export const selectVisibleUsersAsArray = createSelector(
    [selectVisibleUsers],
    (users) => convertObjectToArray(users)
)

export const selectCurrentUserOnboarded = createSelector(
    [selectCurrentUser],
    (currentUser) => (currentUser?.hasDoneOnboarding)
)


export const selectCurrentUserPrivacy = createSelector(
    [selectCurrentUser],
    (currentUser) => (currentUser?.hasDonePrivacy)
)


export const selectCurrentUserTerms = createSelector(
    [selectCurrentUser],
    (currentUser) => (currentUser?.hasDoneTerms)
)

export const selectUserByEmail = (email) =>
createSelector([selectVisibleUsersAsArray], (users) =>
  users ? users.filter(u=>u.email.toLowerCase()===email.toLowerCase()) : [],
);
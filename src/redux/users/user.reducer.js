import UserActionTypes from './user.types';

const INITIAL_STATE = {
  visibleUsers: {},
  currentUser: null,
  repCode: null,
  error: null,
};

const userReducer = (state = INITIAL_STATE, action) => {
  const { type, payload } = action;
  switch (type) {
    case UserActionTypes.SIGN_IN_SUCCESS:
      return {
        ...state,
        currentUser: payload,
        repCode: state.repCode ?? payload?.repCode,
        error: null,
      };

    case UserActionTypes.SIGN_OUT_SUCCESS:
      return INITIAL_STATE;

    case UserActionTypes.EMAIL_SIGN_UP_FAILED:
    case UserActionTypes.SIGN_IN_FAILED:
    case UserActionTypes.SIGN_OUT_FAILED:
      return {
        ...state,
        error: payload,
      };

    case UserActionTypes.SET_PROFILE_COMPLETE:
      return {
        ...state,
        currentUser: {
          ...state.currentUser,
          hasDoneProfile: true,
        },
      };

    case UserActionTypes.SET_TERMS_ACCEPTED:
      return {
        ...state,
        currentUser: {
          ...state.currentUser,
          hasDoneTerms: true,
        },
      };

    case UserActionTypes.SET_ONBOARDING_DONE:
      return {
        ...state,
        currentUser: {
          ...state.currentUser,
          hasDoneOnboarding: true,
        },
      };

    case UserActionTypes.PROFILE_UPDATE_SUCCESS:
      return {
        ...state,
        currentUser: payload,
        repCode: state.repCode ?? payload?.repCode,
        error: null,
      };

    case UserActionTypes.CHECK_USER_EMAIL_EXISTS:
        return {
            ...state,
            processing: true
        }

    case UserActionTypes.VISIBLE_USER_FETCH_FAILED:
        return {
            ...state,
            error: payload,
            processing: false
        }

    case UserActionTypes.VISIBLE_USER_FETCH_SUCCESS:
        return {
            ...state,
            processing: false,
            visibleUsers: {
                ...state.visibleUsers,
                ...payload
            }
        }

    case UserActionTypes.REP_UPDATE_START:
        return {
            ...state,
            processing: false,
            currentUser: {
                ...state.currentUser,
                repCode: payload
            },
            repCode: payload
        }

    default:
      return state;
  }
};

export default userReducer;

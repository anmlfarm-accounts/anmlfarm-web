import { createSelector } from 'reselect';
import { convertObjectToArray } from '../../utils/array/mapper';

export const selectOrders = (state) => state.orders;

export const selectCurrentOrders = createSelector(
  [selectOrders],
  (o) => o.orders
);

export const selectAllOrdersAsArray = createSelector(
  [selectCurrentOrders],
  (orders) => convertObjectToArray(orders),
);

export const selectHistoricOrdersAsArray = createSelector(
  [selectAllOrdersAsArray],
  (orders) => orders.filter(o=>(o.paymentSuccess || o.paymentFailure)),
);

export const selectActiveOrdersAsArray = createSelector(
  [selectAllOrdersAsArray],
  (orders) => orders.filter(o=>(!o.paymentSuccess && !o.paymentFailure)),
);

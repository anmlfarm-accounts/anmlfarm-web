const INITIAL_STATE = {
    Meatmaster: {
        breedName: 'Meatmaster',
        breedLocations: {
            
        },
        inspectors: {
            '0686266MMS': {
                geolocation: {
                    ltd: -24.7020463,
                    lng: 28.4057092
                }
            }
        },
        breedWebsite: 'https://meatmastersa.co.za'
    }
}
import { createSelector } from 'reselect';
import { convertObjectToArray } from '../../utils/array/mapper';
import {selectCurrentSelectedAnimal
} from '../animals/animal.selectors';

export const selectDevices = (state) => state.devices;

export const selectCurrentDevice = createSelector(
  [selectDevices],
  (deviceDetais) => deviceDetais.visibleDevices[deviceDetais.selectedDevice],
);

export const selectVisibleDevices = createSelector(
  [selectDevices],
  (devices) => devices.visibleDevices,
);

export const selectConnectedDevicesArray = createSelector(
  [selectVisibleDevices],
  (devices) => convertObjectToArray(devices),
);

export const selectCurrentAnimalDevices = createSelector(
  [selectCurrentSelectedAnimal, selectConnectedDevicesArray],
  (animal, devices) => devices.filter((d) => d.animalIds.includes(animal?.id ?? animal?.animalId)),
);

export const selectDeviceById = (deviceId) =>
  createSelector([selectVisibleDevices], (devices) =>
    devices ? devices[deviceId] : null,
  );

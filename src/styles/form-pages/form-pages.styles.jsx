import styled from 'styled-components';
import { Grid } from '@material-ui/core';

export const FormIntro = styled(Grid)`
  justify-content: center;
  align-items: center;
  justify-items: center;
  margin: 10px auto;
`;

export const FormImage = styled.img`
  width: 100%;
`;

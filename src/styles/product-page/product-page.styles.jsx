import styled from 'styled-components';
import { Grid, ListItem } from '@material-ui/core';

export const CartListInfo = styled(Grid)`
  display: flex;
  flex-direction: column;
  justify-content: space-evenly;
`;

export const CartListItem = styled(ListItem)`
  border-bottom: 1px solid #eeeeee;
`;

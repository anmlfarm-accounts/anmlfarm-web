import styled from 'styled-components';
import { Card, Grid } from '@material-ui/core';
import { GriDisplayFlexCenter } from '../shared/shared.styles';

export const BoldMainColor = styled.p`
  color: #7fb986;
  font-size: 13px;
  font-weight: bold;
`;

export const FeedText = styled.div`
  display: flex;
  font-size: 13px;
  padding: 5px;
`;

export const EventText = styled.div`
  color: gray;
  font-size: 13px;
  padding-bottom: 5px;
`;

export const FaqText = styled(EventText)`
  padding: 5px 0;
`;

export const GriDisplayFlexAround = styled(Grid)`
  display: flex;
  justify-content: space-around;
  align-items: center;
`;

export const CardMarginX10 = styled(Card)`
  margin: 10px 0;
`;

export const EventCurrentHeader = styled(Grid)`
  display: flex;
  justify-content: space-between;
  align-items: center;
  border-top: 1px solid #e4e4e473;
  padding-top: 5px;
`;

export const SidebarItem = styled(GriDisplayFlexCenter)`
  padding: 5px 0;
  margin: 5px 0;
`;

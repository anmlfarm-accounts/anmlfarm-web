import styled from 'styled-components';
import { Card, Container, Grid, Typography } from '@material-ui/core';

export const Purpose = styled.div`
  font-weight: bold;
  text-align: center;
  color: #7fb986;
  margin-top: 10px;
`;

export const CardItem = styled(Card)`
  margin: 10px;
  padding: 5px;
  @media (max-width: 600px) {
    margin: 10px 0;
    padding: 5px 0;
  }
`;

export const ListImgWrap = styled(Grid)`
  display: flex;
  justify-content: flex-start;
  align-items: center;
`;

export const ListDescription = styled(Grid)`
  display: flex;
  flex-direction: column;
  justify-content: center;
`;
export const FarmListContainer = styled(Container)`
  padding: 0;
`;

export const ListName = styled(Typography)`
  // font-size: 1.25rem;
  @media (max-width: 600px) {
    font-size: 1rem;
  }
`;

export const ListInfo = styled.div`
  display: flex;
  align-items: center;
  font-size: 11px;
  padding: 5px 0;
`;

export const ListImg = styled.img`
  width: 80px;
  height: 80px;
  border-radius: 50%;
  @media (max-width: 600px) {
    width: 48px;
    height: 48px;
  }
`;

export const ListImageDiv = styled(Grid)`
  max-width: 80px;
  max-height: 80px;
  @media (max-width: 600px) {
    margin-left: 8px;
  }
`;

export const ListNotifications = styled(Grid)`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

import styled from 'styled-components';
import { Grid } from '@material-ui/core';

export const ProfileMain = styled(Grid)`
  display: flex;
  margin: 10px auto;
`;

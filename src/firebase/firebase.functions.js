import { auth, firestore, googleProvider, storage, vapidKey, messaging } from './firebase.utils';

export const getCurrentUser = () => {
  return new Promise((resolve, reject) => {
    const unsubscribe = auth.onAuthStateChanged((userAuth) => {
      unsubscribe();
      resolve(userAuth);
    }, reject);
  });
};

export const signInWithGoogle = async () =>
  await auth.signInWithPopup(googleProvider);
export const signInWithEmailAndPassword = async ({ email, password }) => {
  return await auth.signInWithEmailAndPassword(email, password);
};
export const signOut = () => auth.signOut();

export const createUserWithEmailAndPassword = async (email, password) => {
  return await auth.createUserWithEmailAndPassword(email, password);
};

export const updateFirebaseUserField = async (
  userAuth,
  fieldKey,
  fieldValue,
) => {
  if (!userAuth) return;
  const userRef = firestore.doc(`users/${userAuth.uid}`);
  const snapShot = await userRef.get();
  if (snapShot.exists) {
    try {
      await userRef.update({
        [fieldKey]: fieldValue,
      });
      return true;
    } catch (error) {
      console.log('ERROR CREATING USER', error.message);
      return false;
    }
  }
};

export const createUserProfileDocument = async (userAuth, additionalData) => {
  if (!userAuth) return;

  const userRef = firestore.doc(`users/${userAuth.uid}`);
  const snapShot = await userRef.get();

  if (!snapShot.exists) {
    const { photoURL, uid, emailVerified, email, displayName } = userAuth;
    try {
      await userRef.set({
        photoURL,
        uid,
        emailVerified,
        email,
        displayName,
        createDate: new Date(),
        hasDoneOnboarding: false,
        hasDoneProfile: false,
        hasDoneTerms: false,
        ...additionalData,
      });
    } catch (error) {
      console.log('ERROR CREATING USER', error.message);
    }
  }

  return userRef;
};

export const addCollectionAndDocuments = async (
  collectionKey,
  objectsToAdd,
) => {
  const collectionRef = firestore.collection(collectionKey);
  const batch = firestore.batch();
  objectsToAdd.forEach((element) => {
    const newDocRef = collectionRef.doc();
    batch.set(newDocRef, element);
  });
  return await batch.commit();
};

export const convertCollectionsSnapshotToMap = (collections) => {
  const transformedCollection = collections.docs.filter((a) => !a.deleted).map((doc) => {
    return {
      id: doc.id,
      ...doc.data(),
    };
  });
  return transformedCollection.reduce((acc, col) => {
    acc[col.id] = col;
    return acc;
  }, {});
};

export const uploadFileToStorage = (data, path) => {
  if(data.includes('base64')) return uploadDataURLFileToStorage(data, path);
  return new Promise((resolve, reject) => {
    try{
      const storeRef = storage.ref();
      const fileRef = storeRef.child(path);
      
      fileRef.put(data).then((snapshot)=>{
        switch(snapshot.state){
          case "success":
            resolve(snapshot);
            break;

          default:
            console.log("File upload at state ", snapshot.state);
          }
        },
        reject);
      }
    catch (error) {
        reject(error);
    }
  });
}

export const uploadDataURLFileToStorage = (data, path) => {
  return new Promise((resolve, reject) => {
    try{
      const storeRef = storage.ref();
      const fileRef = storeRef.child(path);
      
      fileRef.putString(data, 'data_url').then((snapshot)=>{
        switch(snapshot.state){
          case "success":
            resolve(snapshot);
            break;

            default:
              console.log("File upload at state ", snapshot.state);
        }
      },
      reject);
    }
    catch (error) {
      reject(error);
    }
  });
}

export const getToken = (setTokenFound) => {
  if(messaging) {
    return messaging.getToken({vapidKey}).then((currentToken) => {
      if (currentToken) {
        if(setTokenFound){setTokenFound(currentToken)} else return currentToken;
      } else {
        console.log('No registration token available. Request permission to generate one.');
        setTokenFound(null);
        if(setTokenFound){setTokenFound(null)} else return null;
      }
    }).catch((err) => {
      console.log('An error occurred while retrieving token. ', err);
    });
  }
}

export const onMessageListener = () =>
  new Promise((resolve) => {
    if(messaging){
      messaging.onMessage((payload) => {
        resolve(payload);
      });
    }
});
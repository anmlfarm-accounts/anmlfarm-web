import firebase from "firebase/app";
import 'firebase/firestore'
import 'firebase/storage'
import 'firebase/auth'
import 'firebase/analytics'
import 'firebase/messaging';

const firebaseConfig = {
  apiKey: "AIzaSyDoycCx1GYeeZKvaNFdo1uZOSQPmS5jqvQ",
  authDomain: "anmlfarm-web.firebaseapp.com",
  databaseURL: "https://anmlfarm-web-default-rtdb.europe-west1.firebasedatabase.app",
  projectId: "anmlfarm-web",
  storageBucket: "anmlfarm-web.appspot.com",
  messagingSenderId: "149496621239",
  appId: "1:149496621239:web:354464786c0ed0d8ecc532",
  measurementId: "G-3KFJ1FX256",
};

export const mapboxMapConfig = `pk.eyJ1IjoidGhlamVhbmxvdXciLCJhIjoiY2tiam1kdzlrMHFydTJ4bHMyNXlybHBtaCJ9.zo2T40wde0uOC5g7m1LpHw`;
export const vapidKey = 'BLQV6E7zcuJ7uMAuZne3pbgMlh1gu_e-o0jekSyDme3X6LXGfWbrkP3WyPQ7V1wMwg1U_G9lgpdVUe3uoHlhYbM';

firebase.initializeApp(firebaseConfig);
export const storage = firebase.storage();
export const auth = firebase.auth();
export const firestore = firebase.firestore();
export const firelytics = firebase.analytics();

export const messaging = firebase.messaging.isSupported() ? firebase.messaging() : null;
try{
  firestore.enablePersistence();
} catch(err) {
  console.log(err)
}
export default firebase;

export const googleProvider = new firebase.auth.GoogleAuthProvider();
googleProvider.setCustomParameters({
    prompt: 'select_account'
})

export const arrayRemove = firebase.firestore.FieldValue.arrayRemove;
export const arrayUnion = firebase.firestore.FieldValue.arrayUnion;

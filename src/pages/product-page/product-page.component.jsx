import {
  ShoppingBasket,
  Store,
  Loyalty,
  Receipt,
  Payment,
} from '@material-ui/icons';
import React, { useState } from 'react';
import { useEffect } from 'react';
import { connect } from 'react-redux';
import ProductCategories from '../../components/products/product-categories/product-categories.component';
import ProductCheckout from '../../components/products/product-checkout/product-checkout.component';
import { fetchProducts } from '../../redux/products/product.actions';
import MainPage from '../main-page/main-page.component';
import { Box, Tab } from '@material-ui/core';
import { TabContext, TabList, TabPanel } from '@material-ui/lab';
import { MainColor } from '../../styles/shared/shared.styles';
import ProductBilling from '../../components/products/product-billing/product-billing.component';
import ProductOrderHistory from '../../components/products/product-order-history/product-order-history.component';
import { createStructuredSelector } from 'reselect';
import { selectActiveOrdersAsArray } from '../../redux/orders/order.selectors';
import FarmSubscriptionList from '../../components/farms/farm-subscription-list/farm-subscription-list.component';
import { withRouter } from 'react-router-dom';

const PRODUCT_PAGE_TABS = [
  {
    value: '1',
    icon: <Store />,
    label: 'Store',
    disabled: false,
    component: <ProductCategories />,
  },
  // {
  //   value: '2',
  //   icon: <Money />,
  //   label: 'Market',
  //   disabled: false,
  //   component: <ProductBilling />,
  // },
  {
    value: '2',
    icon: <Loyalty />,
    label: 'Subscriptions',
    disabled: false,
    component: <FarmSubscriptionList />,
  },
  {
    value: '4',
    icon: <Receipt />,
    label: 'History',
    disabled: false,
    component: <ProductOrderHistory />,
  },
];

function ProductPage({ match, fetchProducts, activeOrders }) {
  const [value, setValue] = useState('1');
  const [tabs, setTabs] = useState(PRODUCT_PAGE_TABS);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  useEffect(() => {
    fetchProducts();
  }, [fetchProducts]);

  useEffect(() => {
    tabs.forEach((tab) => {
      if (
        window?.location
          ?.toString()
          .toLowerCase()
          .includes(tab.label.toLowerCase())
      ) {
        setValue(tab.value);
      }
    });
  }, [tabs, setValue]);

  useEffect(() => {
    const tabthree = tabs.find((t) => t.value === '3');
    const currentTabs = [...tabs];
    if (activeOrders.length > 0 && !tabthree) {
      currentTabs.push({
        value: '3',
        icon: <Payment />,
        label: 'Payment',
        disabled: false,
        component: <ProductBilling />,
      });
      currentTabs.sort((a, b) => (a.value > b.value ? 1 : -1));
      setTabs(currentTabs);
    } else if (!tabthree) {
      currentTabs.push({
        value: '3',
        icon: <ShoppingBasket />,
        label: 'Checkout',
        disabled: false,
        component: <ProductCheckout />,
      });
      currentTabs.sort((a, b) => (a.value > b.value ? 1 : -1));
      setTabs(currentTabs);
    } else if (
      (tabthree.label === 'Checkout' && activeOrders.length > 0) ||
      (tabthree.label === 'Payment' && activeOrders.length === 0)
    ) {
      setTabs(currentTabs.filter((t) => t.value !== '3'));
    }
  }, [activeOrders, tabs, setTabs]);

  return (
    <MainPage>
      <TabContext value={value}>
        <TabList onChange={handleChange}>
          {tabs.map((tab, index) => (
            <Tab
              key={index}
              label={tab.label}
              icon={tab.icon}
              value={tab.value}
            />
          ))}
        </TabList>
        <Box
          sx={{
            height: 80,
            width: '100%',
            // marginTop: 16,
            borderRadius: 1,
            bgcolor: 'grey.50012',
          }}
        >
          {tabs.map((panel, index) => (
            <TabPanel key={index} value={String(index + 1)}>
              <MainColor as="h2">{panel.label}</MainColor>
              <Box mt={4}>{panel.component}</Box>
            </TabPanel>
          ))}
        </Box>
      </TabContext>
    </MainPage>
  );
}

const mapStateToProps = createStructuredSelector({
  activeOrders: selectActiveOrdersAsArray,
});

const mapDispatchToProps = (dispatch) => ({
  fetchProducts: () => dispatch(fetchProducts()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withRouter(ProductPage));

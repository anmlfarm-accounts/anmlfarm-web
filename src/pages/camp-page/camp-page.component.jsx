import React from 'react';
import { Route } from 'react-router-dom';
import CampEdit from '../../components/camps/camp-edit/camp-edit.component';
import CampInfo from '../../components/camps/camp-info/camp-info.component';
import NewCamp from '../../components/camps/new-camp/new-camp.component';
import MainPage from '../main-page/main-page.component';

const CampPage = ({ match }) => {
  return (
    <MainPage>
      <Route exact path={`${match.path}/new`} component={NewCamp} />
      <Route exact path={`${match.path}/info`} component={CampInfo} />
      <Route exact path={`${match.path}/info/:campId`} component={CampInfo} />
      <Route exact path={`${match.path}/edit/:campId`} component={CampEdit} />
      <Route exact path={`${match.path}/`} component={CampInfo} />
    </MainPage>
  );
};

export default CampPage;

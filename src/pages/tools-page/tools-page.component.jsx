import React from 'react';
import { Route } from 'react-router';
import ObjectDetect from '../../components/tools/object-detect/object-detect.component';

import MainPage from '../main-page/main-page.component';

const ToolsPage = ({ match }) => {
  return (
    <MainPage>
      <Route path={`${match.path}/object-detect`} component={ObjectDetect} />
    </MainPage>
  );
};

export default ToolsPage;

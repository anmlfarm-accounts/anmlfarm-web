import React from 'react';
import './entry-page.styles.css'
import Subtract from './images/Subtract.png'
import {Route, withRouter} from 'react-router-dom'


import {connect} from 'react-redux'
import { selectCurrentUser } from '../../redux/users/user.selectors';
import { createStructuredSelector } from 'reselect';
import { useEffect } from 'react';
import ProfileAgent from '../../components/profile/profile-agent/profile-agent.component';


const EntryPage = ({currentUser, match, history}) => {

    useEffect(() => {
        if(currentUser && currentUser?.hasDoneOnboarding) 
        {
            if(!currentUser.hasDonePrivacy){
                history.push(`${match.url}profile/privacy-policy`)
            } else if(!currentUser.hasDoneTerms){
                history.push(`${match.url}profile/terms-and-conditions`)
            } else history.push(`${match.url}home`);
        }
    }, [currentUser, history, match])


    const handleClick = () =>
    {
        if(!currentUser){
            history.push(`${match.url}profile/signin`)
        } else if(!currentUser.hasDoneOnboarding){
            history.push(`${match.url}profile/onboarding`)
        } else if(!this.props.currentUser.hasDonePrivacy){
            this.props.history.push(`${this.props.match.url}profile/privacy-policy`)
        } else if(!this.props.currentUser.hasDoneTerms){
            this.props.history.push(`${this.props.match.url}profile/terms-and-conditions`)
        }  else history.push(`${match.url}home`)
    }

    return(
        <div className='entry-page'>
            <div className='spacer'></div>
            <img src={Subtract} onClick={handleClick} alt='main logo' />
            <Route path='/agent/:agentCode' component={ProfileAgent} />

            <div className='spacer'></div>
        </div>
    )
    
}


const mapStateToProps = createStructuredSelector({
    currentUser: selectCurrentUser
})

export default connect(mapStateToProps)(withRouter(EntryPage));
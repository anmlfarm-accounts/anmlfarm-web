import React from 'react';
import './home-page.styles.css'

import MainPage from '../main-page/main-page.component';
// import EventFeed from '../../components/events/event-feed/event-feed.component';
import NotificationFeed from '../../components/notifications/notification-feed/notification-feed.component';
// import AnnouncementFeed from '../../components/announcements/announcement-feed/announcement-feed.component';
import ProfileHeader from '../../components/profile/profile-header/profile-header.component';
import { connect } from 'react-redux';
import { fetchPublicEventsStart } from '../../redux/events/event.actions';

const HomePage =({fetchPublicEvents}) => {
    fetchPublicEvents(null);
    return(
        <MainPage>
            <ProfileHeader />
            <NotificationFeed />
        </MainPage>
    )
}

const mapDispatchToProps = dispatch => ({
    fetchPublicEvents: (startTime) => dispatch(fetchPublicEventsStart(startTime))
})

export default connect(null,mapDispatchToProps)(HomePage);
import React from 'react';
import { Component } from 'react';
import { connect } from 'react-redux';
import { Route } from 'react-router-dom';
import { createStructuredSelector } from 'reselect';
import AllFarms from '../../components/farms/all-farms/all-farms.component';
import EditFarm from '../../components/farms/farm-edit/edit-farm.component';
import FarmInfo from '../../components/farms/farm-info/farm-info.component';
import NewFarm from '../../components/farms/new-farm/new-farm.component';
import { fetchUserFarmsStart } from '../../redux/farms/farm.actions';
import { selectCurrentFarm } from '../../redux/farms/farm.selectors';
import { selectCurrentUser } from '../../redux/users/user.selectors';
import MainPage from '../main-page/main-page.component';

class FarmPage extends Component {
  componentDidMount() {
    const { user, fetchUserFarms } = this.props;
    fetchUserFarms(user);
  }

  render() {
    const { match } = this.props;
    return (
      <MainPage>
        <Route exact path={`${match.path}/new`} component={NewFarm} />
        <Route exact path={`${match.path}/info/:farmId`} component={FarmInfo} />
        <Route exact path={`${match.path}/info/`} component={FarmInfo} />
        <Route path={`${match.path}/edit/:farmId`} component={EditFarm} />
        <Route exact path={`${match.path}/`} component={AllFarms} />
      </MainPage>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  farm: selectCurrentFarm,
  user: selectCurrentUser,
});

const mapDispatchToProps = (dispatch) => ({
  fetchUserFarms: (user) => dispatch(fetchUserFarmsStart(user)),
});

export default connect(mapStateToProps, mapDispatchToProps)(FarmPage);

import { Grid } from '@material-ui/core';
import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { selectCamp } from '../../../redux/camps/camp.actions';
import CardListItem from '../../shared/list-page/list-item';

const CampListItem = ({ setCamp, history, camp }) => {
  const gotoCamp = () => {
    const { id, campId } = camp;
    setCamp(campId ?? id);
    history.push(`/camps/info/${campId ?? id}`);
  };
  return (
    <>
      <Grid onClick={gotoCamp}>
        <CardListItem
          id={camp?.id??camp?.campId}
          image={camp?.campImage}
          name={camp?.displayName}
          campPurpose={camp?.campPurpose}
        />
      </Grid>
    </>
  );
};

const mapDispatchToProps = (dispatch) => ({
  setCamp: (campId) => dispatch(selectCamp(campId)),
});

export default withRouter(connect(null, mapDispatchToProps)(CampListItem));

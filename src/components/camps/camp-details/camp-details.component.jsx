import React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import campStats from './camp-stats.svg';
import CampDescription from '../camp-description/camp-description.component';
import InfoStats from '../../shared/info-page/info-stats/info-stats.component';

function CampDetails({ camp, stats }) {
  return (
    <>
      <CssBaseline />
      <CampDescription camp={camp} />
      {stats ? (
        <InfoStats
          displayName="Camp"
          description="Lorem, ipsum dolor sit amet consectetur adipisicing elit.
            Consectetur nam inventore nemo at maiores amet in veniam fuga,
            numquam ducimus consequuntur neque quo cupiditate dolore ad dolor
            quia quos provident?"
          image={campStats}
        />
      ) : null}
    </>
  );
}

export default CampDetails;

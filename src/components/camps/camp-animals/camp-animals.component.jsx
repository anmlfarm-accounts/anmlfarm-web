import React, { useState } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { createStructuredSelector } from 'reselect';
import {
  animalLoading,
  selectCurrentActiveCampAnimalsAsArray,
} from '../../../redux/animals/animal.selectors';
import { selectUserCanModifyCamp } from '../../../redux/camps/camp.selectors';
import { Box, Typography } from '@material-ui/core';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import AnimalListItem from '../../animals/animal-list-item/animal-list-item.component';
import CustomButton from '../../shared/custom-button/custom-button.component';
import SearchField from '../../shared/search-field/search-field.component';
import Loading from '../../shared/loading/loading.component';
import { ToggleHeading } from '../../../styles/shared/shared.styles';

const CampAnimals = ({ campAuth, animals, history, loading }) => {
  const [query, setQuery] = useState('');
  const [active, setActive] = useState(true);

  const getFilteredAnimals = () => {
    if (!query || !animals || query === '') return animals;
    return animals.filter(
      (a) =>
        a.tagNumber?.toLowerCase()?.includes(query?.toLowerCase()) ||
        a.displayName?.toLowerCase()?.includes(query?.toLowerCase()) ||
        a.animalGender?.toLowerCase() === query?.toLowerCase() ||
        a.animalBreed?.toLowerCase() === query?.toLowerCase() ||
        a.animalType?.toLowerCase() === query?.toLowerCase(),
    );
  };

  const filterByStatus = () => {
    return active
      ? getFilteredAnimals().filter((a) => !a?.animalEnd)
      : getFilteredAnimals().filter((a) => a?.animalEnd);
  };

  return (
    <div className="camp-animals">
      {loading && <Loading />}
      {animals.length > 0 && (
        <>
          <Box sx={{ textAlign: 'center', fontWeight: 600 }}>
            <ToggleHeading>
              <FormControlLabel
                control={
                  <Switch
                    checked={active}
                    onChange={() => setActive(!active)}
                    color="primary"
                  />
                }
              />
              <Typography variant="h5">
                {active && `Active`}
                {!active && `Inactive`}
                Animals
              </Typography>
            </ToggleHeading>
          </Box>
          <SearchField
            values={filterByStatus() ?? []}
            valueUpdated={setQuery}
            optionUpdated={setQuery}
          />
          <div className="animal-list">
            {filterByStatus().map((animal) => (
              <AnimalListItem
                animal={animal}
                key={animal?.id ?? animal?.animalId}
              />
            ))}
          </div>
        </>
      )}
      <div className="add-camp">
        {campAuth && (
          <CustomButton onClick={() => history.push('/animals/new')}>
            + ADD ANIMAL
          </CustomButton>
        )}
      </div>
    </div>
  );
};

const mapStateToProps = createStructuredSelector({
  animals: selectCurrentActiveCampAnimalsAsArray,
  campAuth: selectUserCanModifyCamp,
  loading: animalLoading,
});

export default connect(mapStateToProps)(withRouter(CampAnimals));

import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { createStructuredSelector } from 'reselect';
import { selectCurrentActiveCampsAsArray } from '../../../redux/camps/camp.selectors';
import {
  selectCurrentFarm,
  selectUserCanEditFarm,
} from '../../../redux/farms/farm.selectors';
import CustomButton from '../../shared/custom-button/custom-button.component';
import CampListItem from '../camp-list-item/camp-list-item.component';
import { restoreCampStart } from '../../../redux/camps/camp.actions';

const CampList = ({
  selectCurrentFarm,
  camps,
  history,
  selectUserCanEditFarm,
}) => {
  return (
    <div className="camp-list-container">
      {camps && (
        <div className="camp-list">
          {camps.map((camp) => (
            <CampListItem camp={camp} key={camp?.id ?? camp?.campId} />
          ))}
        </div>
      )}
      {selectUserCanEditFarm && (
        <div className="add-camp">
          <CustomButton onClick={() => history.push('/camps/new')}>
            ADD CAMP
          </CustomButton>
        </div>
      )}
    </div>
  );
};

const mapStateToProps = createStructuredSelector({
  selectUserCanEditFarm,
  selectCurrentFarm,
  camps: selectCurrentActiveCampsAsArray,
});

const mapDispatchToProps = (dispatch) => ({
  restoreCamp: (camp) => dispatch(restoreCampStart(camp)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withRouter(CampList));

import React from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import {
  selectCurrentFarm,
  selectCurrentSelectedFarmWorkersArray,
} from '../../../redux/farms/farm.selectors';
import { Grid } from '@material-ui/core';
import {
  WorkerTableRow,
  GriDisplayFlex,
  MainColor,
  Bold,
  TextCenter,
} from '../../../styles/shared/shared.styles';
import RestoreIcon from '@material-ui/icons/Restore';
// import { selectRecycleBinCampsAsArray } from '../../../redux/camps/camp.selectors';

const CampBinTable = ({ binCamps, restoreCamp }) => {
  return (
    <>
      <WorkerTableRow container>
        <Grid item xs={10}>
          <Bold>
            <MainColor>Camp</MainColor>
          </Bold>
        </Grid>
        <GriDisplayFlex item xs={2}>
          <Grid item xs={6}>
            <Bold>
              <TextCenter>
                <MainColor>Restore</MainColor>
              </TextCenter>
            </Bold>
          </Grid>
        </GriDisplayFlex>
      </WorkerTableRow>
      {binCamps.map((camp) => (
        <WorkerTableRow container key={camp.id}>
          <Grid item xs={10} md={9}>
            {camp.campPurpose}
          </Grid>
          <GriDisplayFlex item xs={2} md={3}>
            <TextCenter item xs={6}>
              {<RestoreIcon onClick={() => restoreCamp(camp)} />}
            </TextCenter>
          </GriDisplayFlex>
        </WorkerTableRow>
      ))}
    </>
  );
};

const mapStateToProps = createStructuredSelector({
  // binCamps: selectRecycleBinCampsAsArray,
});

export default connect(mapStateToProps)(CampBinTable);

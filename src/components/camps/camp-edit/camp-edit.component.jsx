import React, { useState } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { createStructuredSelector } from 'reselect';
import { Formik, Form } from 'formik';
import * as Yup from 'yup';
import {
  deleteCampStart,
  updateCampStart,
} from '../../../redux/camps/camp.actions';
import { selectCurrentCamp } from '../../../redux/camps/camp.selectors';
import { selectCurrentFarm } from '../../../redux/farms/farm.selectors';
import { selectCurrentUser } from '../../../redux/users/user.selectors';
import { MainContainer } from '../../../styles/info.pages/info.page.styles';
import TextField from '../../shared/formik-form/text-field/text-field';
import SubmitButton from '../../shared/formik-form/submit-button/ButtonSubmit';
import UploadOrSnapshot from '../../shared/upload-or-snapshot/upload-or-snapshot.component';
import editCampIcon from './Assets/edit-camp.svg';
import CampHeader from '../camp-header/camp-header.component';
import {
  FormImage,
  FormIntro,
} from '../../../styles/form-pages/form-pages.styles';
import { Box, Button } from '@material-ui/core';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import CustomButton from '../../shared/custom-button/custom-button.component';
import DeletePopup from '../../shared/delete-popup/delete-popup.component';

const FORM_VALIDATION = Yup.object().shape({
  displayName: Yup.string().required('Camp Name is Required'),
  campPurpose: Yup.string().required('Camp Purpose is Required'),
});

const EditCamp = ({ user, editCamp, history, camp, deleteCamp }) => {
  const [openUndo, setOpenUndo] = useState(false);

  const INITIAL_FORM_STATE = {
    displayName: camp?.displayName,
    campPurpose: camp?.campPurpose,
    creatingUserId: camp?.creatingUserId,
    campImage: camp?.campImage,
    id: camp?.id,
  };

  const redirectOnClick = () => {
    history.goBack();
  };

  const removeCamp = () => {
    deleteCamp(camp);
    history.push(`/farms/info/${camp.farmId}`);
  };

  const onSubmit = (values) => {
    if (!user) return;
    INITIAL_FORM_STATE?.campImage
      ? editCamp({
          ...values,
          campImage: INITIAL_FORM_STATE?.campImage,
        })
      : editCamp(values);
    history.push(`/camps/info/${camp?.id ?? camp?.campId}`);
  };

  const handleImageUploaded = (image) => {
    if (!image) return;
    INITIAL_FORM_STATE.campImage = image;
  };

  return (
    <>
      <CampHeader />
      <MainContainer maxWidth="sm">
        <Box m={3}>
          <Button
            variant="text"
            startIcon={<ArrowBackIcon />}
            onClick={redirectOnClick}
          >
            Go Back
          </Button>
        </Box>
        <FormIntro>
          <FormImage src={editCampIcon} />
          <h2>EDIT YOUR CAMP!</h2>
          <UploadOrSnapshot captureCompleteCallback={handleImageUploaded} />
        </FormIntro>
        <Formik
          initialValues={{
            ...INITIAL_FORM_STATE,
          }}
          validationSchema={FORM_VALIDATION}
          onSubmit={onSubmit}
        >
          <Form>
            <Box m={1}>
              <Box py={1}>
                <TextField label="Display Name" name="displayName" />
              </Box>
              <Box py={1}>
                <TextField label="Camp Purpose" name="campPurpose" />
              </Box>
              <Box py={1}>
                <SubmitButton type="submit">SAVE CAMP</SubmitButton>
              </Box>
            </Box>
          </Form>
        </Formik>
        <CustomButton type="delete" onClick={() => setOpenUndo(true)}>
          DELETE CAMP
        </CustomButton>
        <DeletePopup
          record="Camp"
          deleteRecord={removeCamp}
          popupOpen={openUndo}
          cancel={() => setOpenUndo(false)}
          onClick={() => setOpenUndo(false)}
        />
      </MainContainer>
    </>
  );
};

const mapStateToProps = createStructuredSelector({
  user: selectCurrentUser,
  camp: selectCurrentCamp,
  farm: selectCurrentFarm,
});

const mapDispacthToProps = (dispatch) => ({
  editCamp: (campDetails) => dispatch(updateCampStart(campDetails)),
  deleteCamp: (campDetails) => dispatch(deleteCampStart(campDetails)),
});

export default connect(
  mapStateToProps,
  mapDispacthToProps,
)(withRouter(EditCamp));

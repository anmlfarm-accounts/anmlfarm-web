import React, { useState } from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { Formik, Form } from 'formik';
import * as Yup from 'yup';
import {
  addCampFeedStart,
  deleteLatestCampFeedStart,
} from '../../../redux/camps/camp.actions';
import {
  selectCurrentCamp,
  selectCurrentSelectedCampFeedsAsArray,
  selectUserCanModifyCamp,
} from '../../../redux/camps/camp.selectors';
import TextField from '../../shared/formik-form/text-field/text-field';
import SubmitButton from '../../shared/formik-form/submit-button/ButtonSubmit';
import {
  GridPadding20,
  ColumnFlexEnd,
  TextCenter,
} from '../../../styles/shared/shared.styles';
import { Typography, Button } from '@material-ui/core';
import ChartColumnSingle from '../../minimal-ui/charts/ChartColumnSingle';
import { fDate, fDateYear } from '../../minimal-ui/utils/formatTime';
import { Box, IconButton } from '@material-ui/core';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import CheckboxField from '../../shared/formik-form/checkbox-field/checkbox-field';
import DateTimeField from '../../shared/formik-form/datetime-field/datetime-field.component';

const FORM_VALIDATION = Yup.object().shape({
  feed: Yup.string().required('Feed is Required'),
  quantity: Yup.number()
    .typeError('Must be a number')
    .required('Quantity is Required'),
});

const INITIAL_FORM_STATE = {
  feed: '',
  quantity: '',
  date: fDateYear(new Date()),
};

const CampFeed = ({
  addFeed,
  campDetails,
  campFeeds,
  campAuth,
  deleteFeed,
}) => {
  const [isChecked, setIsChecked] = useState(true);
  const [calender, setCalender] = useState(false);
  const [menu, setMenu] = useState(false);

  const deleteLatestCampFeed = () => {
    deleteFeed(campDetails);
  };

  const addCampFeed = (values, props) => {
    const details = {
      feed: values.feed,
      quantity: values.quantity,
      feedDate: fDate(new Date()),
      dateTime: new Date(),
    };
    isChecked
      ? addFeed(campDetails, { ...details })
      : addFeed(campDetails, { ...details, feedDate: fDate(values.date) });
    props.resetForm();
  };

  const feedDates = campFeeds.map((feed) => feed?.feedDate);
  const feedValues = campFeeds.map((feed) => feed?.quantity);

  return (
    <div>
      <div>
        {campFeeds && campFeeds.length > 0 ? (
          <>
            <ColumnFlexEnd align="right">
              <IconButton onClick={() => setMenu(!menu)}>
                <MoreVertIcon />
              </IconButton>
              {menu && campAuth && (
                <Button onClick={deleteLatestCampFeed}>Undo last Feed</Button>
              )}
            </ColumnFlexEnd>
            <ChartColumnSingle
              title="Animal Feed"
              values={feedValues}
              dates={feedDates}
            />
          </>
        ) : (
          <GridPadding20>
            <TextCenter>
              <Typography variant="h6" paragraph>
                No Feed to show
              </Typography>
            </TextCenter>
          </GridPadding20>
        )}
      </div>
      {campAuth && (
        <Box sx={{ maxWidth: 480, margin: 'auto' }}>
          <Formik
            initialValues={{
              ...INITIAL_FORM_STATE,
            }}
            validationSchema={FORM_VALIDATION}
            onSubmit={addCampFeed}
          >
            <Form>
              <Box m={1}>
                <Box py={1}>
                  <TextField label="Feed" name="feed" />
                </Box>
                <Box py={1}>
                  <TextField label="Quantity (kg)" name="quantity" />
                </Box>
                <Box py={1}>
                  <CheckboxField
                    label="Today"
                    onChange={(e) => setIsChecked(e.currentTarget.checked)}
                    checked={isChecked}
                    onClick={() => setCalender(!calender)}
                  />
                </Box>
                {calender && (
                  <Box py={1}>
                    <DateTimeField label="Date" name="date" />
                  </Box>
                )}
                <Box py={1}>
                  <SubmitButton type="submit">Create Camp</SubmitButton>
                </Box>
              </Box>
            </Form>
          </Formik>
        </Box>
      )}
    </div>
  );
};

const mapStateToProps = createStructuredSelector({
  campDetails: selectCurrentCamp,
  campFeeds: selectCurrentSelectedCampFeedsAsArray,
  campAuth: selectUserCanModifyCamp,
});

const mapDispatchToProps = (dispatch) => ({
  addFeed: (campInfo, feedInfo) =>
    dispatch(addCampFeedStart(campInfo, feedInfo)),
  deleteFeed: (animalInfo) => dispatch(deleteLatestCampFeedStart(animalInfo)),
});

export default connect(mapStateToProps, mapDispatchToProps)(CampFeed);

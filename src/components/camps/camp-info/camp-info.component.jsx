import React from 'react';
import { useEffect } from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { selectCurrentCampAnimalsAsArray } from '../../../redux/animals/animal.selectors';
import { selectCamp } from '../../../redux/camps/camp.actions';
import {
  selectCurrentCamp,
  selectUserCanModifyCamp,
} from '../../../redux/camps/camp.selectors';
import CampAnimals from '../camp-animals/camp-animals.component';
import CssBaseline from '@material-ui/core/CssBaseline';
import CampDetails from '../camp-details/camp-details.component';
import CustomButton from '../../shared/custom-button/custom-button.component';
import CampHeader from '../camp-header/camp-header.component';
import CampFeed from '../camp-feed/camp-feed.component';
import AccordionItem from '../../minimal-ui/accordion/Accordion';
import { calculateCombinedAnimalDryMassIndex } from '../../../utils/animals/dry-mass-index';
import PetsIcon from '@material-ui/icons/Pets';
import GrainIcon from '@material-ui/icons/Grain';
import { Container } from '@material-ui/core';

const CampInfo = ({
  camp,
  animals,
  campAuth,
  // events,
  setCamp,
  match,
  history,
}) => {
  useEffect(() => {
    const { campId } = match.params;
    if (campId) {
      setCamp(campId);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [match.params]);

  if (!camp) return null;

  return (
    <div className="camp-info">
      <CssBaseline />
      <CampHeader />
      <Container maxWidth="md">
        <CampDetails camp={camp} />
        <AccordionItem title="Feed" icon={<GrainIcon width={20} height={20} />}>
          <>
            <span>
              Animals currently consuming{' '}
              <strong>
                {calculateCombinedAnimalDryMassIndex(animals)} kg / day{' '}
              </strong>{' '}
              according to their Dry Mass Index (DMI)
            </span>
            <CampFeed />
          </>
        </AccordionItem>
        <AccordionItem
          expanded={true}
          title="Animals"
          icon={<PetsIcon width={20} height={20} />}
        >
          <CampAnimals animals={animals} />
        </AccordionItem>
        {campAuth && (
          <CustomButton
            onClick={() =>
              history.push(`/camps/edit/${camp?.id ?? camp?.campId}`)
            }
          >
            EDIT CAMP
          </CustomButton>
        )}
        <CustomButton onClick={() => history.push(`/tools/object-detect/`)}>
          COUNT
        </CustomButton>
      </Container>
    </div>
  );
};

const mapStateToProps = createStructuredSelector({
  camp: selectCurrentCamp,
  animals: selectCurrentCampAnimalsAsArray,
  campAuth: selectUserCanModifyCamp,
});
const mapDispatchToProps = (dispatch) => ({
  setCamp: (campId) => dispatch(selectCamp(campId)),
});
export default connect(mapStateToProps, mapDispatchToProps)(CampInfo);

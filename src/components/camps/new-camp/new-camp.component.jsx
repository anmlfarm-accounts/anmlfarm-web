import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { createStructuredSelector } from 'reselect';
import { Formik, Form } from 'formik';
import * as Yup from 'yup';
import { createCampStart } from '../../../redux/camps/camp.actions';
import { selectCurrentFarm } from '../../../redux/farms/farm.selectors';
import { selectCurrentUser } from '../../../redux/users/user.selectors';
import { MainContainer } from '../../../styles/info.pages/info.page.styles';
import TextField from '../../shared/formik-form/text-field/text-field';
import SubmitButton from '../../shared/formik-form/submit-button/ButtonSubmit';
import UploadOrSnapshot from '../../shared/upload-or-snapshot/upload-or-snapshot.component';
import newCampIcon from './Assets/new-camp.svg';
import {
  FormImage,
  FormIntro,
} from '../../../styles/form-pages/form-pages.styles';
import { Box, Button } from '@material-ui/core';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';

const FORM_VALIDATION = Yup.object().shape({
  displayName: Yup.string().required('Camp Name is Required'),
  campPurpose: Yup.string().required('Camp Purpose is Required'),
});

const INITIAL_FORM_STATE = {
  displayName: '',
  campPurpose: '',
};

const NewCamp = ({ user, createNewCampAsync, history, farm }) => {
  const redirectOnClick = () => {
    history.goBack();
  };

  const onSubmit = (values) => {
    if (!user) return;
    createNewCampAsync({
      ...values,
      creatingUserId: user?.uid,
      farmId: farm?.farmId ?? farm?.id,
      campImage: INITIAL_FORM_STATE?.campImage,
    });
    history.push(`/farms/info/${farm?.id ?? farm?.farmId}`);
  };

  const handleImageUploaded = (image) => {
    if (!image) return;
    INITIAL_FORM_STATE.campImage = image;
  };

  return (
    <MainContainer maxWidth="sm">
      {!user && <div>PLEASE LOG IN TO CREATE A CAMP</div>}
      <Box m={3}>
        <Button
          variant="text"
          startIcon={<ArrowBackIcon />}
          onClick={redirectOnClick}
        >
          Go Back
        </Button>
      </Box>
      <FormIntro>
        <FormImage src={newCampIcon} />
        <h2>WELCOME TO YOUR NEW CAMP!</h2>
        <span>Please provide us with some information about your camp:</span>

        <UploadOrSnapshot captureCompleteCallback={handleImageUploaded} />
      </FormIntro>
      <Formik
        initialValues={{
          ...INITIAL_FORM_STATE,
        }}
        validationSchema={FORM_VALIDATION}
        onSubmit={onSubmit}
      >
        <Form>
          <Box m={1}>
            <Box py={1}>
              <TextField label="Display Name" name="displayName" />
            </Box>
            <Box py={1}>
              <TextField label="Camp Purpose" name="campPurpose" />
            </Box>
            <Box py={1}>
              <SubmitButton type="submit">Create Camp</SubmitButton>
            </Box>
          </Box>
        </Form>
      </Formik>
    </MainContainer>
  );
};

const mapStateToProps = createStructuredSelector({
  user: selectCurrentUser,
  farm: selectCurrentFarm,
});

const mapDispacthToProps = (dispatch) => ({
  createNewCampAsync: (campDetails) => dispatch(createCampStart(campDetails)),
});

export default connect(
  mapStateToProps,
  mapDispacthToProps,
)(withRouter(NewCamp));

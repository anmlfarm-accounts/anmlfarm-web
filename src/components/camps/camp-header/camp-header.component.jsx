import React from 'react'
import { Avatar } from '@material-ui/core';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import './camp-header.styles.scss'
import { withRouter } from 'react-router';
import { selectCurrentCamp } from '../../../redux/camps/camp.selectors';

const CampHeader = ({camp, history}) => {
    const redirectOnClick = () => {
        const {farmId} = camp;
        if(farmId){
            history.push(`/farms/info/${farmId}`)
        } else history.push(`/farms/`)
    }
    return (
        <div className='header' onClick={redirectOnClick}>
            <div className='camp-pic'>
                {(camp && camp.campImage) ? 
                    <Avatar className='avatar' src={camp?.campImage} /> : 
                <Avatar className='avatar'>{camp?.displayName ? camp?.displayName[0] : '?'}</Avatar>}
            </div>
            <div className='welcome-text'>
                {camp?.displayName ?? 'No Camp Found. Tap To Create a Camp.'}
            </div>
        </div>
    )
}
const mapStateToProps = createStructuredSelector({
    camp: selectCurrentCamp
})

export default connect(mapStateToProps)(withRouter(CampHeader));

import React from 'react';
import {
  DisplayFlex,
  Margin10TopBottom,
  TextCenter,
} from '../../../styles/shared/shared.styles';
import {
  Info,
  InfoBold,
  // SmallGray,
} from '../../../styles/info.pages/info.page.styles';
import InfoDescription from '../../shared/info-page/info-description/info-description.component';

const CampDescription = ({ camp }) => {
  return (
    <InfoDescription images={[camp.campImage]}>
      <h1>
        <TextCenter>{camp.displayName}</TextCenter>
      </h1>
      <DisplayFlex>
        <Info>Camp Name:</Info>
        <InfoBold>{camp.displayName}</InfoBold>
      </DisplayFlex>
      <Margin10TopBottom as="p">
        {camp.campPurpose}
      </Margin10TopBottom>
      {/* <SmallGray>Added 5 days ago</SmallGray> */}
    </InfoDescription>
  );
};

export default CampDescription;

import React, { useEffect, useRef, useState } from 'react'
import Webcam from 'react-webcam';
import { convertObjectToArray } from '../../../utils/array/mapper';
import Loading from '../../shared/loading/loading.component';

const videoConstraints = {
    width: 800,
    height: 800,
    facingMode: "environment"
}

export default function ObjectDetect() {
    
    const videoRef = useRef();
    const divRef = useRef();
    const [detections, setDetections] = useState();
    const [modelLoaded, setmodelLoaded] = useState(false);

    const reduceDetections = (det) => {
        return convertObjectToArray(
            det.reduce((a,c) => 
                ({
                    ...a, 
                    [c.label] : {
                        ...a[c.label],
                        ...c, 
                        count: a[c.label] ? a[c.label].count + 1 : 1
                    }
                }),{})
        )
    }

    useEffect(() => {
        let detectionInterval;
    
    // 1. Once the model has loaded, update the dimensions run the model's detection interval
    const modelLoaded = () => {
        setmodelLoaded(true);
        detectionInterval = setInterval(() => {
            detect();
        }, 1000);
    };
    
        /*global ml5*/ 
        const objectDetector = ml5.objectDetector('cocossd', modelLoaded);
    
        const detect = () => {
            if (videoRef.current.video.readyState !== 4) {
                console.warn('Video not ready yet');
                return;
            }
    
            objectDetector.detect(videoRef.current.video, (err, results) => {
                setDetections(results);
            });
        };
    
        return () => {
            if (detectionInterval) {
                clearInterval(detectionInterval);
            }
        }
    
    }, []);


    return (
        <div style={{width:'100%'}} ref={divRef}>
            {modelLoaded ? <>
            {detections ? reduceDetections(detections).map(d=>(
                <h2 key={d.label}>{d.label} - {d.count}</h2>)):null}
            <Webcam 
                audio={false}
                width='100%'
                height='100%'
                videoConstraints={videoConstraints}
                ref={videoRef}/>
                </> : <Loading /> }
        </div>
    )
}

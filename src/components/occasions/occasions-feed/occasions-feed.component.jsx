import React from 'react';
import OccasionItem from '../occasion-item/occasion-item.component';
import { Container } from '@material-ui/core';
import { createStructuredSelector } from 'reselect';
import { selectPublicOccasions } from '../../../redux/occasions/occasion.selectors';
import { connect } from 'react-redux';

const OccasionFeed = ({
  ownedFarmOccasions,
  workingFarmOccasions,
  followingFarmOccasions,
  selectPublicOccasions,
  desktopDevice,
}) => {
  return (
    <Container maxWidth="md">
      <div className="feed">
        {selectPublicOccasions ? (
          <div className="occasion-collection">
            {selectPublicOccasions.map((evt) => (
              <OccasionItem key={evt.id} occasion={evt} />
            ))}
          </div>
        ) : null}
      </div>
    </Container>
  );
};

const mapStateToProps = createStructuredSelector({
  selectPublicOccasions,
});

export default connect(mapStateToProps)(OccasionFeed);

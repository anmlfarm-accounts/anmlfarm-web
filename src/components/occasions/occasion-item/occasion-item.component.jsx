import React from 'react';
import { FeedText } from '../../../styles/occasion.feed/occasion.feed.styles';
import { Media } from '../../../styles/info.pages/info.page.styles';
import { CardFullWidth } from '../../../styles/shared/shared.styles';
// import OccasionFeedItemHeader from './occasion-feed-item-header';
import OccasionFeedOptions from './occasion-feed-item-options'

const OccasionItem = ({occasion, active}) => {
  const {coverImage, description} = occasion;
  return (
    <CardFullWidth>
      {/* <OccasionFeedItemHeader /> */}
      <Media image={coverImage} title="Occasion Image" />
      <FeedText>
        {description}
      </FeedText>
      {active?<FeedText>
        <OccasionFeedOptions />
      </FeedText>:null}
    </CardFullWidth>
  );
};

export default OccasionItem;

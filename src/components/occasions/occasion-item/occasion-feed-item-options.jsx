import { Grid } from '@material-ui/core';
import React from 'react';
import OccasionAvailableIcon from '@material-ui/icons/OccasionAvailable';
import ChatBubbleOutlineIcon from '@material-ui/icons/ChatBubbleOutline';
import MoreHorizIcon from '@material-ui/icons/MoreHoriz';
import ShareIcon from '@material-ui/icons/Share';
import { GriDisplayFlexAround } from '../../../styles/occasion.feed/occasion.feed.styles';

const OccasionFeedOptions = () => {
  return (
    <GriDisplayFlexAround container>
      <Grid item>
        <OccasionAvailableIcon />
      </Grid>
      <Grid item>
        <ChatBubbleOutlineIcon />
      </Grid>
      <Grid item>
        <ShareIcon />
      </Grid>
      <Grid item>
        <MoreHorizIcon />
      </Grid>
    </GriDisplayFlexAround>
  );
};

export default OccasionFeedOptions;

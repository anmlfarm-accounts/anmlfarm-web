import { Avatar } from '@material-ui/core';
import React from 'react';
import { BoldMainColor } from '../../../styles/occasion.feed/occasion.feed.styles';
import { SmallGray } from '../../../styles/info.pages/info.page.styles';
import {
  Bold,
  GridColumn,
  GriDisplayFlex,
} from '../../../styles/shared/shared.styles';

const OccasionFeedItemHeader = ({displayName, occasionTime, occasionCreator}) => {
  return (
    <GriDisplayFlex>
      <GridColumn item xs={2}>
        <Avatar image={occasionCreator?.photoUrl}/>
      </GridColumn>
      <GridColumn item xs={6}>
        <Bold as="h3">{displayName}</Bold>
        <SmallGray>Added By {occasionCreator?.displayName}</SmallGray>
      </GridColumn>
      <GridColumn item xs={4}>
        <BoldMainColor>{occasionTime}</BoldMainColor>
        <SmallGray>Category</SmallGray>
      </GridColumn>
    </GriDisplayFlex>
  );
};

export default OccasionFeedItemHeader;

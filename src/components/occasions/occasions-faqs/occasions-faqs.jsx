import React from 'react';
import {
  BoldMainColor,
  CardMarginX10,
  FaqText,
} from '../../../styles/occasion.feed/occasion.feed.styles';
import { GridColumn } from '../../../styles/shared/shared.styles';

const OccasionFaqs = () => {
  return (
    <CardMarginX10 variant="outlined">
      <GridColumn>
        <BoldMainColor>FAQ's</BoldMainColor>
        <FaqText as="p">How to create a new farm</FaqText>
        <FaqText as="p">How to become a supplier</FaqText>
        <FaqText as="p">How to become an agent</FaqText>
      </GridColumn>
    </CardMarginX10>
  );
};

export default OccasionFaqs;

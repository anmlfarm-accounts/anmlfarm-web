import { Button, Checkbox, FormControlLabel, Input } from '@material-ui/core';
import React from 'react'
import { connect } from 'react-redux';
import { createReminderStart } from '../../../redux/reminders/reminder.actions';
import Popup from '../../shared/popup/popup.component';
import './set-reminder.styles.scss'

function SetReminder({reminderDetails, saveReminder}) {

  const [open, setOpen] = React.useState(false);
  const [reminder, setReminder] = React.useState({
    isOnce: true,
    isRecurring: false,
    isDaily: false,
    isWeekly: false,
    isMonthly: false,
    isCustom: false,
    ...reminderDetails});

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const setReminderValue = (fieldName, value) => {
    let updatedValue = {...reminder};
    updatedValue[fieldName] = value;
    switch(fieldName){
      case 'isOnce':
        updatedValue['isRecurring'] = !value;
        if(value){
          updatedValue['isDaily'] = false;
          updatedValue['isWeekly'] = false;
          updatedValue['isMonthly'] = false;
          updatedValue['isCustom'] = false;
        }
        break;

      case 'isRecurring':
        updatedValue['isOnce'] = !value;
        if(value){
          updatedValue['isDaily'] = true;
          updatedValue['isWeekly'] = false;
          updatedValue['isMonthly'] = false;
          updatedValue['isCustom'] = false;
        }
        break;

      case 'isDaily':
        if(value){
          updatedValue['isWeekly'] = false;
          updatedValue['isMonthly'] = false;
          updatedValue['isCustom'] = false;
        }
        break;
        
      case 'isWeekly':
        if(value){
          updatedValue['isDaily'] = false;
          updatedValue['isMonthly'] = false;
          updatedValue['isCustom'] = false;
        }
        break;
        
      case 'isMonthly':
        if(value){
          updatedValue['isDaily'] = false;
          updatedValue['isWeekly'] = false;
          updatedValue['isCustom'] = false;
        }
        break;
        
      case 'isCustom':
        if(value){
          updatedValue['isDaily'] = false;
          updatedValue['isWeekly'] = false;
          updatedValue['isMonthly'] = false;
        }
        break;

      default:
        updatedValue[fieldName] = value;
    }
    setReminder(updatedValue)
  }

  const handleSave = () => {
    if(saveReminder) saveReminder(reminder);
    handleClose();
  }


  return (
    <div>
      <Button onClick={handleClickOpen}>Create Reminder</Button>
      <Popup popupOpen={open} title={'Create Reminder'} cancel={()=>handleClose()}>
        <div className="setreminder">
          <FormControlLabel
            className='form-input'
            label="Title"
            labelPlacement="top"
            control={<Input title={'Title'} type='text' value={reminder.title ?? ''} onChange={(e)=>{ setReminderValue('title', e.target.value);} } name="title" />}
            />
          <FormControlLabel
            className='form-input'
            labelPlacement="top"
            control={<Input title={'Title'} type='date' defaultValue={reminder.startDate ?? (new Date()).toISOString().substr(0,10)} onChange={(e)=>{ setReminderValue('startDate', e.target.value);} } name="startDate" />}
            label="Start Date"
          />
          <FormControlLabel
            className='form-input'
            labelPlacement="top"
            control={<Input title={'Title'} type='time' value={reminder.startTime ?? (new Date()).toTimeString().substr(0,5)} onChange={(e)=>{ setReminderValue('startTime', e.target.value);} } name="startTime" />}
            label="Start Time"
          />
          
          <FormControlLabel
            control={<Checkbox checked={reminder.isOnce} onChange={(e)=>{ setReminderValue('isOnce', e.target.checked);} } name="isOnce" />}
            label="Once"
          />
          <FormControlLabel
            control={<Checkbox checked={reminder.isRecurring} onChange={(e)=>{setReminderValue('isRecurring', e.target.checked);} } name="isRecurring" />}
            label="Recurring"
          />
          {reminder.isRecurring && 
            <FormControlLabel
              control={<Checkbox checked={reminder.isDaily} onChange={(e)=>setReminderValue('isDaily', e.target.checked)} name="isDaily" />}
              label="Daily"
            /> }

          {reminder.isRecurring && 
            <FormControlLabel
              control={<Checkbox checked={reminder.isWeekly} onChange={(e)=>setReminderValue('isWeekly', e.target.checked)} name="isWeekly" />}
              label="Weekly"
            /> }

          {reminder.isRecurring && 
            <FormControlLabel
              control={<Checkbox checked={reminder.isMonthly} onChange={(e)=>setReminderValue('isMonthly', e.target.checked)} name="isMonthly" />}
              label="Monthly"
            /> }

          {reminder.isRecurring && 
            <FormControlLabel
              control={<Checkbox checked={reminder.isCustom} onChange={(e)=>setReminderValue('isCustom', e.target.checked)} name="isCustom" />}
              label="Custom"
            /> }
          {reminder.isCustom && <span>Every <Input title={'days'} type='number' defaultValue={reminder.customDays ??  1} onChange={(e)=>setReminderValue('customDays', e.nativeEvent.data)} /> days</span> }
        </div>
        <div className="form-input">
          <Button onClick={handleSave}>Save</Button> 
          <Button onClick={handleClose}>Cancel</Button> 
        </div>
      </Popup>
    </div>
  );
}

const mapDispatchToState = dispatch => ({
  saveReminder: (reminderDetails) => dispatch(createReminderStart(reminderDetails))
})

export default connect(null, mapDispatchToState)(SetReminder);
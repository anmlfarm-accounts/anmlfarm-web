import React, { useEffect } from 'react';
import { createStructuredSelector } from 'reselect';
import FarmHeader from '../farm-header/farm-header.component';
import {
  selectCurrentFarm,
  selectUserCanEditFarm,
} from '../../../redux/farms/farm.selectors';
import { withRouter } from 'react-router-dom';
import { selectFarm } from '../../../redux/farms/farm.actions';
import {
  selectCurrentFarmCamps,
  selectCurrentFarmCampsCount,
} from '../../../redux/camps/camp.selectors';
import { connect } from 'react-redux';
import FarmDetails from '../farm-details/farm-details.component';
import CampList from '../../camps/camp-list/camp-list.component';
import CustomButton from '../../shared/custom-button/custom-button.component';
import FileUpload from '../../shared/file-upload/file-upload.component';
import FarmAddWorkers from '../farm-add-workers/farm-add-workers.component';
import AccordionItem from '../../minimal-ui/accordion/Accordion';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import WorkIcon from '@material-ui/icons/Work';
import CompareIcon from '@material-ui/icons/Compare';
import { Container } from '@material-ui/core';

function FarmInfo({
  selectCurrentFarm,
  selectCurrentFarmCamps,
  selectFarm,
  selectUserCanEditFarm,
  match,
  history,
}) {
  useEffect(() => {
    const { farmId } = match.params;
    if (
      farmId &&
      farmId !== (selectCurrentFarm?.id ?? selectCurrentFarm?.farmId)
    ) {
      selectFarm(farmId);
    }
  }, [match, selectCurrentFarm, selectFarm]);

  if (!selectCurrentFarm || selectCurrentFarm?.length === 0) return null;
  return (
    <>
      <FarmHeader />
      <Container maxWidth="md">
        <FarmDetails farm={selectCurrentFarm} />
        <AccordionItem
          title="File Uploads"
          icon={<CloudUploadIcon width={20} height={20} />}
        >
          <FileUpload forFarm={selectCurrentFarm} />
        </AccordionItem>
        <AccordionItem
          title="Farm Workers"
          icon={<WorkIcon width={20} height={20} />}
        >
          <FarmAddWorkers />
        </AccordionItem>
        <AccordionItem
          expanded={true}
          title="Farm Camps"
          icon={<CompareIcon width={20} height={20} />}
        >
          <CampList camps={selectCurrentFarmCamps} />
        </AccordionItem>
        {selectUserCanEditFarm && (
          <CustomButton
            onClick={() => history.push(`/farms/edit/${selectCurrentFarm?.id}`)}
          >
            EDIT FARM
          </CustomButton>
        )}
      </Container>
    </>
  );
}

const mapStateToProps = createStructuredSelector({
  selectUserCanEditFarm,
  selectCurrentFarm,
  selectCurrentFarmCamps,
  selectCurrentFarmCampsCount,
});

const mapDispatchToProps = (dispatch) => ({
  selectFarm: (farmId) => dispatch(selectFarm(farmId)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withRouter(FarmInfo));

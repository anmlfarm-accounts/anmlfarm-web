// import { Button } from '@material-ui/core';
import React from 'react'

function FarmSubscriptionItem({farm, subscription}) {
    return (
        <div>
            <h6>FARM NAME</h6>
            <div className="farm-name">{farm?.displayName}</div>
            <h6>Expires on</h6>
            <div className="subscription">{(new Date(subscription?.trialEndDate?.seconds ? subscription?.trialEndDate?.seconds*1000 : subscription?.trialEndDate)).toString().substr(0,15)}</div>
            {/* <Button>Purchase Subscription</Button> */}
        </div>
    )
}

export default FarmSubscriptionItem;
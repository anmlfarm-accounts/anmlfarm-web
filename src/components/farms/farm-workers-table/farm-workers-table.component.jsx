import React from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import {
  selectCurrentFarm,
  selectCurrentSelectedFarmWorkersArray,
} from '../../../redux/farms/farm.selectors';
import { Grid } from '@material-ui/core';
import {
  WorkerTableRow,
  GriDisplayFlex,
  MainColor,
  Bold,
  TextCenter,
} from '../../../styles/shared/shared.styles';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';

const FarmWorkersTable = ({ farmWorkers, editWorker, deleteWorker }) => {
  return (
    <>
      <WorkerTableRow container>
        <Grid item xs={10}>
          <Bold>
            <MainColor>Worker</MainColor>
          </Bold>
        </Grid>
        <GriDisplayFlex item xs={2}>
          <Grid item xs={6}>
            <Bold>
              <TextCenter>
                <MainColor>Edit</MainColor>
              </TextCenter>
            </Bold>
          </Grid>
          <Grid item xs={6}>
            <Bold>
              <TextCenter>
                <MainColor> Delete</MainColor>
              </TextCenter>
            </Bold>
          </Grid>
        </GriDisplayFlex>
      </WorkerTableRow>
      {farmWorkers.map((email) => (
        <WorkerTableRow container key={email}>
          <Grid item xs={10} md={9}>
            {email}
          </Grid>
          <GriDisplayFlex item xs={2} md={3}>
            <TextCenter item xs={6}>
              {<EditIcon onClick={() => editWorker(email)} />}
            </TextCenter>
            <TextCenter item xs={6}>
              <DeleteIcon onClick={() => deleteWorker(email)} />
            </TextCenter>
          </GriDisplayFlex>
        </WorkerTableRow>
      ))}
    </>
  );
};

const mapStateToProps = createStructuredSelector({
  farmDetails: selectCurrentFarm,
  farmWorkers: selectCurrentSelectedFarmWorkersArray,
});

export default connect(mapStateToProps)(FarmWorkersTable);

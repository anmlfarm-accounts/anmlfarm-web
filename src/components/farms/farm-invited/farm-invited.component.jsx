import React from 'react';
import Paper from '@material-ui/core/Paper';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { selectCurrentUserInvitedFarmsAsArray } from '../../../redux/farms/farm.selectors';
import { Button, Box, Container, Grid } from '@material-ui/core';
import { selectCurrentUser } from '../../../redux/users/user.selectors';
import { acceptInvitationStart } from '../../../redux/farms/farm.actions';
import { AcceptItem, ColorGray } from '../../../styles/shared/shared.styles';

const FarmInvited = ({ farmInvited, user, accept }) => {
  const acceptInvitation = (farmDetails) => {
    accept(farmDetails, user);
  };
  return (
    <Box sx={{ maxWidth: 480, margin: 'auto' }}>
      <Container component={Paper} elevation={0}>
        {farmInvited.map((farm) => (
          <AcceptItem container spacing={1}>
            <Grid item>
              <ColorGray>
                {farm?.displayName} has invited you to work on their farm
              </ColorGray>
            </Grid>
            <Grid item>
              <Button
                variant="outlined"
                color="default"
                onClick={() => acceptInvitation(farm)}
              >
                Accept
              </Button>
            </Grid>
          </AcceptItem>
        ))}
      </Container>
    </Box>
  );
};

const mapStateToProps = createStructuredSelector({
  farmInvited: selectCurrentUserInvitedFarmsAsArray,
  user: selectCurrentUser,
});

const mapDispatchToProps = (dispatch) => ({
  accept: (farmDetails, user) =>
    dispatch(acceptInvitationStart(farmDetails, user)),
});

export default connect(mapStateToProps, mapDispatchToProps)(FarmInvited);

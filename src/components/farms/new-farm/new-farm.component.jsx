import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { Formik, Form } from 'formik';
import * as Yup from 'yup';
import { createStructuredSelector } from 'reselect';
import { createFarmStart } from '../../../redux/farms/farm.actions';
import { selectCurrentUser } from '../../../redux/users/user.selectors';
import { MainContainer } from '../../../styles/info.pages/info.page.styles';
import TextField from '../../shared/formik-form/text-field/text-field';
import SubmitButton from '../../shared/formik-form/submit-button/ButtonSubmit';
import UploadOrSnapshot from '../../shared/upload-or-snapshot/upload-or-snapshot.component';
import newFarmIcon from './Assets/new-farm.svg';
import {
  FormImage,
  FormIntro,
} from '../../../styles/form-pages/form-pages.styles';
import { Box, Button } from '@material-ui/core';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';

const FORM_VALIDATION = Yup.object().shape({
  displayName: Yup.string().required('Farm Display Name is Required'),
  farmCompanyName: Yup.string().required('Company Name is Required'),
});

const INITIAL_FORM_STATE = {
  displayName: '',
  farmCompanyName: '',
};

const NewFarm = ({ history, user, createNewFarm }) => {
  const redirectOnClick = () => {
    history.push(`/farms/`);
  };

  const onSubmit = (values) => {
    if (!user) return;
    const details = { ...values, creatingUserId: user?.uid ?? user?.id };
    INITIAL_FORM_STATE?.farmImage
      ? createNewFarm({
          ...details,
          farmImage: INITIAL_FORM_STATE?.farmImage,
        })
      : createNewFarm(details);
    history.push('/farms/');
  };

  const handleImageUploaded = (image) => {
    if (!image) return;
    INITIAL_FORM_STATE.farmImage = image;
  };

  return (
    <MainContainer maxWidth="sm">
      <Box m={3}>
        <Button
          variant="text"
          startIcon={<ArrowBackIcon />}
          onClick={redirectOnClick}
        >
          Back to Farms
        </Button>
      </Box>
      <FormIntro>
        <FormImage src={newFarmIcon} />
        <h2>WELCOME TO YOUR NEW FARM!</h2>
        <span>Please provide us with some information about your farm:</span>

        <UploadOrSnapshot captureCompleteCallback={handleImageUploaded} />
      </FormIntro>
      <Formik
        initialValues={{
          ...INITIAL_FORM_STATE,
        }}
        validationSchema={FORM_VALIDATION}
        onSubmit={onSubmit}
      >
        <Form>
          <Box m={1}>
            <Box py={1}>
              <TextField label="Display Name" name="displayName" />
            </Box>
            <Box py={1}>
              <TextField label="Company Name" name="farmCompanyName" />
            </Box>
            <Box py={1}>
              <SubmitButton type="submit">Create Farm</SubmitButton>
            </Box>
          </Box>
        </Form>
      </Formik>
    </MainContainer>
  );
};

const mapStateToProps = createStructuredSelector({
  user: selectCurrentUser,
});

const mapDispacthToProps = (dispatch) => ({
  createNewFarm: (farmDetails) => dispatch(createFarmStart(farmDetails)),
});

export default connect(
  mapStateToProps,
  mapDispacthToProps,
)(withRouter(NewFarm));

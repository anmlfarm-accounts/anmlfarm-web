import React from 'react';
import { withRouter } from 'react-router';
import { Avatar } from '@material-ui/core';
import './farm-header.styles.scss';
import { createStructuredSelector } from 'reselect';
import { selectCurrentFarm } from '../../../redux/farms/farm.selectors';
import { connect } from 'react-redux';

const FarmHeader = ({ farm, history }) => {
  const redirectOnClick = () => {
    history.push(`/farms/`);
  };
  return (
    <div className="header" onClick={redirectOnClick}>
      <div className="farm-pic">
        {farm && farm.farmImage ? (
          <Avatar className="avatar" src={farm?.farmImage} />
        ) : (
          <Avatar className="avatar">
            {farm?.displayName ? farm?.displayName[0] : '?'}
          </Avatar>
        )}
      </div>
      <div className="welcome-text">
        {farm?.displayName ?? 'No Farm Found. Tap To Create a Farm.'}
      </div>
    </div>
  );
};
const mapStateToProps = createStructuredSelector({
  farm: selectCurrentFarm,
});

export default connect(mapStateToProps)(withRouter(FarmHeader));

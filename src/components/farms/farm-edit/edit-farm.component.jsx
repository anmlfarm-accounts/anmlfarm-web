import React, { useState } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { Formik, Form } from 'formik';
import * as Yup from 'yup';
import { createStructuredSelector } from 'reselect';
import {
  updateFarmStart,
  deleteFarmStart,
} from '../../../redux/farms/farm.actions';
import { selectCurrentUser } from '../../../redux/users/user.selectors';
import { MainContainer } from '../../../styles/info.pages/info.page.styles';
import TextField from '../../shared/formik-form/text-field/text-field';
import SubmitButton from '../../shared/formik-form/submit-button/ButtonSubmit';
import UploadOrSnapshot from '../../shared/upload-or-snapshot/upload-or-snapshot.component';
import editFarmIcon from './Assets/edit-farm.svg';
import {
  FormImage,
  FormIntro,
} from '../../../styles/form-pages/form-pages.styles';
import { Box, Button } from '@material-ui/core';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import { selectCurrentFarm } from '../../../redux/farms/farm.selectors';
import CustomButton from '../../shared/custom-button/custom-button.component';
import DeletePopup from '../../shared/delete-popup/delete-popup.component';

const FORM_VALIDATION = Yup.object().shape({
  displayName: Yup.string().required('Farm Display Name is Required'),
  farmCompanyName: Yup.string().required('Company Name is Required'),
});

const EditFarm = ({ history, user, farm, editFarm, deleteFarm }) => {
  const [openUndo, setOpenUndo] = useState(false);

  const INITIAL_FORM_STATE = {
    displayName: farm?.displayName,
    farmCompanyName: farm?.farmCompanyName,
    creatingUserId: farm?.creatingUserId,
    farmImage: farm?.farmImage,
    id: farm?.id,
  };

  const redirectOnClick = () => {
    history.push(`/farms/`);
  };

  const removeFarm = () => {
    deleteFarm(farm);
    history.push(`/farms`);
  };

  const onSubmit = (values) => {
    if (!user) return;
    INITIAL_FORM_STATE?.farmImage
      ? editFarm({
          ...values,
          farmImage: INITIAL_FORM_STATE?.farmImage,
        })
      : editFarm(values);
    history.push(`/farms/info/${farm?.farmId ?? farm?.id}`);
  };

  const handleImageUploaded = (image) => {
    if (!image) return;
    INITIAL_FORM_STATE.farmImage = image;
  };

  return (
    <MainContainer maxWidth="sm">
      <Box m={3}>
        <Button
          variant="text"
          startIcon={<ArrowBackIcon />}
          onClick={redirectOnClick}
        >
          Back to Farms
        </Button>
      </Box>
      <FormIntro>
        <FormImage src={editFarmIcon} atl="Edit Farm Icon" />
        <h2>EDIT YOUR FARM!</h2>
        <UploadOrSnapshot captureCompleteCallback={handleImageUploaded} />
      </FormIntro>
      <Formik
        initialValues={{
          ...INITIAL_FORM_STATE,
        }}
        validationSchema={FORM_VALIDATION}
        onSubmit={onSubmit}
      >
        <Form>
          <Box m={1}>
            <Box py={1}>
              <TextField label="Display Name" name="displayName" />
            </Box>
            <Box py={1}>
              <TextField label="Company Name" name="farmCompanyName" />
            </Box>
            <Box py={1}>
              <SubmitButton type="submit">Update Farm</SubmitButton>
            </Box>
          </Box>
        </Form>
      </Formik>
      <CustomButton type="delete" onClick={() => setOpenUndo(true)}>
        DELETE FARM
      </CustomButton>
      <DeletePopup
        record="Farm"
        deleteRecord={removeFarm}
        popupOpen={openUndo}
        cancel={() => setOpenUndo(false)}
        onClick={() => setOpenUndo(false)}
      />
    </MainContainer>
  );
};

const mapStateToProps = createStructuredSelector({
  user: selectCurrentUser,
  farm: selectCurrentFarm,
});

const mapDispacthToProps = (dispatch) => ({
  editFarm: (farmDetails) => dispatch(updateFarmStart(farmDetails)),
  deleteFarm: (farmDetails) => dispatch(deleteFarmStart(farmDetails)),
});

export default connect(
  mapStateToProps,
  mapDispacthToProps,
)(withRouter(EditFarm));

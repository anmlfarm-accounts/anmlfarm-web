import React, { useState } from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { Formik, Form } from 'formik';
import * as Yup from 'yup';
import {
  addFarmWorkerStart,
  deleteInvitedWorkerStart,
  deleteFarmWorkerStart,
} from '../../../redux/farms/farm.actions';
import {
  selectCurrentFarm,
  selectCurrentSelectedFarmWorkersArray,
  selectCurrentSelectedInvitedWorkersArray,
  selectUserCanModifyFarm,
} from '../../../redux/farms/farm.selectors';
import TextField from '../../shared/formik-form/text-field/text-field';
import SubmitButton from '../../shared/formik-form/submit-button/ButtonSubmit';
import {
  GridPadding20,
  TextCenter,
} from '../../../styles/shared/shared.styles';
import { Typography } from '@material-ui/core';
import { Box } from '@material-ui/core';
import InvitedWorkersTable from '../farm-invited-workers-table/farm-invited-workers-table.component';
// import FarmWorkersTable from '../farm-workers-table/farm-workers-table.component';

const FORM_VALIDATION = Yup.object().shape({
  email: Yup.string().email('Invalid email.').required('Email is Required'),
});

const INITIAL_FORM_STATE = {
  email: '',
};

const FarmAddWorkers = ({
  addWorker,
  farmWorkers,
  invitedWorkers,
  farmDetails,
  cancelInvitedWorker,
  cancelFarmWorker,
  farmAuth,
}) => {
  const deleteInvitedWorker = (worker) => {
    const filteredUsers = invitedWorkers.filter((f) => f !== worker);
    cancelInvitedWorker({ ...farmDetails, invitedUsers: [...filteredUsers] });
  };

  const deleteFarmWorker = (worker) => {
    const filteredUsers = farmWorkers.filter((f) => f !== worker);
    cancelFarmWorker({ ...farmDetails, workingUsers: [...filteredUsers] });
  };

  const onSubmit = (values, props) => {
    addWorker(farmDetails, values.email.toLowerCase());
    props.resetForm();
  };

  return (
    <div>
      <div>
        {farmWorkers.length > 0 ? (
          <div>
            {/* <FarmWorkersTable 
            // editWorker={editFarmWorker}
            deleteWorker={deleteFarmWorker}
          />
          */}
          </div>
        ) : (
          <GridPadding20>
            <TextCenter>
              <Typography variant="h6" paragraph>
                {farmWorkers?.length} Workers
              </Typography>
            </TextCenter>
          </GridPadding20>
        )}
        {invitedWorkers.length > 0 && (
          <InvitedWorkersTable deleteInvitedWorker={deleteInvitedWorker} />
        )}
      </div>
      {farmAuth && (
        <Box sx={{ maxWidth: 480, margin: 'auto' }}>
          <Typography variant="h5" paragraph>
            Add Farmers:
          </Typography>
          <Formik
            initialValues={{
              ...INITIAL_FORM_STATE,
            }}
            validationSchema={FORM_VALIDATION}
            onSubmit={onSubmit}
          >
            <Form>
              <Box m={1}>
                <Box py={1}>
                  <TextField label="Email" name="email" type="email" />
                </Box>
                <Box py={1}>
                  <SubmitButton type="submit">Add Worker</SubmitButton>
                </Box>
              </Box>
            </Form>
          </Formik>
        </Box>
      )}
    </div>
  );
};

const mapStateToProps = createStructuredSelector({
  farmDetails: selectCurrentFarm,
  farmWorkers: selectCurrentSelectedFarmWorkersArray,
  invitedWorkers: selectCurrentSelectedInvitedWorkersArray,
  farmAuth: selectUserCanModifyFarm,
});

const mapDispatchToProps = (dispatch) => ({
  addWorker: (farmInfo, workerInfo) =>
    dispatch(addFarmWorkerStart(farmInfo, workerInfo)),
  cancelInvitedWorker: (farmInfo) =>
    dispatch(deleteInvitedWorkerStart(farmInfo)),
  cancelFarmWorker: (farmInfo) => dispatch(deleteFarmWorkerStart(farmInfo)),
});

export default connect(mapStateToProps, mapDispatchToProps)(FarmAddWorkers);

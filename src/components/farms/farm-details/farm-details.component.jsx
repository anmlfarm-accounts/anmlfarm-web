import React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import farmStats from './farm-stats.svg';
import FarmDescription from '../farm-description/farm-description.component';
import InfoStats from '../../shared/info-page/info-stats/info-stats.component';

function FarmDetails({ farm, stats }) {
  return (
    <>
      <CssBaseline />
      <FarmDescription farm={farm} />
      {stats ? (
        <InfoStats
          displayName="Farm"
          description="Lorem, ipsum dolor sit amet consectetur adipisicing elit.
            Consectetur nam inventore nemo at maiores amet in veniam fuga,
            numquam ducimus consequuntur neque quo cupiditate dolore ad dolor
            quia quos provident?"
          image={farmStats}
        />
      ) : null}
    </>
  );
}

export default FarmDetails;

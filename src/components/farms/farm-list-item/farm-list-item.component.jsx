import React from 'react';
import { withRouter } from 'react-router-dom';
import { Grid } from '@material-ui/core';
import CardListItem from '../../shared/list-page/list-item';

function FarmListItem({
  history,
  farm: { id, farmId, displayName, farmImage, farmCompanyName },
}) {
  const redirectToFarm = () => {
    const redirectId = farmId ?? id;
    history.push(`/farms/info/${redirectId}`);
  };
  return (
    <Grid onClick={redirectToFarm}>
      <CardListItem
        image={farmImage}
        name={displayName}
        title={farmCompanyName}
      />
    </Grid>
  );
}

export default withRouter(FarmListItem);

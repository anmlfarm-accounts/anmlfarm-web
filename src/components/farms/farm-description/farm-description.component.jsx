import React from 'react';
import {
  DisplayFlex,
  Margin10TopBottom,
  TextCenter,
} from '../../../styles/shared/shared.styles';
import {
  Info,
  InfoBold,
} from '../../../styles/info.pages/info.page.styles';
import InfoDescription from '../../shared/info-page/info-description/info-description.component';

const FarmDescription = ({ farm }) => {
  return (
    <InfoDescription images={[farm.farmImage]}>
      <h1>
        <TextCenter>{farm.displayName}</TextCenter>
      </h1>
      <DisplayFlex>
        <Info>Company Name:</Info>
        <InfoBold>{farm.farmCompanyName}</InfoBold>
      </DisplayFlex>
      <DisplayFlex>
        <Info>Workers:</Info>
        <InfoBold>{farm?.workingUsers?.length ?? 0}</InfoBold>
      </DisplayFlex>
      {farm.size && <DisplayFlex>
        <Info>Size:</Info>
        <InfoBold>{farm.size}</InfoBold>
      </DisplayFlex> }
      <Margin10TopBottom as="p">{farm.description}</Margin10TopBottom>
      {/* <SmallGray>Added 5 days ago</SmallGray> */}
    </InfoDescription>
  );
};

export default FarmDescription;

import React from 'react'
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { selectCurrentUserOwnedFarmsAsArray } from '../../../redux/farms/farm.selectors'
import { selectAllSubscriptionsAsArray } from '../../../redux/subscriptions/subscription.selectors'
import FarmSubscriptionItem from '../farm-subscription/farm-subscription.component';

function FarmSubscriptionList({ownedFarms, allSubs}) {
    return (
        <div>
           { ownedFarms.map(f => (
                <FarmSubscriptionItem farm={f} subscription={allSubs.find(s=> (s.farmId === (f?.id ?? f?.farmId)))} />
           ))}
        </div>
    )
}

const mapStateToProps = createStructuredSelector({
    ownedFarms: selectCurrentUserOwnedFarmsAsArray,
    allSubs: selectAllSubscriptionsAsArray
})

export default connect(mapStateToProps)(FarmSubscriptionList)
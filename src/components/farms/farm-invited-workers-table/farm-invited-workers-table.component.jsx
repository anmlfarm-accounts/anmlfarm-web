import React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import {
  selectCurrentFarm,
  selectCurrentSelectedInvitedWorkersArray,
} from '../../../redux/farms/farm.selectors';
import { Button, Box, Typography } from '@material-ui/core';
import { TextCenter } from '../../../styles/shared/shared.styles';

const InvitedWorkersTable = ({ invitedWorkers, deleteInvitedWorker }) => {
  return (
    <Box sx={{ margin: 'auto' }}>
      <TextCenter>
        <Typography variant="h6" paragraph>
          Invited Workers
        </Typography>
      </TextCenter>
      <TableContainer component={Paper} elevation={0}>
        <Table aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell>Email</TableCell>
              <TableCell align="right">Cancel Invitation</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {invitedWorkers.map((email) => (
              <TableRow key={email}>
                <TableCell component="th" scope="row">
                  {email}
                </TableCell>
                <TableCell align="right">
                  <Button
                    variant="outlined"
                    color="default"
                    onClick={() => deleteInvitedWorker(email)}
                  >
                    Cancel
                  </Button>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </Box>
  );
};

const mapStateToProps = createStructuredSelector({
  farmDetails: selectCurrentFarm,
  invitedWorkers: selectCurrentSelectedInvitedWorkersArray,
});

export default connect(mapStateToProps)(InvitedWorkersTable);

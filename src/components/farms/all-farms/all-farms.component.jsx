import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { createStructuredSelector } from 'reselect';
import {
  selectCurrentUserFollowingFarmsAsArray,
  selectCurrentUserInvitedFarmsAsArray,
  selectCurrentUserManagingFarmsAsArray,
  selectCurrentUserOwnedFarmsAsArray,
  selectCurrentUserWorkingFarmsAsArray,
} from '../../../redux/farms/farm.selectors';
import ProfileHeader from '../../profile/profile-header/profile-header.component';
import CustomButton from '../../shared/custom-button/custom-button.component';
import FarmCollection from '../farm-collection/farm-collection.component';
import './all-farms.styles.scss';
import FarmInvited from '../farm-invited/farm-invited.component';
import { selectCurrentUser } from '../../../redux/users/user.selectors';

function AllFarms({
  selectCurrentUserManagingFarmsAsArray,
  selectCurrentUserWorkingFarmsAsArray,
  selectCurrentUserFollowingFarmsAsArray,
  selectCurrentUserOwnedFarmsAsArray,
  selectCurrentUserInvitedFarmsAsArray,
  user,
  history,
}) {
  return (
    <div className="all-farms">
      <ProfileHeader />
      <FarmCollection
        title="Owned Farms"
        collection={selectCurrentUserOwnedFarmsAsArray}
      />
      <FarmCollection
        title="Managing Farms"
        collection={selectCurrentUserManagingFarmsAsArray}
      />
      <FarmCollection
        title="Working Farms"
        collection={selectCurrentUserWorkingFarmsAsArray}
      />
      <FarmCollection
        title="Following Farms"
        collection={selectCurrentUserFollowingFarmsAsArray}
      />
      <FarmInvited />
      <CustomButton
        onClick={() =>
          user ? history.push('/farms/new') : history.push('/profile/signin')
        }
      >
        {!user ? 'SIGN IN TO' : ''} ADD FARM
      </CustomButton>
    </div>
  );
}

const mapStateToProps = createStructuredSelector({
  selectCurrentUserManagingFarmsAsArray,
  selectCurrentUserWorkingFarmsAsArray,
  selectCurrentUserFollowingFarmsAsArray,
  selectCurrentUserOwnedFarmsAsArray,
  selectCurrentUserInvitedFarmsAsArray,
  user: selectCurrentUser,
});

export default connect(mapStateToProps)(withRouter(AllFarms));

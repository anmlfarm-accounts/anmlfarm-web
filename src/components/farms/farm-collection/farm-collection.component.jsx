import React from 'react';
import FarmListItem from '../farm-list-item/farm-list-item.component';
import './farm-collection.styles.scss';
import { FarmListContainer } from '../../../styles/list.pages/list.page.styles';

function FarmCollection({ title, collection }) {
  if (!collection || collection.length === 0) return null;
  return (
    <FarmListContainer maxWidth="md">
      <span className="sub-heading">{title}</span>
      {collection.map((farm) => (
        <FarmListItem farm={farm} key={farm?.id} />
      ))}
    </FarmListContainer>
  );
}

export default FarmCollection;

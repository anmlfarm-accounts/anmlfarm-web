import { Button } from '@material-ui/core';
import React from 'react'
import { useEffect } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { createStructuredSelector } from 'reselect';
import { updateUserStart } from '../../../redux/users/user.actions';
import { selectCurrentUser } from '../../../redux/users/user.selectors';
import './terms-and-conditions.styles.scss'

function TermsAndConditions({user, history, editUser}) {

    useEffect(() => {
        if(user?.hasDoneTerms) history.push(`/`);
    }, [user, history])
    
    const handleSubmission = async (event) => {
        event.preventDefault();
        if (!user) return;
        editUser({...user, hasDoneTerms: true})
        history.push(`/`);
      };

    return (
        <div className="iframe">
            <div className="button-box">
                <div className="button-group">
                    <Button variant="contained" color="primary" onClick={handleSubmission}>ACCEPT</Button>
                    <Button variant="contained" color="red" onClick={()=>window.location="/static/WELCOME.html"}>DECLINE</Button>
                </div>
            </div>
            <iframe className='frame' url={'/static/TERMS_AND_CONDITIONS.html'} src={'/static/TERMS_AND_CONDITIONS.html'}
                title="FRAME"
                width="100%"
                height="100%"
                allowFullScreen = {true}
                sanbox = "allow-forms
                allow-pointer-lock
                allow-popups
                allow-same-origin
                allow-scripts
                allow-top-navigation"
                allow="camera *; microphone *; accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                frameBorder = {0}
                />
        </div>
    )
}

const mapStateToProps = createStructuredSelector({
    user: selectCurrentUser,
});

const mapDispatchToProps = (dispatch) => ({
    editUser: (userDetails) => dispatch(updateUserStart(userDetails)),
});

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(TermsAndConditions));
import { Button } from '@material-ui/core'
import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom';
import { createStructuredSelector } from 'reselect';
import { updateUserStart } from '../../../redux/users/user.actions';
import { selectCurrentUser } from '../../../redux/users/user.selectors';

function PrivacyPolicy({user, history, editUser}) {

    useEffect(() => {
        if(user?.hasDonePrivacy) history.push(`/`);
    }, [user, history])

    const handleSubmission = async (event) => {
        event.preventDefault();
        if (!user) return;
        editUser({...user, hasDonePrivacy: true})
        history.push(`/`);
      };

    return (
        <div className="iframe">
        <div className="button-box">
            <div className="button-group">
                <Button variant="contained" color="primary" onClick={handleSubmission}>ACCEPT</Button>
                <Button variant="contained" color="red" onClick={()=>window.location="/static/WELCOME.html"}>DECLINE</Button>
            </div>
        </div>
            <iframe className='frame' url={'/static/PRIVACY_POLICY.html'} src={'/static/PRIVACY_POLICY.html'}
                title="FRAME"
                width="100%"
                height="100%"
                allowFullScreen = {true}
                sanbox = "allow-forms
                allow-pointer-lock
                allow-popups
                allow-same-origin
                allow-scripts
                allow-top-navigation"
                allow="camera *; microphone *; accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                frameBorder = {0}
                />
        </div>
    )
}


const mapStateToProps = createStructuredSelector({
    user: selectCurrentUser,
});

const mapDispatchToProps = (dispatch) => ({
    editUser: (userDetails) => dispatch(updateUserStart(userDetails)),
});


export default connect(mapStateToProps, mapDispatchToProps)(withRouter(PrivacyPolicy));
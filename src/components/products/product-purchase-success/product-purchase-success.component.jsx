import React, { useEffect, useState } from 'react'
import { connect } from 'react-redux'
import { clearCart } from '../../../redux/cart/cart-actions'
import { updateOrderStart } from '../../../redux/orders/order.actions'

function ProductPurchaseSuccess({match, history, updateOrder}) {

    const [orderId, setOrderId] = useState(null)

    useEffect(() => {
        setOrderId(match.params.orderId)
        if(orderId) {
            updateOrder({orderId: orderId, id: orderId, paymentSuccess: true, paymentProcessedDate: new Date()})
            clearCart();
            history.push('/products/history');
        }
    }, [match, updateOrder, history, orderId])

    return (
        <div style={{width: '100%', backgroundColor: 'green', borderRadius: '3px', color:'darkgreen', textAlign: 'center'}}>
            ORDER SUCCESS
        </div>
    )
}

const mapDispatchToProps = dispatch => ({
    clearCart: () => dispatch(clearCart()),
    updateOrder: order => dispatch(updateOrderStart(order))
})

export default connect(null, mapDispatchToProps)(ProductPurchaseSuccess);
import React from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { selectProductCategories } from '../../../redux/products/product.selectors';
import ProductCollection from '../product-collection/product-collection.component';
import { Box, Grid } from '@material-ui/core';
import {
  CategoryTitle,
  GriDisplayFlex,
} from '../../../styles/shared/shared.styles';

function ProductCategories({ categories }) {
  return (
    <div>
      <GriDisplayFlex container>
        <Grid item xs={12}>
          {categories
            ? categories.map((cat) => (
                <Box
                  margin="0 24px"
                  paddingBottom="24px"
                  borderBottom="1px solid #eeeeee"
                  m={3}
                  key={cat}
                >
                  <CategoryTitle as="h3">{cat.toUpperCase()}</CategoryTitle>
                  <ProductCollection category={cat} />
                </Box>
              ))
            : null}
        </Grid>
      </GriDisplayFlex>
    </div>
  );
}

const mapStateToProps = createStructuredSelector({
  categories: selectProductCategories,
});

export default connect(mapStateToProps)(ProductCategories);

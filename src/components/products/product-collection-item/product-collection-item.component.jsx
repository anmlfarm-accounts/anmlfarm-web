import PropTypes from 'prop-types';
import { styled } from '@material-ui/core/styles';
import {
  Card,
  Button,
  Typography,
  Box
} from '@material-ui/core';
// routes
//
import { addItemToCart } from '../../../redux/cart/cart-actions';
import { connect } from 'react-redux';
import { LabelRounded } from '@material-ui/icons';

// ----------------------------------------------------------------------

const RootStyle = styled(Card)(({ theme }) => ({
  maxWidth: 480,
  height: '100%',
  margin: 'auto',
  display: 'flex',
  position: 'relative',
  alignItems: 'center',
  flexDirection: 'column',
  padding: theme.spacing(2),
}));

// ----------------------------------------------------------------------

ProductCollectionItem.propTypes = {
  index: PropTypes.number,
  card: PropTypes.object,
};

function ProductCollectionItem({ product, index, addItem }) {
  const {
    id,
    displayName,
    imageURL,
    price,
    subscriptionPeriod,
    isSale,
    stock,
  } = product;

  const buyButtonClickHandler = () => {
    addItem({ id: id, name: displayName, price: price, image: imageURL });
  };

  return (
    <RootStyle key={id}>
      <Typography variant="overline" sx={{ color: 'text.primary' }}>
        {displayName}
      </Typography>

      <Box sx={{ display: 'flex', justifyContent: 'flex-end', my: 1 }}>
        {price > 0 ? (
          <Typography variant="subtitle1" sx={{ color: 'text.secondary' }}>
            R
          </Typography>
        ) : (
          ''
        )}
        <Typography variant="h2" sx={{ mx: 1 }}>
          {price === 0 ? 'Free' : price}
        </Typography>
        {subscriptionPeriod ? (
          <Typography
            gutterBottom
            component="span"
            variant="subtitle2"
            sx={{
              alignSelf: 'flex-end',
              color: 'text.secondary',
            }}
          >
            {subscriptionPeriod}
          </Typography>
        ) : (
          ''
        )}
      </Box>

      <Typography
        variant="caption"
        sx={{
          color: 'primary.main',
          textTransform: 'capitalize',
        }}
      >
        {displayName}
      </Typography>

      <Box sx={{ width: 150, height: 150, position: 'relative' }}>
        <img width="150" height="150" src={imageURL} alt={displayName} />
        {isSale && (
          <Box sx={{ top: 0, right: 0, position: 'absolute' }}>
            <LabelRounded sx={{}}>
              SALE
            </LabelRounded>
          </Box>
        )}
      </Box>

      <Button
        fullWidth
        size="large"
        variant="contained"
        disabled={stock === 0}
        onClick={buyButtonClickHandler}
      >
        {stock > 0 ? `Add To Cart` : `Out Of Stock`}
      </Button>
    </RootStyle>
  );
}

const matchDispatchToProps = (dispatch) => ({
  addItem: (item) => dispatch(addItemToCart(item)),
});

export default connect(null, matchDispatchToProps)(ProductCollectionItem);

import React, { useState } from 'react'
import { useEffect } from 'react'
import { connect } from 'react-redux'
import { clearCart } from '../../../redux/cart/cart-actions'
import { updateOrderStart } from '../../../redux/orders/order.actions'

function ProductPurchaseFailed({match, history, updateOrder}) {

    const [orderId, setOrderId] = useState(null)

    useEffect(() => {
        setOrderId(match.params.orderId)
        if(orderId) {
            updateOrder({orderId: orderId, id: orderId, paymentFailure: true})
            clearCart();
            history.push('/products/history');
        }
    }, [match, updateOrder, history, orderId])

    return (
        <div style={{width: '100%', backgroundColor: 'red', borderRadius: '3px', color:'darkred', textAlign: 'center'}}>
            ORDER FAILED
        </div>
    )
}

const mapDispatchToProps = dispatch => ({
    clearCart: () => dispatch(clearCart()),
    updateOrder: order => dispatch(updateOrderStart(order))
})

export default connect(null, mapDispatchToProps)(ProductPurchaseFailed);

import React from 'react'
import { connect } from 'react-redux'
import { Route, withRouter } from 'react-router-dom'
import { createStructuredSelector } from 'reselect'
import { clearCart } from '../../../redux/cart/cart-actions'
import { startOrderDelete } from '../../../redux/orders/order.actions'
import { selectActiveOrdersAsArray } from '../../../redux/orders/order.selectors'
import { DeleteButton } from '../../../styles/shared/shared.styles'
import PayFastButton from '../../shared/payfast-button/payfast-button.component'
import ProductOrderInfo from '../product-order-info/product-order-info.component'
import ProductPurchaseFailed from '../product-purchase-failed/product-purchase-failed.component'
import ProductPurchaseSuccess from '../product-purchase-success/product-purchase-success.component'

function ProductBilling({match, history, clearCart, orders, cancelOrder}) {
    const abortOrder = (order) => {
        cancelOrder(order);
        clearCart();
        history.push('/products/store');
    }
    return (
        <>
            <Route path={`${match.path}/payment/success/:orderId`} component={ProductPurchaseSuccess} />
            <Route path={`${match.path}/payment/failure/:orderId`} component={ProductPurchaseFailed} />
            {orders &&
            <div style={{display: 'flex', flexDirection: 'column'}}>
                <h3>Order summary</h3>
                <ProductOrderInfo order={orders[0]}/>
                <PayFastButton order={orders[0]}/>
                <DeleteButton onClick={()=>abortOrder(orders[0])}>No Thanks</DeleteButton>
            </div> }
            
        </>
    )
}

const mapStateToProps = createStructuredSelector({
    orders: selectActiveOrdersAsArray
})

const mapDispatchToProps = dispatch => ({
    // setSelectedOrder: (orderId) => dispatch(selectOrder(orderId))
    cancelOrder: (order) => dispatch(startOrderDelete(order)),
    clearCart: () => dispatch(clearCart())
})

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(ProductBilling));
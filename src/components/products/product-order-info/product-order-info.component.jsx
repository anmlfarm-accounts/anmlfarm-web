import { Avatar } from '@material-ui/core';
import React from 'react';
import { calculateOrderTotalItemPrice } from '../../../utils/orders/calculations';

function ProductOrderInfo({ order }) {
  if (!order) return null;
  return (
    <>
      <h3>Order ID: </h3>
      <span>{order?.id}</span>
      {order?.items && (
        <>
          <h3>Items: </h3>
          {order?.items?.map((i, idx) => (
            <div style={{ padding: '20px' }}>
              Item {idx + 1}
              {i.image && <Avatar src={i.image} />}
              <h4>Name:</h4>
              <span>{i.name}</span>
              <h4>Amount:</h4>
              <span>{i.quantity}</span>
              <h4>Price:</h4>
              <span>{i.price}</span>
            </div>
          ))}
          <h3>Total: R {calculateOrderTotalItemPrice(order)}</h3>
        </>
      )}
      {order?.subscriptions && (
        <>
          <h3>Items: </h3>
          {order?.subscriptions.map((i, idx) => (
            <div style={{ padding: '20px' }}>
              {i.image && <Avatar src={i.image} />}
              <h4>Name:</h4>
              <span>ANML FARM Subscription</span>
              <h4>Amount:</h4>
              <span>1</span>
              <h4>Price:</h4>
              <span>300</span>
            </div>
          ))}
          <h3>Total: R 300 p/m</h3>
        </>
      )}
    </>
  );
}

export default ProductOrderInfo;

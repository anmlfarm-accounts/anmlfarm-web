import React from 'react';
import './product-checkout.styles.scss';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import {
  selectCartDelivery,
  selectCartItems,
  selectCartTotal,
} from '../../../redux/cart/cart.selectors';
import ProductCheckoutItem from '../product-checkout-item/product-checkout-item.component';
import { Container, Button } from '@material-ui/core';
import List from '@material-ui/core/List';
import ProductDeliveryOrPickup from '../product-delivery-or-pickup/product-delivery-or-pickup.component';
import {
  selectCurrentUser,
  selectRepCode,
} from '../../../redux/users/user.selectors';
import { startOrderCreate } from '../../../redux/orders/order.actions';
import { withRouter } from 'react-router-dom';

const ProductCheckout = ({
  cartItems,
  cartDelivery,
  total,
  user,
  history,
  createOrder,
  userRepCode,
}) => {
  const createOrderFromCart = () => {
    if (user) {
      createOrder({
        items: cartItems,
        delivery: cartDelivery,
        userId: user?.id ?? user?.uid,
        agentCode: userRepCode,
        createdDate: new Date(),
      });
      history.push('/products/payment');
    } else history.push('/profile/signin');
  };

  return (
    <>
      <Container maxWidth="sm">
        {cartItems && cartItems.length > 0 ? (
          <>
            <List className="cart-items">
              {cartItems.map((ci, idx) => (
                <ProductCheckoutItem key={ci.id ?? idx} cartItem={ci} />
              ))}
            </List>
            <div className="total-wrap">
              <div className="total">
                <span>TOTAL: R{total}</span>
              </div>
            </div>
            <div className="button-container">
              <Button
                variant="contained"
                color="primary"
                onClick={createOrderFromCart}
              >
                Proceed To Payment
              </Button>
            </div>

            <ProductDeliveryOrPickup />
            <span style={{ fontSize: 'small', color: 'grey' }}>
              Placing an item in your shopping cart does not reserve that item
              or price. We only reserve stock for your order once payment is
              received.
            </span>
          </>
        ) : (
          <h2 style={{ margin: 'auto' }}>No products selected :(</h2>
        )}
      </Container>
    </>
  );
};

const mapStateToProps = createStructuredSelector({
  cartItems: selectCartItems,
  cartDelivery: selectCartDelivery,
  total: selectCartTotal,
  user: selectCurrentUser,
  userRepCode: selectRepCode,
});

const mapDispatchToProps = (dispatch) => ({
  createOrder: (orderInfo) => dispatch(startOrderCreate(orderInfo)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withRouter(ProductCheckout));

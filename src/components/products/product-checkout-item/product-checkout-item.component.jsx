import React from 'react';
import { connect } from 'react-redux';
import './product-checkout-item.styles.scss';
import {
  addItemToCart,
  reduceItemInCart,
  removeItemFromCart,
} from '../../../redux/cart/cart-actions';
import { Grid } from '@material-ui/core';
import {
  CartListInfo,
  CartListItem,
} from '../../../styles/product-page/product-page.styles';
import {
  GriDisplayFlex,
  GriDisplayFlexEnd,
  GridRowAlignCenter,
} from '../../../styles/shared/shared.styles';

const ProductCheckoutItem = ({
  removeItemFromCart,
  addItem,
  reduceItem,
  cartItem,
}) => {
  return (
    <CartListItem>
      <GriDisplayFlex container>
        <Grid item xs={3}>
          <div className="img-wrap">
            <img className="cart-img" src={cartItem.image} alt={cartItem.name} />
          </div>
        </Grid>
        <CartListInfo item xs={9}>
          <div className="name">{cartItem.name}</div>
          <GriDisplayFlexEnd container>
            <GridRowAlignCenter>
              <div className="arrow" onClick={() => reduceItem(cartItem)}>
                &#10094;
              </div>
              <div className="value">{cartItem.quantity}</div>
              <div className="arrow" onClick={() => addItem(cartItem)}>
                &#10095;
              </div>
            </GridRowAlignCenter>
            <div className="amount">R {cartItem.price * cartItem.quantity}</div>
            <div
              className="remove-button"
              onClick={() => removeItemFromCart(cartItem)}
            >
              &#10005;
            </div>
          </GriDisplayFlexEnd>
        </CartListInfo>
      </GriDisplayFlex>
    </CartListItem>
  );
};

const mapDispatchToProps = (dispatch) => ({
  removeItemFromCart: (cartItem) => dispatch(removeItemFromCart(cartItem)),
  addItem: (cartItem) => dispatch(addItemToCart(cartItem)),
  reduceItem: (cartItem) => dispatch(reduceItemInCart(cartItem)),
});

export default connect(null, mapDispatchToProps)(ProductCheckoutItem);

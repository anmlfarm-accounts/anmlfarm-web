import { Button } from '@material-ui/core'
import React, { useState } from 'react'

export default function ProductDeliveryOrPickup() {
    const estimatedCollection = new Date(new Date().getTime()+(7*24*60*60*1000));

    const [collect, setCollect] = useState('collect');

    return (
        <div style={{display: 'flex', flexDirection: 'column', alignContent: 'space-around'}}>
            <Button disabled={true} onClick={() => setCollect('collect')}>Deliver To Door [NOT YET AVAILABLE]</Button>
            <Button onClick={() => setCollect('collect')}>Collect My Order</Button>
            {collect === 'collect' && 
                <>
                    <h5>Address:</h5>
                    <p>Plot 136 , Karee Road , Rayton</p>
                    <a href="https://www.google.com/maps/place/Kareeweg/@-25.7667009,28.510014,228m/data=!3m1!1e3!4m15!1m9!4m8!1m3!2m2!1d28.510618!2d-25.7664624!1m3!2m2!1d28.5100354!2d-25.7667934!3m4!1s0x1e9550c7126721df:0x2f262c89e13722dd!8m2!3d-25.7666957!4d28.5097877" style={{outline:'none'}} tabIndex={-1} target="_blank" rel="noreferrer">
                        <img alt="Rayton" align="center" border="0" className="center autowidth" src="https://d15k2d11r6t6rl.cloudfront.net/public/users/Integrators/BeeProAgency/701032_683715/karee.png" style={{textDecoration: 'none', height: 'auto', border: 0, width: '207px', maxWidth: '100%', display: 'block' }} />
                    </a>
                </>
            }
            <span>Estimated Collection Date: {estimatedCollection.toString()}</span>
        </div>
    )
}

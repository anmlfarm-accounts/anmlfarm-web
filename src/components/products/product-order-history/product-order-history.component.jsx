import React from 'react'
import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'
import { selectHistoricOrdersAsArray } from '../../../redux/orders/order.selectors'

function ProductOrderHistory({orders}) {
    return (
        <div>
            {orders.map((o,idx)=>(
            <div style={{display:'flex',flexDirection:'row', justifyContent: 'space-evenly'}}>
                <h2>Order:</h2><span> {o.id}</span>
                {o.paymentSuccess && < div className="success" style={{backgroundColor:'green', color:'darkgreen', borderRadius: '10px', width: '100px', textAlign: 'center', margin: '10px'}}>SUCCESS</div>}
                {o.paymentFailure && <div className="success" style={{backgroundColor:'green', color:'darkgreen', borderRadius: '10px', width: '100px', textAlign: 'center', margin: '10px'}}>FAILED</div>}
            </div>
            ))}
        </div>
    )
}

const mapStateToProps = createStructuredSelector({
    orders: selectHistoricOrdersAsArray
})

export default connect(mapStateToProps)(ProductOrderHistory)
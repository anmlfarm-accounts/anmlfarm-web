import React from 'react';
import { connect } from 'react-redux';
import { selectProductsByCategory } from '../../../redux/products/product.selectors';
import ProductCollectionItem from '../product-collection-item/product-collection-item.component';
import { Grid } from '@material-ui/core';
import { GriDisplayFlex } from '../../../styles/shared/shared.styles';

function ProductCollection({ category, selectProductsByCategory }) {
  return (
    <GriDisplayFlex key={category} container spacing={1}>
      {selectProductsByCategory(category)
        ? selectProductsByCategory(category).map((product,idx) => (
            <Grid item xs={12} sm={6} md={4} key={product.id??idx}>
              <ProductCollectionItem key={product.id??idx} product={product} />
            </Grid>
          ))
        : null}
    </GriDisplayFlex>
  );
}

const mapStateToProps = (state) => ({
  selectProductsByCategory: (category) =>
    selectProductsByCategory(category)(state),
});

export default connect(mapStateToProps)(ProductCollection);

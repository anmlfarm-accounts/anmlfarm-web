import React from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { selectCurrentSelectedAnimalMedicationsAsArray } from '../../../redux/animals/animal.selectors';
import Battery60Icon from '@material-ui/icons/Battery60';
import {
  Timeline,
  TimelineItem,
  TimelineContent,
  TimelineSeparator,
  TimelineConnector,
  TimelineOppositeContent,
} from '@material-ui/lab';
import { Grid, Typography } from '@material-ui/core';
import MTimelineDot from '../../minimal-ui/@material-extend/MTimelineDot';
import { Bold, TimelinePaper } from '../../../styles/shared/shared.styles';

const AnimalMedicationTable = ({ animalMedications }) => {
  return (
    <Grid item xs={12}>
      <Timeline align="alternate">
        {animalMedications.map((item) => (
          <TimelineItem key={item.dateTime}>
            <TimelineOppositeContent>
              <Typography variant="body2" sx={{ color: 'text.secondary' }}>
                {item.medicationDate}
              </Typography>
            </TimelineOppositeContent>
            <TimelineSeparator>
              <MTimelineDot color="primary">
                <Battery60Icon color="primary" />
              </MTimelineDot>
              <TimelineConnector />
            </TimelineSeparator>
            <TimelineContent>
              <TimelinePaper elevation={0}>
                <Bold as="h5">{item.medication}</Bold>
                <Typography variant="body2" sx={{ color: 'text.secondary' }}>
                  {item.dosage}
                </Typography>
              </TimelinePaper>
            </TimelineContent>
          </TimelineItem>
        ))}
      </Timeline>
    </Grid>
  );
};

const mapStateToProps = createStructuredSelector({
  animalMedications: selectCurrentSelectedAnimalMedicationsAsArray,
});

export default connect(mapStateToProps)(AnimalMedicationTable);

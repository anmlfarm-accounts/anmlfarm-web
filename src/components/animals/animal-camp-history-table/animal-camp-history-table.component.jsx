import React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { calculateTimeDifference } from '../../../utils/date/calculations';
import { connect } from 'react-redux';
import {
  selectCurrentSelectedAnimal,
  selectCurrentSelectedAnimalCampHistoryAsArray,
} from '../../../redux/animals/animal.selectors';
import { createStructuredSelector } from 'reselect';

const AnimalCampHistoryTable = ({ animalCampHistory, animalDetails }) => {
  return (
    <div>
      <TableContainer component={Paper} elevation={0}>
        <Table aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell>Camp Name</TableCell>
              <TableCell align="right">Moved On</TableCell>
              <TableCell align="right">Stayed Until</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {animalCampHistory.map((history) => (
              <TableRow key={history.dateTime.seconds}>
                <TableCell component="th" scope="row">
                  {history.displayName}
                </TableCell>
                <TableCell align="right">{history.movedOn}</TableCell>
                <TableCell align="right">
                  {calculateTimeDifference(
                    history.movedOn,
                    animalDetails.animalBirthday,
                  )}
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </div>
  );
};

const mapStateToProps = createStructuredSelector({
  animalDetails: selectCurrentSelectedAnimal,
  animalCampHistory: selectCurrentSelectedAnimalCampHistoryAsArray,
});

export default connect(mapStateToProps)(AnimalCampHistoryTable);

import React, { useEffect } from 'react';
import { createStructuredSelector } from 'reselect';
import {
  WhatsappShareButton,
  FacebookShareButton,
  FacebookIcon,
  WhatsappIcon,
} from 'react-share';
import { connect } from 'react-redux';
import {
  animalLoading,
  selectCurrentSelectedAnimal,
  selectCurrentSelectedAnimalChildrenAsArray,
  selectUserCanModifyAnimal,
} from '../../../redux/animals/animal.selectors';
import { setSelectedAnimal } from '../../../redux/animals/animal.actions';
import { selectCurrentAnimalDevices } from '../../../redux/devices/device.selectors';
import CustomButton from '../../shared/custom-button/custom-button.component';
import {
  Info,
  InfoBold,
  MainContainer,
} from '../../../styles/info.pages/info.page.styles';
import FileUpload from '../../shared/file-upload/file-upload.component';
import FitnessCenterIcon from '@material-ui/icons/FitnessCenter';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import Battery50Icon from '@material-ui/icons/Battery50';
import MenuBookIcon from '@material-ui/icons/MenuBook';
import WhatshotIcon from '@material-ui/icons/Whatshot';
import CompareIcon from '@material-ui/icons/Compare';
import LocalCafeIcon from '@material-ui/icons/LocalCafe';
import DeviceHubIcon from '@material-ui/icons/DeviceHub';
import AnimalWeight from '../animal-weight/animal-weight.component';
import AnimalStory from '../animal-story/animal-story.component';
import AnimalMap from '../animal-map/animal-map.component';
import TagWriter from '../../shared/tag-writer/tag-writer.component';
import AnimalMedication from '../animal-medication/animal-medication.component';
import AccordionItem from '../../minimal-ui/accordion/Accordion';
import AnimalDescription from '../animal-description/animal-description.component';
// import ChromecastButton from '../../shared/chrome-share/chrome-share.component';
import AnimalHeader from '../animal-header/animal-header.component';
import AnimalFamily from '../animal-family/animal-family.component';
import AnimalMilk from '../animal-milk/animal-milk.component';
import AnimalHeat from '../animal-heat/animal-heat.component';
import AnimalCampHistory from '../animal-camp-history/animal-camp-history.component';
import LoadingFullScreen from '../../shared/loading/loading-fullscreen.component';

const AnimalInfo = ({
  animalDetails,
  animalDevices,
  animalAuth,
  match,
  history,
  setSelectedAnimal,
  animalChildren,
  loading,
}) => {
  const url = window?.location?.href?.toString();

  useEffect(() => {
    const { animalId } = match.params;
    if (
      animalId &&
      animalId !== (animalDetails?.animalId ?? animalDetails?.id)
    ) {
      setSelectedAnimal(animalId);
    }
  }, [match, animalDetails, setSelectedAnimal]);

  if (!animalDetails) return null;

  return (
    <>
      <AnimalHeader />
      <TagWriter />
      <WhatsappShareButton title={animalDetails?.displayName} url={url}>
        <WhatsappIcon />
      </WhatsappShareButton>
      <FacebookShareButton
        quote={animalDetails?.displayName}
        hashtag={`#anmlfarm`}
        url={url}
        href={url}
      >
        <FacebookIcon />
      </FacebookShareButton>
      {/* <ChromecastButton images={animalDetails?.animalImages} /> */}
      <MainContainer maxWidth="md">
        <AnimalDescription animal={animalDetails} />
        <AccordionItem
          title="Weight"
          icon={<FitnessCenterIcon width={20} height={20} />}
        >
          <AnimalWeight />
        </AccordionItem>
        {animalAuth && (
          <AccordionItem
            title="File Uploads"
            icon={<CloudUploadIcon width={20} height={20} />}
          >
            <FileUpload forAnimal={animalDetails} />
          </AccordionItem>
        )}
        <AccordionItem
          title="Medication"
          icon={<Battery50Icon width={20} height={20} />}
        >
          <AnimalMedication />
        </AccordionItem>
        {(animalDetails?.animalGender === 'Ewe' ||
          animalDetails?.animalGender === 'Female') && (
          <AccordionItem
            title="Milk"
            icon={<LocalCafeIcon width={20} height={20} />}
          >
            <AnimalMilk />
          </AccordionItem>
        )}
        {(animalDetails?.animalGender === 'Ewe' ||
          animalDetails?.animalGender === 'Female') && (
          <AccordionItem
            title="Heat Pattern"
            icon={<WhatshotIcon width={20} height={20} />}
          >
            <AnimalHeat />
          </AccordionItem>
        )}
        {animalAuth && (
          <AccordionItem
            title="Camp History"
            icon={<CompareIcon width={20} height={20} />}
          >
            <AnimalCampHistory />
          </AccordionItem>
        )}
        {animalAuth && (
          <AccordionItem
            title="Animal Story"
            icon={<MenuBookIcon width={20} height={20} />}
          >
            <AnimalStory />
          </AccordionItem>
        )}

        {(animalDetails.animalFather ||
          animalDetails.animalMother ||
          animalChildren.length > 0) && (
          <AccordionItem
            title="Animal Family"
            icon={<DeviceHubIcon width={20} height={20} />}
          >
            <AnimalFamily />
          </AccordionItem>
        )}

        {animalDevices &&
        animalDevices.length !== 0 &&
        animalDevices[0].lastUpdated ? (
          <AccordionItem title="GPS Coordinates">
            <div>
              <Info>Last Updated:</Info>
              <InfoBold>
                {new Date(
                  (animalDevices[0].lastUpdated?.seconds ?? 0) * 1000,
                ).toLocaleString()}
              </InfoBold>
            </div>
            <AnimalMap device={animalDevices[0]} />
          </AccordionItem>
        ) : null}
        {animalAuth && (
          <>
            <CustomButton
              onClick={() =>
                history.push(
                  `/animals/edit/${
                    animalDetails?.id ?? animalDetails?.animalId
                  }`,
                )
              }
            >
              EDIT ANIMAL
            </CustomButton>
          </>
        )}
      </MainContainer>
      {loading && <LoadingFullScreen />}
    </>
  );
};

const mapStateToProps = createStructuredSelector({
  animalDetails: selectCurrentSelectedAnimal,
  animalDevices: selectCurrentAnimalDevices,
  animalAuth: selectUserCanModifyAnimal,
  animalChildren: selectCurrentSelectedAnimalChildrenAsArray,
  loading: animalLoading,
});

const mapDispatchToProps = (dispatch) => ({
  setSelectedAnimal: (animalId) => dispatch(setSelectedAnimal(animalId)),
});

export default connect(mapStateToProps, mapDispatchToProps)(AnimalInfo);

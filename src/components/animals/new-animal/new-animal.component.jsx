import React from 'react';
import { Button, Box } from '@material-ui/core';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { createStructuredSelector } from 'reselect';
import { Formik, Form } from 'formik';
import * as Yup from 'yup';
import { createAnimalStart } from '../../../redux/animals/animal.actions';
import { selectCurrentCamp } from '../../../redux/camps/camp.selectors';
import { selectCurrentFarm } from '../../../redux/farms/farm.selectors';
import { selectCurrentUser } from '../../../redux/users/user.selectors';
import TextField from '../../shared/formik-form/text-field/text-field';
import SubmitButton from '../../shared/formik-form/submit-button/ButtonSubmit';
import UploadOrSnapshot from '../../shared/upload-or-snapshot/upload-or-snapshot.component';
import newAnimalIcon from './Assets/new-animal.svg';
import { MainContainer } from '../../../styles/info.pages/info.page.styles';
import {
  FormImage,
  FormIntro,
} from '../../../styles/form-pages/form-pages.styles';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import DateTimeField from '../../shared/formik-form/datetime-field/datetime-field.component';
import RadioButtonField from '../../shared/formik-form/radio-button/radio-button.component';

const FORM_VALIDATION = Yup.object().shape({
  displayName: Yup.string().required('Animal Name is Required'),
  animalBreed: Yup.string().required('Breed is Required'),
  animalType: Yup.string().required('Type is Required'),
  animalTeeth: Yup.number().typeError('Must be a number'),
  tagNumber: Yup.string()
    .matches(/^\S*$/, 'No spaces')
    .required('Tag Number is Required'),
});

const INITIAL_FORM_STATE = {
  displayName: '',
  animalBirthday: '',
  animalBreed: '',
  animalGender: '',
  animalTeeth: '',
  animalFather: '',
  animalMother: '',
  animalLotNumber: '',
  animalStory: '',
  animalType: '',
  animalImages: [],
  creatingUserId: '',
  studStatus: '',
  tagNumber: '',
};

const NewAnimal = ({ user, createNewAnimal, history, camp, farm }) => {
  const redirectOnClick = () => {
    history.goBack();
  };

  const onSubmit = (values) => {
    if (!user) return;
    createNewAnimal({
      ...values,
      creatingUserId: user?.uid ?? user?.id,
      farmId: farm?.farmId ?? farm?.id,
      campId: camp?.campId ?? camp?.id,
      animalImages: INITIAL_FORM_STATE?.animalImages,
      tagNumber: values.tagNumber.trim(),
    });
    history.push(`/camps/info/${camp?.id ?? camp?.campId}`);
  };

  const handleImageUploaded = (image) => {
    if (!image) return;
    const animalImages = INITIAL_FORM_STATE.animalImages ?? [];
    animalImages.push(image);
  };

  return (
    <MainContainer maxWidth="sm">
      <Box m={3}>
        <Button
          variant="text"
          startIcon={<ArrowBackIcon />}
          onClick={redirectOnClick}
        >
          Go Back
        </Button>
      </Box>
      <FormIntro>
        <FormImage src={newAnimalIcon} />
        <h2>CONGRATS ON YOUR NEW ANIMAL!</h2>
        <span>Please provide us with some information about your animal:</span>
        <UploadOrSnapshot captureCompleteCallback={handleImageUploaded} />
      </FormIntro>
      <Formik
        initialValues={{
          ...INITIAL_FORM_STATE,
        }}
        validationSchema={FORM_VALIDATION}
        onSubmit={onSubmit}
      >
        {({ values }) => (
          <Form>
            <Box m={1}>
              <Box py={1}>
                <TextField label="Display Name" name="displayName" />
              </Box>
              <Box py={1}>
                <TextField
                  label="Tag Number"
                  name="tagNumber"
                  value={values.tagNumber.toUpperCase()}
                />
              </Box>
              <Box py={1}>
                <TextField label="Animal Type" name="animalType" />
              </Box>
              <Box py={1}>
                <TextField label="Animal Breed" name="animalBreed" />
              </Box>
              <Box py={1}>
                <TextField label="Mother" name="animalMother" />
              </Box>
              <Box py={1}>
                <TextField label="Father" name="animalFather" />
              </Box>
              <Box py={1}>
                <DateTimeField label="Animal Birthday" name="animalBirthday" />
              </Box>
              <Box py={1}>
                <TextField
                  label="Animal Teeth"
                  type="number"
                  name="animalTeeth"
                />
              </Box>
              <Box py={1}>
                <TextField label="Animal Lot Number" name="animalLotNumber" />
              </Box>
              <Box py={1}>
                <RadioButtonField
                  label="Animal Gender"
                  name="animalGender"
                  options={['Female', 'Male']}
                />
              </Box>
              <Box py={1}>
                <RadioButtonField
                  label="Stud Status"
                  name="studStatus"
                  options={['Verified', 'Unverified']}
                />
              </Box>
              <Box py={1}>
                <TextField
                  label="Animal Story"
                  name="animalStory"
                  multiline={true}
                  rows={3}
                />
              </Box>
              <Box py={1}>
                <SubmitButton type="submit">Create Animal</SubmitButton>
              </Box>
            </Box>
          </Form>
        )}
      </Formik>
    </MainContainer>
  );
};

const mapStateToProps = createStructuredSelector({
  user: selectCurrentUser,
  camp: selectCurrentCamp,
  farm: selectCurrentFarm,
});

const mapDispacthToProps = (dispatch) => ({
  createNewAnimal: (animalDetails) =>
    dispatch(createAnimalStart(animalDetails)),
});

export default connect(
  mapStateToProps,
  mapDispacthToProps,
)(withRouter(NewAnimal));

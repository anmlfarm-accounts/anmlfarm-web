import React, { useState } from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { Formik, Form } from 'formik';
import {
  addAnimalHeatStart,
  deleteLatestAnimalHeatStart,
} from '../../../redux/animals/animal.actions';
import {
  selectCurrentSelectedAnimal,
  selectCurrentSelectedAnimalHeatAsArray,
  selectUserCanModifyAnimal,
} from '../../../redux/animals/animal.selectors';
import SubmitButton from '../../shared/formik-form/submit-button/ButtonSubmit';
import CheckboxField from '../../shared/formik-form/checkbox-field/checkbox-field';
import DateTimeField from '../../shared/formik-form/datetime-field/datetime-field.component';
import {
  GridPadding20,
  ColumnFlexEnd,
  TextCenter,
} from '../../../styles/shared/shared.styles';
import { Typography, Button } from '@material-ui/core';
import IconButton from '@material-ui/core/IconButton';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import { fDate, fDateYear } from '../../minimal-ui/utils/formatTime';
import { Box } from '@material-ui/core';
import AnimalHeatTable from '../animal-heat-timeline/animal-heat-timeline.component';
import AnimalUndoPopup from '../animal-undo-popup/animal-undo-popup.component';
import SetReminder from '../../reminders/set-reminder/set-reminder.component';
import { generateAnimalHeatReminder } from '../../../redux/reminders/reminder.selectors';

const INITIAL_FORM_STATE = {
  heatDate: fDateYear(new Date()),
};

const AnimalHeat = ({
  addHeat,
  animalDetails,
  animalHeat,
  animalAuth,
  deleteLatestHeat,
  generateHeatReminder,
}) => {
  const [isChecked, setIsChecked] = useState(true);
  const [calender, setCalender] = useState(false);
  const [menu, setMenu] = useState(false);
  const [openUndo, setOpenUndo] = useState(false);

  const deleteAnimalLatestHeat = () => {
    deleteLatestHeat(animalDetails);
    setOpenUndo(false);
  };

  const addAnimalHeat = (values, props) => {
    addHeat(animalDetails, {
      ...values,
      heatDate: fDate(values?.heatDate),
      dateTime: new Date(),
    });
    props.resetForm();
  };

  return (
    <div>
      <div>
        {animalHeat.length > 0 ? (
          <>
            <div>
              {!animalDetails?.animalEnd && (
                <ColumnFlexEnd align="right">
                  <IconButton onClick={() => setMenu(!menu)}>
                    <MoreVertIcon />
                  </IconButton>
                  {menu && animalAuth && (
                    <Button onClick={() => setOpenUndo(true)}>
                      Undo last Heat
                    </Button>
                  )}
                </ColumnFlexEnd>
              )}
            </div>
            <AnimalHeatTable />
          </>
        ) : (
          <GridPadding20>
            <TextCenter>
              <Typography variant="h6" paragraph>
                No Heat to show
              </Typography>
            </TextCenter>
          </GridPadding20>
        )}
      </div>
      <AnimalUndoPopup
        record="heat"
        deleteRecord={deleteAnimalLatestHeat}
        popupOpen={openUndo}
        cancel={() => setOpenUndo(false)}
        onClick={() => setOpenUndo(false)}
      />
      {animalAuth && !animalDetails?.animalEnd && (
        <Box sx={{ maxWidth: 480, margin: 'auto' }}>
          <Formik
            initialValues={{
              ...INITIAL_FORM_STATE,
            }}
            onSubmit={addAnimalHeat}
          >
            <Form>
              <Box m={1}>
                <Box py={1}>
                  <CheckboxField
                    label="Today"
                    onChange={(e) => setIsChecked(e.currentTarget.checked)}
                    checked={isChecked}
                    onClick={() => setCalender(!calender)}
                  />
                </Box>
                {calender && (
                  <Box py={1}>
                    <DateTimeField label="Date" name="heatDate" />
                  </Box>
                )}
                <Box py={1}>
                  <SubmitButton type="submit">Add Heat</SubmitButton>
                </Box>
              </Box>
            </Form>
          </Formik>
          <SetReminder reminderDetails={generateHeatReminder} />
        </Box>
      )}
    </div>
  );
};

const mapStateToProps = createStructuredSelector({
  animalDetails: selectCurrentSelectedAnimal,
  animalHeat: selectCurrentSelectedAnimalHeatAsArray,
  animalAuth: selectUserCanModifyAnimal,
  generateHeatReminder: generateAnimalHeatReminder,
});

const mapDispatchToProps = (dispatch) => ({
  addHeat: (animalInfo, heatInfo) =>
    dispatch(addAnimalHeatStart(animalInfo, heatInfo)),
  deleteLatestHeat: (animalInfo) =>
    dispatch(deleteLatestAnimalHeatStart(animalInfo)),
});

export default connect(mapStateToProps, mapDispatchToProps)(AnimalHeat);

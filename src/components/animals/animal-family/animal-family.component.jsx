import { Typography } from '@material-ui/core';
import { Box } from '@material-ui/core';
import React from 'react'
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { createStructuredSelector } from 'reselect';
import { selectCurrentSelectedAnimal, selectCurrentSelectedAnimalChildrenAsArray, selectCurrentSelectedAnimalFatherAsArray, selectCurrentSelectedAnimalMotherAsArray, selectCurrentSelectedAnimalSiblingsAsArray } from '../../../redux/animals/animal.selectors';
import AnimalListItem from '../animal-list-item/animal-list-item.component';

function AnimalFamily({mother, father, siblings, children}) {
    return (
        <div>
            
            {mother.length > 0 && (
        <>
          <Box sx={{ textAlign: 'center', fontWeight: 600 }}>
            <Typography variant="h5" paragraph>
            Mother:
            </Typography>
          </Box>
          <div className="animal-list">
            {mother.map((mom) => (
              <AnimalListItem animal={mom} key={mom?.id??mom?.animalId} />
            ))}
          </div>
        </>
      )}
            {father.length > 0 && (
        <>
          <Box sx={{ textAlign: 'center', fontWeight: 600 }}>
            <Typography variant="h5" paragraph>
            Father:
            </Typography>
          </Box>
          <div className="animal-list">
            {father.map((dad) => (
              <AnimalListItem animal={dad} key={dad?.id??dad?.animalId} />
            ))}
          </div>
        </>
      )}
           {siblings.length > 0 && (
        <>
          <Box sx={{ textAlign: 'center', fontWeight: 600 }}>
            <Typography variant="h5" paragraph>
            Siblings:
            </Typography>
          </Box>
          <div className="animal-list">
            {siblings.map((sib) => (
              <AnimalListItem animal={sib} key={sib?.id??sib?.animalId} />
            ))}
          </div>
        </>
      )}
      {children.length > 0 && (
        <>
          <Box sx={{ textAlign: 'center', fontWeight: 600 }}>
            <Typography variant="h5" paragraph>
            Children:
            </Typography>
          </Box>
          <div className="animal-list">
            {children.map((child) => (
              <AnimalListItem animal={child} key={child?.id??child?.animalId} />
            ))}
          </div>
        </>
      )}
            
        </div>
    )
}


const mapStateToProps = createStructuredSelector({
    animal: selectCurrentSelectedAnimal,
    mother: selectCurrentSelectedAnimalMotherAsArray,
    father: selectCurrentSelectedAnimalFatherAsArray,
    siblings: selectCurrentSelectedAnimalSiblingsAsArray,
    children: selectCurrentSelectedAnimalChildrenAsArray
  });
  
  export default connect(mapStateToProps)(withRouter(AnimalFamily));
  
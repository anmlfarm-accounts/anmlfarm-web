import React, { useState } from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { Formik, Form } from 'formik';
import * as Yup from 'yup';
import {
  addAnimalWeightStart,
  deleteLatestAnimalWeightStart,
} from '../../../redux/animals/animal.actions';
import {
  selectCurrentSelectedAnimal,
  selectCurrentSelectedAnimalWeightsAsArray,
  selectUserCanModifyAnimal,
} from '../../../redux/animals/animal.selectors';
import TextField from '../../shared/formik-form/text-field/text-field';
import SubmitButton from '../../shared/formik-form/submit-button/ButtonSubmit';
import CheckboxField from '../../shared/formik-form/checkbox-field/checkbox-field';
import DateTimeField from '../../shared/formik-form/datetime-field/datetime-field.component';
import {
  GridPadding20,
  ColumnFlexEnd,
  TextCenter,
} from '../../../styles/shared/shared.styles';
import {
  Typography,
  Checkbox,
  FormControlLabel,
  Box,
  Button,
  IconButton,
} from '@material-ui/core';
import ChartLine from '../../minimal-ui/charts/ChartLine';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import { fDate, fDateYear } from '../../minimal-ui/utils/formatTime';
import { calculateTimeDifference } from '../../../utils/date/calculations';
import AlertFeedback from '../../shared/alert/alert.component';
import AnimalUndoPopup from '../animal-undo-popup/animal-undo-popup.component';
import SetReminder from '../../reminders/set-reminder/set-reminder.component';
import { generateAnimalWeightReminder } from '../../../redux/reminders/reminder.selectors';

const FORM_VALIDATION = Yup.object().shape({
  weight: Yup.number()
    .typeError('Must be a number')
    .required('Weight is Required'),
});

const INITIAL_FORM_STATE = {
  weight: '',
  weightDate: fDateYear(new Date()),
};

const AnimalWeight = ({
  addWeight,
  animalDetails,
  animalWeights,
  animalAuth,
  deleteLatestWeight,
  generateWeightReminder,
}) => {
  const [isChecked, setIsChecked] = useState(true);
  const [calender, setCalender] = useState(false);
  const [menu, setMenu] = useState(false);
  const [openUndo, setOpenUndo] = useState(false);

  const deleteAnimalLatestWeight = () => {
    deleteLatestWeight(animalDetails);
    setOpenUndo(false);
  };

  const addAnimalWeight = (values, props) => {
    addWeight(animalDetails, {
      ...values,
      weightDate: fDate(values?.weightDate),
      dateTime: new Date(),
    });
    props.resetForm();
  };

  const weightDates = animalWeights.map((weight) => weight.weightDate);
  const weightDatesConverted = animalWeights.map((weight) =>
    calculateTimeDifference(weight.weightDate, animalDetails.animalBirthday),
  );
  const weightValues = animalWeights.map((weight) => weight?.weight);

  return (
    <div>
      <div className="animal-weights">
        {animalWeights.length > 1 ? (
          <>
            <div>
              {!animalDetails?.animalEnd && (
                <ColumnFlexEnd align="right">
                  <IconButton onClick={() => setMenu(!menu)}>
                    <MoreVertIcon />
                  </IconButton>
                  {menu && animalAuth && (
                    <Button onClick={() => setOpenUndo(true)}>
                      Undo last Weight
                    </Button>
                  )}
                </ColumnFlexEnd>
              )}
            </div>
            <ChartLine
              title="Animal Weights"
              values={weightValues}
              dates={
                animalDetails.animalBirthday
                  ? weightDatesConverted
                  : weightDates
              }
            />
          </>
        ) : animalWeights.length === 1 ? (
          <TextCenter>
            Weighs {weightValues[0]}KG. Last updated {weightDates[0]}
          </TextCenter>
        ) : (
          <GridPadding20>
            <TextCenter>
              <Typography variant="h6" paragraph>
                No weight to show
              </Typography>
            </TextCenter>
          </GridPadding20>
        )}
      </div>
      <AnimalUndoPopup
        record="weight"
        deleteRecord={deleteAnimalLatestWeight}
        popupOpen={openUndo}
        cancel={() => setOpenUndo(false)}
        onClick={() => setOpenUndo(false)}
      />
      {animalAuth && !animalDetails?.animalEnd && (
        <Box sx={{ maxWidth: 480, margin: 'auto' }}>
          <Formik
            initialValues={{
              ...INITIAL_FORM_STATE,
            }}
            validationSchema={FORM_VALIDATION}
            onSubmit={addAnimalWeight}
          >
            <Form>
              <Box m={1}>
                <Box py={1}>
                  <TextField label="Weight (kg)" name="weight" type="number" />
                </Box>
                <Box py={1}>
                  <CheckboxField
                    label="Today"
                    onChange={(e) => setIsChecked(e.currentTarget.checked)}
                    checked={isChecked}
                    onClick={() => setCalender(!calender)}
                  />
                </Box>
                {calender && (
                  <Box py={1}>
                    <DateTimeField label="Date" name="weightDate" />
                  </Box>
                )}
                <Box py={1}>
                  <SubmitButton type="submit">Add Weight</SubmitButton>
                </Box>
              </Box>
            </Form>
          </Formik>
          <SetReminder reminderDetails={generateWeightReminder} />
        </Box>
      )}
    </div>
  );
};

const mapStateToProps = createStructuredSelector({
  animalDetails: selectCurrentSelectedAnimal,
  animalWeights: selectCurrentSelectedAnimalWeightsAsArray,
  animalAuth: selectUserCanModifyAnimal,
  generateWeightReminder: generateAnimalWeightReminder,
});

const mapDispatchToProps = (dispatch) => ({
  addWeight: (animalInfo, weightInfo) =>
    dispatch(addAnimalWeightStart(animalInfo, weightInfo)),
  deleteLatestWeight: (animalInfo) =>
    dispatch(deleteLatestAnimalWeightStart(animalInfo)),
});

export default connect(mapStateToProps, mapDispatchToProps)(AnimalWeight);

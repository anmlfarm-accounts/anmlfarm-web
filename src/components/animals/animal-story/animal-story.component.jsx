import React, { useState } from 'react';
import { Box, Button, IconButton, Typography } from '@material-ui/core';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { Formik, Form } from 'formik';
import * as Yup from 'yup';
import { endAnimalStart } from '../../../redux/animals/animal.actions';
import {
  selectCurrentSelectedAnimal,
  selectUserCanModifyAnimal,
} from '../../../redux/animals/animal.selectors';
import { fDateYear } from '../../minimal-ui/utils/formatTime';
import SubmitButton from '../../shared/formik-form/submit-button/ButtonSubmit';
import CheckboxField from '../../shared/formik-form/checkbox-field/checkbox-field';
import DateTimeField from '../../shared/formik-form/datetime-field/datetime-field.component';
import { ColumnFlexEnd, Column } from '../../../styles/shared/shared.styles';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import SelectField from '../../shared/formik-form/select-field/select-field.component';
import { StoryBlock } from '../../../styles/info.pages/info.page.styles';
import TextInputfield from '../../shared/formik-form/text-field/text-field';

const AnimalStory = ({ animalDetails, endAnimal, animalAuth }) => {
  const INITIAL_FORM_STATE = {
    reason: '',
    weightBefore: '',
    weightAfter: '',
    grading: '',
    price: '',
    cost: '',
    location: '',
    cause: '',
    email: '',
    date: fDateYear(new Date()),
  };

  const [isChecked, setIsChecked] = useState(true);
  const [calender, setCalender] = useState(false);
  const [menu, setMenu] = useState(false);
  const [openForm, setOpenForm] = useState(false);
  const [selected, setSelected] = useState('');

  const reasons = ['slaughtered', 'sold', 'death'];

  const initialValidation = {
    weightBefore: Yup.number()
      .typeError('Must be a number')
      .required('Weight Before is Required'),
    reason: Yup.string().required('Reason is Required'),
    cost: Yup.number()
      .typeError('Must be a number')
      .required('Cost is Required'),
  };

  const FORM_VALIDATION = () => {
    debugger;
    if (selected === '') {
      return Yup.object().shape({
        reason: Yup.string().required('Reason is Required'),
      });
    }
    if (selected === 'slaughtered') {
      return Yup.object().shape({
        ...initialValidation,
        weightAfter: Yup.number()
          .typeError('Must be a number')
          .required('Weight After is Required'),
        grading: Yup.string().required('Grading is Required'),
        price: Yup.number()
          .typeError('Must be a number')
          .required('Price is Required'),
      });
    }
    if (selected === 'sold') {
      return Yup.object().shape({
        ...initialValidation,
        grading: Yup.string().required('Grading is Required'),
        price: Yup.number()
          .typeError('Must be a number')
          .required('Price is Required'),
        location: Yup.string().required('Location is Required'),
        email: Yup.string()
          .email('Invalid email.')
          .required('Email is Required'),
      });
    }
    if (selected === 'death') {
      return Yup.object().shape({
        ...initialValidation,
        location: Yup.string().required('Location is Required'),
        cause: Yup.string().required('Cause of death is Required'),
      });
    }
  };

  const discontinueAnimal = (values, props) => {
    endAnimal(animalDetails, values);
    props.resetForm();
  };

  return (
    <div>
      {!animalDetails?.animalEnd && (
        <>
          <Button onClick={() => setOpenForm(!openForm)}>
            Discontinue Animal
          </Button>
          <Box sx={{ maxWidth: 500, margin: 'auto' }}>
            {openForm && (
              <Box>
                <Formik
                  initialValues={{
                    ...INITIAL_FORM_STATE,
                  }}
                  validationSchema={FORM_VALIDATION}
                  onSubmit={discontinueAnimal}
                >
                  {({ values }) => (
                    <Form>
                      <Box m={1}>
                        <Box py={1}>
                          <SelectField
                            name="reason"
                            label="Reason"
                            options={reasons}
                          />
                        </Box>
                        {setSelected(values.reason)}
                        {values.reason.length > 0 && (
                          <>
                            <Box py={1}>
                              <TextInputfield type="number"
                                label="Live Animal Weight (kg)"
                                name="weightBefore"
                              />
                            </Box>
                            {values.reason === 'slaughtered' && (
                              <>
                                <Box py={1}>
                                  <TextInputfield
                                    label="Weight After Slaughter (kg)"
                                    name="weightAfter"
                                  />
                                </Box>
                                <Box py={1}>
                                  <TextInputfield
                                    label="Grading"
                                    name="grading"
                                  />
                                </Box>
                                <Box py={1}>
                                  <TextInputfield
                                    label="Price (R)"
                                    name="price"
                                  />
                                </Box>
                              </>
                            )}
                            {values.reason === 'death' && (
                              <>
                                <Box py={1}>
                                  <TextInputfield
                                    label="Location"
                                    name="location"
                                  />
                                </Box>
                                <Box py={1}>
                                  <TextInputfield
                                    label="Cause of Death"
                                    name="cause"
                                  />
                                </Box>
                              </>
                            )}
                            {values.reason === 'sold' && (
                              <>
                                <Box py={1}>
                                  <TextInputfield
                                    label="Auction Location"
                                    name="location"
                                  />
                                </Box>
                                <Box py={1}>
                                  <TextInputfield label="Recipient Email" name="email" type="email"/>
                                </Box>
                                <Box py={1}>
                                  <TextInputfield
                                    label="Grading"
                                    name="grading"
                                  />
                                </Box>
                                <Box py={1}>
                                  <TextInputfield
                                    label="Price (R)"
                                    name="price"
                                  />
                                </Box>
                              </>
                            )}
                            <Box py={1}>
                              <TextInputfield label="Cost (R)" name="cost" />
                            </Box>
                            <Box py={1}>
                              <CheckboxField
                                label="Today"
                                onChange={(e) =>
                                  setIsChecked(e.currentTarget.checked)
                                }
                                checked={isChecked}
                                onClick={() => setCalender(!calender)}
                              />
                            </Box>
                            {calender && (
                              <Box py={1}>
                                <DateTimeField label="Date" name="date" />
                              </Box>
                            )}
                          </>
                        )}
                        <Box py={1}>
                          <SubmitButton type="submit">
                            Discontinue Animal
                          </SubmitButton>
                        </Box>
                      </Box>
                    </Form>
                  )}
                </Formik>
              </Box>
            )}
          </Box>
        </>
      )}
      <StoryBlock>
        <Box sx={{ maxWidth: 480, margin: 'auto', textAlign: 'center' }}>
          <Typography variant="h5" paragraph>
            Animal Story
          </Typography>
          <Typography>{animalDetails?.animalStory}</Typography>
        </Box>
      </StoryBlock>
    </div>
  );
};

const mapStateToProps = createStructuredSelector({
  animalDetails: selectCurrentSelectedAnimal,
  animalAuth: selectUserCanModifyAnimal,
});

const mapDispatchToProps = (dispatch) => ({
  endAnimal: (animalInfo, endDetails) =>
    dispatch(endAnimalStart(animalInfo, endDetails)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withRouter(AnimalStory));

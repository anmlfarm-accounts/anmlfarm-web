import React from 'react';
import { Avatar } from '@material-ui/core';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import './animal-header.styles.scss';
import { withRouter } from 'react-router';
// import { selectCurrentCamp } from '../../../redux/camps/camp.selectors';
import { selectCurrentSelectedAnimal } from '../../../redux/animals/animal.selectors';

const AnimalHeader = ({ animal, history }) => {
  const redirectOnClick = () => {
    history.push(`/camps/info/${animal.campId}`);
  };
  return (
    <div className="header" onClick={redirectOnClick}>
      <div className="animal-pic">
        {animal ? (
          <Avatar
            className="avatar"
            src={animal.animalImages ? animal.animalImages[animal.animalImages.length-1] : null}
          />
        ) : (
          <Avatar className="avatar">
            {animal?.displayName ? animal?.displayName : '?'}
          </Avatar>
        )}
      </div>
      <div className="welcome-text">
        {animal?.displayName ?? 'No animal Found. Tap To Create a animal.'}
      </div>
    </div>
  );
};
const mapStateToProps = createStructuredSelector({
  // camp: selectCurrentCamp,
  animal: selectCurrentSelectedAnimal,
});

export default connect(mapStateToProps)(withRouter(AnimalHeader));

import React from 'react';
import { withRouter } from 'react-router-dom';
import { DisplayFlex, TextCenter } from '../../../styles/shared/shared.styles';
import {
  Info,
  InfoBold,
  // SmallGray,
} from '../../../styles/info.pages/info.page.styles';
import InfoDescription from '../../shared/info-page/info-description/info-description.component';
import { calculateCurrentAge } from '../../../utils/date/calculations';
import { selectImagesByAnimalId } from '../../../redux/images/image.selectors';
import { connect } from 'react-redux';
import { calculateAnimalDryMassIndex } from '../../../utils/animals/dry-mass-index';

const AnimalDescription = ({ animal, selectAnimalImages }) => {
  return (
    <InfoDescription
      images={animal?.animalImages ?? selectAnimalImages(animal?.id??animal?.animalId)}
    >
      <h1>
        <TextCenter>{animal?.displayName}</TextCenter>
      </h1>
      <DisplayFlex>
        <Info>Tag Number:</Info>
        <InfoBold>{animal?.tagNumber}</InfoBold>
      </DisplayFlex>
      <DisplayFlex>
        <Info>Type:</Info>
        <InfoBold>{animal?.animalType}</InfoBold>
      </DisplayFlex>
      <DisplayFlex>
        <Info>Breed:</Info>
        <InfoBold>{animal?.animalBreed}</InfoBold>
      </DisplayFlex>
      { animal?.animalBirthday && <DisplayFlex>
        <Info>Birthday:</Info>
        <InfoBold>{animal?.animalBirthday}</InfoBold>
      </DisplayFlex>}
      { animal?.animalBirthday && <DisplayFlex>
        <Info>Age:</Info>
        <InfoBold>{calculateCurrentAge(animal?.animalBirthday)}</InfoBold>
      </DisplayFlex> }
      { animal?.animalTeeth && animal?.animalTeeth > 0 && <DisplayFlex>
        <Info>Teeth:</Info>
        <InfoBold>{animal?.animalTeeth}</InfoBold>
      </DisplayFlex> }
      <DisplayFlex>
        <Info>Gender:</Info>
        <InfoBold>{animal?.animalGender}</InfoBold>
      </DisplayFlex>

      { animal?.weightData &&  <DisplayFlex>
        <Info>Consumption:</Info>
        <InfoBold>{calculateAnimalDryMassIndex(animal)} kg/day</InfoBold>
      </DisplayFlex>
        }
      {animal?.animalLotNumber && <DisplayFlex>
        <Info>Lot Number:</Info>
        <InfoBold>{animal?.animalLotNumber}</InfoBold>
      </DisplayFlex> }
      {animal?.studStatus && <DisplayFlex>
        <Info>Stud:</Info>
        <InfoBold>{animal?.studStatus}</InfoBold>
      </DisplayFlex>}
      {/* <SmallGray>Added 5 days ago</SmallGray> */}
    </InfoDescription>
  );
};

const mapStateToProps = state => ({
  selectAnimalImages: (animalId) => selectImagesByAnimalId(animalId)(state)
})

export default connect(mapStateToProps)(withRouter(AnimalDescription));

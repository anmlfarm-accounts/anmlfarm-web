import mapboxgl from 'mapbox-gl';
import { Container } from '@material-ui/core';
import { styled } from '@material-ui/styles';
import React, { Suspense } from 'react'
import { lazy } from 'react';
import { connect } from 'react-redux';
import { mapboxMapConfig } from '../../../firebase/firebase.utils';
import { selectDeviceEvents } from '../../../redux/events/event.selectors';
import { convertDeviceDataToGeoJson } from '../../../utils/map/geojson-converter';
import Loading from '../../shared/loading/loading.component';
// import { Skeleton } from '@material-ui/core/skeleton';

const MapHeatmap = lazy(() => import('../../minimal-ui/map/map-heatmap'));

// eslint-disable-next-line import/no-webpack-loader-syntax, import/no-unresolved
mapboxgl.workerClass = require('worker-loader!mapbox-gl/dist/mapbox-gl-csp-worker').default;

const THEMES = {
  streets: 'mapbox://styles/mapbox/streets-v11',
  outdoors: 'mapbox://styles/mapbox/outdoors-v11',
  light: 'mapbox://styles/mapbox/light-v10',
  dark: 'mapbox://styles/mapbox/dark-v10',
  satellite: 'mapbox://styles/mapbox/satellite-v9',
  satelliteStreets: 'mapbox://styles/mapbox/satellite-streets-v11'
};

const baseSettings = {
    mapboxApiAccessToken: mapboxMapConfig,
    width: '100%',
    height: '100%',
    minZoom: 1
};

const MapWrapperStyle = styled('div')(({ theme }) => ({
    zIndex: 0,
    height: 560,
    overflow: 'hidden',
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    '& .mapboxgl-ctrl-logo, .mapboxgl-ctrl-bottom-right': {
      display: 'none'
    }
  }));

const SkeletonLoad = (
    <>
      <Loading />
    </>
  );

function AnimalMap({device, deviceEvents}) {
    if(!deviceEvents || deviceEvents.length === 0) return null;
    const geoData = convertDeviceDataToGeoJson(deviceEvents);
    return (
        <Container maxWidth="lg">
            <Suspense fallback={SkeletonLoad}>
              <MapWrapperStyle>
                  <MapHeatmap geodata={geoData} {...baseSettings} mapStyle={THEMES.satellite} />
              </MapWrapperStyle>
            </Suspense>
        </Container>
    )
}

const mapStateToProps = (state, otherdata) => ({
    deviceEvents: selectDeviceEvents(otherdata.device)(state)
  });

export default connect(mapStateToProps)(AnimalMap)
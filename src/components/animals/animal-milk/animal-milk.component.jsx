import React, { useState } from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { Formik, Form } from 'formik';
import * as Yup from 'yup';
import {
  addAnimalMilkStart,
  deleteLatestAnimalMilkStart,
} from '../../../redux/animals/animal.actions';
import {
  selectCurrentSelectedAnimal,
  selectCurrentSelectedAnimalMilkAsArray,
  selectUserCanModifyAnimal,
} from '../../../redux/animals/animal.selectors';
import TextField from '../../shared/formik-form/text-field/text-field';
import SubmitButton from '../../shared/formik-form/submit-button/ButtonSubmit';
import CheckboxField from '../../shared/formik-form/checkbox-field/checkbox-field';
import DateTimeField from '../../shared/formik-form/datetime-field/datetime-field.component';
import {
  GridPadding20,
  ColumnFlexEnd,
  TextCenter,
} from '../../../styles/shared/shared.styles';
import { Typography, Button } from '@material-ui/core';
import ChartColumnSingle from '../../minimal-ui/charts/ChartColumnSingle';
import { fDate, fDateYear } from '../../minimal-ui/utils/formatTime';
import { Box, IconButton } from '@material-ui/core';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import AnimalUndoPopup from '../animal-undo-popup/animal-undo-popup.component';

const FORM_VALIDATION = Yup.object().shape({
  milk: Yup.number().typeError('Must be a number').required('Milk is Required'),
});

const INITIAL_FORM_STATE = {
  milk: '',
  milkDate: fDateYear(new Date()),
};

const AnimalMilk = ({
  addMilk,
  animalDetails,
  animalMilk,
  animalAuth,
  deleteLatestMilk,
}) => {
  const [isChecked, setIsChecked] = useState(true);
  const [calender, setCalender] = useState(false);
  const [menu, setMenu] = useState(false);
  const [openUndo, setOpenUndo] = useState(false);

  const deleteAnimalLatestMilk = () => {
    deleteLatestMilk(animalDetails);
    setOpenUndo(false);
  };

  const addAnimalMilk = (values, props) => {
    addMilk(animalDetails, {
      ...values,
      milkDate: fDate(values?.milkDate),
      dateTime: new Date(),
    });
    props.resetForm();
  };

  const milkDates = animalMilk.map((milk) => milk?.milkDate);
  const milkValues = animalMilk.map((milk) => milk?.milk);

  return (
    <div>
      <div>
        {animalMilk.length > 0 ? (
          <>
            <div>
              {!animalDetails?.animalEnd && (
                <ColumnFlexEnd align="right">
                  <IconButton onClick={() => setMenu(!menu)}>
                    <MoreVertIcon />
                  </IconButton>
                  {menu && animalAuth && (
                    <Button onClick={() => setOpenUndo(true)}>
                      Undo last Milk
                    </Button>
                  )}
                </ColumnFlexEnd>
              )}
            </div>
            <ChartColumnSingle
              title="Animal Milk"
              values={milkValues}
              dates={milkDates}
            />
          </>
        ) : (
          <GridPadding20>
            <TextCenter>
              <Typography variant="h6" paragraph>
                No Milk to show
              </Typography>
            </TextCenter>
          </GridPadding20>
        )}
      </div>
      <AnimalUndoPopup
        record="milk"
        deleteRecord={deleteAnimalLatestMilk}
        popupOpen={openUndo}
        cancel={() => setOpenUndo(false)}
        onClick={() => setOpenUndo(false)}
      />
      {animalAuth && !animalDetails?.animalEnd && (
        <Box sx={{ maxWidth: 480, margin: 'auto' }}>
          <Formik
            initialValues={{
              ...INITIAL_FORM_STATE,
            }}
            validationSchema={FORM_VALIDATION}
            onSubmit={addAnimalMilk}
          >
            <Form>
              <Box m={1}>
                <Box py={1}>
                  <TextField label="Milk (ml)" name="milk" type="number" />
                </Box>
                <Box py={1}>
                  <CheckboxField
                    label="Today"
                    onChange={(e) => setIsChecked(e.currentTarget.checked)}
                    checked={isChecked}
                    onClick={() => setCalender(!calender)}
                  />
                </Box>
                {calender && (
                  <Box py={1}>
                    <DateTimeField label="Date" name="milkDate" />
                  </Box>
                )}
                <Box py={1}>
                  <SubmitButton type="submit">Add Milk</SubmitButton>
                </Box>
              </Box>
            </Form>
          </Formik>
        </Box>
      )}
    </div>
  );
};

const mapStateToProps = createStructuredSelector({
  animalDetails: selectCurrentSelectedAnimal,
  animalMilk: selectCurrentSelectedAnimalMilkAsArray,
  animalAuth: selectUserCanModifyAnimal,
});

const mapDispatchToProps = (dispatch) => ({
  addMilk: (animalInfo, milkInfo) =>
    dispatch(addAnimalMilkStart(animalInfo, milkInfo)),
  deleteLatestMilk: (animalInfo) =>
    dispatch(deleteLatestAnimalMilkStart(animalInfo)),
});

export default connect(mapStateToProps, mapDispatchToProps)(AnimalMilk);

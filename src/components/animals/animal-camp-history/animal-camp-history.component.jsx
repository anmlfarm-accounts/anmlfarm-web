import React, { useState } from 'react';
import { Box, Button, IconButton, Typography } from '@material-ui/core';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { Formik, Form } from 'formik';
import * as Yup from 'yup';
import {
  transferAnimalCampStart,
  deleteLatestAnimalCampHistoryStart,
} from '../../../redux/animals/animal.actions';
import {
  selectCurrentSelectedAnimal,
  selectCurrentSelectedAnimalCampHistoryAsArray,
  selectUserCanModifyAnimal,
} from '../../../redux/animals/animal.selectors';
import { selectCurrentFarmCamps } from '../../../redux/camps/camp.selectors';
import { fDate, fDateYear } from '../../minimal-ui/utils/formatTime';
import SubmitButton from '../../shared/formik-form/submit-button/ButtonSubmit';
import CheckboxField from '../../shared/formik-form/checkbox-field/checkbox-field';
import DateTimeField from '../../shared/formik-form/datetime-field/datetime-field.component';
import {
  GridPadding20,
  ColumnFlexEnd,
  Column,
  TextCenter,
} from '../../../styles/shared/shared.styles';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import AnimalCampHistoryTable from '../animal-camp-history-table/animal-camp-history-table.component';
import Popup from '../../shared/popup/popup.component';
import AnimalUndoPopup from '../animal-undo-popup/animal-undo-popup.component';
import SelectFieldName from '../../shared/formik-form/select-field-displayname/select-field-displayname.component';

const FORM_VALIDATION = Yup.object().shape({
  camp: Yup.string().required('Camp is Required'),
});

const INITIAL_FORM_STATE = {
  camp: '',
  movedOn: fDateYear(new Date()),
};

const AnimalCampHistory = ({
  camps,
  animalDetails,
  transferCamp,
  animalAuth,
  animalCampHistory,
  deleteLatestCampHistory,
  history,
}) => {
  const [isChecked, setIsChecked] = useState(true);
  const [calender, setCalender] = useState(false);
  const [menu, setMenu] = useState(false);
  const [openUndo, setOpenUndo] = useState(false);
  const [openTransfer, setOpenTransfer] = useState(false);

  const deleteAnimalLatestCampHistory = () => {
    const campId = animalCampHistory[1].campId;
    deleteLatestCampHistory(animalDetails, campId);
    history.push(`/camps/info/${campId}`);
  };

  const transferAnimalCamp = (values) => {
    if (!camps) return;
    const matchingCamps = camps.filter((c) =>
      c.displayName.toLowerCase().includes(values.camp.toLowerCase()),
    );
    if (matchingCamps && matchingCamps.length === 1) {
      const details = {
        displayName: matchingCamps[0].displayName,
        campId: matchingCamps[0].id ?? matchingCamps[0].campId,
        movedOn: fDate(values?.movedOn),
        dateTime: new Date(),
      };
      transferCamp(animalDetails, { ...details });
      history.push(
        `/camps/info/${matchingCamps[0].id ?? matchingCamps[0].campId}`,
      );
    }
  };

  return (
    <div>
      <div>
        {!animalDetails?.animalEnd && (
          <ColumnFlexEnd>
            <IconButton onClick={() => setMenu(!menu)}>
              <MoreVertIcon />
            </IconButton>
            {menu && animalAuth && (
              <Column>
                {animalCampHistory.length > 1 && (
                  <Button onClick={() => setOpenUndo(true)}>
                    Undo last Transfer
                  </Button>
                )}
                <Button onClick={() => setOpenTransfer(true)}>
                  Transfer Animal Camp
                </Button>
              </Column>
            )}
          </ColumnFlexEnd>
        )}
      </div>
      <AnimalUndoPopup
        record="camp history"
        deleteRecord={deleteAnimalLatestCampHistory}
        popupOpen={openUndo}
        cancel={() => setOpenUndo(false)}
        onClick={() => setOpenUndo(false)}
      />
      {animalAuth && !animalDetails?.animalEnd && (
        <Box sx={{ maxWidth: 500, margin: 'auto' }}>
          <Popup
            title="Transfer Form"
            popupOpen={openTransfer}
            cancel={() => setOpenTransfer(false)}
          >
            <Formik
              initialValues={{
                ...INITIAL_FORM_STATE,
              }}
              validationSchema={FORM_VALIDATION}
              onSubmit={transferAnimalCamp}
            >
              <Form>
                <Box m={1}>
                  <Box py={1}>
                    <SelectFieldName name="camp" label="Camp" options={camps} />
                  </Box>
                  <Box py={1}>
                    <CheckboxField
                      label="Today"
                      onChange={(e) => setIsChecked(e.currentTarget.checked)}
                      checked={isChecked}
                      onClick={() => setCalender(!calender)}
                    />
                  </Box>
                  {calender && (
                    <Box py={1}>
                      <DateTimeField label="Date" name="movedOn" />
                    </Box>
                  )}
                  <Box py={1}>
                    <SubmitButton type="submit">Transfer Animal</SubmitButton>
                  </Box>
                </Box>
              </Form>
            </Formik>
          </Popup>
        </Box>
      )}
      {animalCampHistory.length > 0 ? (
        <AnimalCampHistoryTable />
      ) : (
        <GridPadding20>
          <TextCenter>
            <Typography variant="h6" paragraph>
              No Camp Movements
            </Typography>
          </TextCenter>
        </GridPadding20>
      )}
    </div>
  );
};

const mapStateToProps = createStructuredSelector({
  animalDetails: selectCurrentSelectedAnimal,
  camps: selectCurrentFarmCamps,
  animalAuth: selectUserCanModifyAnimal,
  animalCampHistory: selectCurrentSelectedAnimalCampHistoryAsArray,
});

const mapDispatchToProps = (dispatch) => ({
  transferCamp: (animalDetails, campDetails) =>
    dispatch(transferAnimalCampStart(animalDetails, campDetails)),
  deleteLatestCampHistory: (animalInfo, campId) =>
    dispatch(deleteLatestAnimalCampHistoryStart(animalInfo, campId)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withRouter(AnimalCampHistory));

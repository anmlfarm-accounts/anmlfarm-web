import React, { useState } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { createStructuredSelector } from 'reselect';
import { Formik, Form } from 'formik';
import * as Yup from 'yup';
import {
  deleteAnimalStart,
  updateAnimalStart,
} from '../../../redux/animals/animal.actions';
import { selectCurrentCamp } from '../../../redux/camps/camp.selectors';
import { selectCurrentFarm } from '../../../redux/farms/farm.selectors';
import { selectCurrentUser } from '../../../redux/users/user.selectors';
import TextField from '../../shared/formik-form/text-field/text-field';
import SubmitButton from '../../shared/formik-form/submit-button/ButtonSubmit';
import CustomButton from '../../shared/custom-button/custom-button.component';
import UploadOrSnapshot from '../../shared/upload-or-snapshot/upload-or-snapshot.component';
import { selectCurrentSelectedAnimal } from '../../../redux/animals/animal.selectors';
import { Button, Box } from '@material-ui/core';
import { MainContainer } from '../../../styles/info.pages/info.page.styles';
import AnimalHeader from '../animal-header/animal-header.component';
import { FormIntro } from '../../../styles/form-pages/form-pages.styles';
import { selectImagesByAnimalId } from '../../../redux/images/image.selectors';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import DateTimeField from '../../shared/formik-form/datetime-field/datetime-field.component';
import RadioButtonField from '../../shared/formik-form/radio-button/radio-button.component';
import DeletePopup from '../../shared/delete-popup/delete-popup.component';

const FORM_VALIDATION = Yup.object().shape({
  displayName: Yup.string().required('Animal Name is Required'),
  animalBreed: Yup.string().required('Breed is Required'),
  animalType: Yup.string().required('Type is Required'),
  animalTeeth: Yup.number().typeError('Must be a number'),
  tagNumber: Yup.string()
    .matches(/^\S*$/, 'No spaces')
    .required('Tag Number is Required'),
});

const EditAnimal = ({
  user,
  editAnimal,
  history,
  animalDetails,
  deleteAnimal,
}) => {
  const [openUndo, setOpenUndo] = useState(false);
  const [animalImgs, setAnimalImgs] = useState(
    animalDetails?.animalImages ?? [],
  );

  const INITIAL_FORM_STATE = {
    ...animalDetails,
    id: animalDetails?.id ?? animalDetails?.animalId,
  };

  const redirectOnClick = () => {
    history.goBack();
  };

  const removeUploadingImage = (e) => {
    let src = animalDetails.animalImages[e];
    animalDetails.animalImages = animalDetails.animalImages.filter(
      (i) => i !== src,
    );
    setAnimalImgs(animalDetails?.animalImages ?? []);
  };

  const removeAnimal = () => {
    deleteAnimal(animalDetails);
    history.push(`/camps/info/${animalDetails.campId}`);
  };

  const onSubmit = (values) => {
    if (!user) return;
    INITIAL_FORM_STATE?.animalImages
      ? editAnimal({
          ...values,
          tagNumber: values.tagNumber.trim(),
          animalImages: INITIAL_FORM_STATE?.animalImages,
        })
      : editAnimal({
          ...values,
          tagNumber: values.tagNumber.trim(),
        });
    history.push(
      `/animals/info/${animalDetails?.id ?? animalDetails?.animalId}`,
    );
  };

  const handleImageUploaded = (image) => {
    if (!image) return;
    const animalImages = INITIAL_FORM_STATE.animalImages ?? [];
    animalImages.push(image);
  };

  return (
    <>
      <AnimalHeader />
      <MainContainer maxWidth="sm">
        <Box m={3}>
          <Button
            variant="text"
            startIcon={<ArrowBackIcon />}
            onClick={redirectOnClick}
          >
            Go Back
          </Button>
        </Box>
        <FormIntro>
          {/* <FormImage src={editAnimalIcon} /> */}
          <h2>EDIT YOUR ANIMAL!</h2>
          {animalDetails?.animalImages &&
          animalDetails?.animalImages.length > 0 ? (
            <>
              <h3>Uploading Images</h3>
              {animalImgs.map((i, idx) => (
                <div key={idx}>
                  <img
                    width="100em"
                    style={{ padding: '20', margin: '10' }}
                    alt={`animal shot ${idx}`}
                    key={idx}
                    src={i}
                  />
                  <Button
                    onClick={() => {
                      removeUploadingImage(idx);
                    }}
                  >
                    Delete Image
                  </Button>
                </div>
              ))}
            </>
          ) : null}
          <UploadOrSnapshot captureCompleteCallback={handleImageUploaded} />
        </FormIntro>
        <Formik
          initialValues={{
            ...INITIAL_FORM_STATE,
          }}
          validationSchema={FORM_VALIDATION}
          onSubmit={onSubmit}
        >
          {({ values }) => (
            <Form>
              <Box m={1}>
                <Box py={1}>
                  <TextField label="Display Name" name="displayName" />
                </Box>
                <Box py={1}>
                  <TextField
                    label="Tag Number"
                    name="tagNumber"
                    value={values.tagNumber.toUpperCase()}
                  />
                </Box>
                <Box py={1}>
                  <TextField label="Animal Type" name="animalType" />
                </Box>
                <Box py={1}>
                  <TextField label="Animal Breed" name="animalBreed" />
                </Box>
                <Box py={1}>
                  <TextField label="Mother" name="animalMother" />
                </Box>
                <Box py={1}>
                  <TextField label="Father" name="animalFather" />
                </Box>
                <Box py={1}>
                  <DateTimeField
                    label="Animal Birthday"
                    name="animalBirthday"
                  />
                </Box>
                <Box py={1}>
                  <TextField label="Animal Teeth" name="animalTeeth" />
                </Box>
                <Box py={1}>
                  <TextField label="Animal Lot Number" name="animalLotNumber" />
                </Box>
                <Box py={1}>
                  <RadioButtonField
                    label="Animal Gender"
                    name="animalGender"
                    options={['Female', 'Male']}
                  />
                </Box>
                <Box py={1}>
                  <RadioButtonField
                    label="Stud Status"
                    name="studStatus"
                    options={['Verified', 'Unverified']}
                  />
                </Box>
                <Box py={1}>
                  <TextField
                    label="Animal Story"
                    name="animalStory"
                    multiline={true}
                    rows={3}
                  />
                </Box>
                <Box py={1}>
                  <SubmitButton type="submit">Update Animal</SubmitButton>
                </Box>
              </Box>
            </Form>
          )}
        </Formik>
        <CustomButton type="delete" onClick={() => setOpenUndo(true)}>
          DELETE ANIMAL
        </CustomButton>
        <DeletePopup
          record="Animal"
          deleteRecord={removeAnimal}
          popupOpen={openUndo}
          cancel={() => setOpenUndo(false)}
          onClick={() => setOpenUndo(false)}
        />
      </MainContainer>
    </>
  );
};

const mapStateToProps = createStructuredSelector({
  user: selectCurrentUser,
  animalDetails: selectCurrentSelectedAnimal,
  camp: selectCurrentCamp,
  farm: selectCurrentFarm,
  selectAnimalImages: (animalId) => selectImagesByAnimalId(animalId),
});

const mapDispacthToProps = (dispatch) => ({
  editAnimal: (animalDetails) => dispatch(updateAnimalStart(animalDetails)),
  deleteAnimal: (animalDetails) => dispatch(deleteAnimalStart(animalDetails)),
});

export default connect(
  mapStateToProps,
  mapDispacthToProps,
)(withRouter(EditAnimal));

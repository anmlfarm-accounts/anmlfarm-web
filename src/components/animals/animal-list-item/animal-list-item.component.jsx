import { Grid } from '@material-ui/core';
import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { setSelectedAnimal } from '../../../redux/animals/animal.actions';
import { Bold, MainColor } from '../../../styles/shared/shared.styles';
import CardListItem from '../../shared/list-page/list-item';

const AnimalListItem = ({ setAnimal, animal, history }) => {
  const animalClick = () => {
    setAnimal(animal?.id ?? animal?.animalId);
    history.push(`/animals/info/${animal?.id ?? animal?.animalId}`);
  };

  return (
    <>
      <Grid onClick={animalClick}>
        <CardListItem
          image={
            animal?.animalImages
              ? animal?.animalImages[animal?.animalImages.length - 1]
              : null
          }
          name={animal.displayName}
          tagNumber={animal.tagNumber}
          children={
            <>
              <Bold>
                <MainColor>{animal?.animalEnd?.reason ?? ''}</MainColor>
              </Bold>
              <p>Tag Number: {animal?.tagNumber}</p>
              <p>Breed: {animal?.animalBreed}</p>
            </>
          }
        ></CardListItem>
      </Grid>
    </>
  );
};

const mapDispatchToProps = (dispatch) => ({
  setAnimal: (animalId) => dispatch(setSelectedAnimal(animalId)),
});

export default connect(null, mapDispatchToProps)(withRouter(AnimalListItem));

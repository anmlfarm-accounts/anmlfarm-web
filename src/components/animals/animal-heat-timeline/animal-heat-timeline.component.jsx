import React from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { selectCurrentSelectedAnimalHeatAsArray } from '../../../redux/animals/animal.selectors';
import {
  Timeline,
  TimelineItem,
  TimelineContent,
  TimelineSeparator,
  TimelineConnector,
  TimelineOppositeContent,
} from '@material-ui/lab';
import { Grid, Typography } from '@material-ui/core';
import MTimelineDot from '../../minimal-ui/@material-extend/MTimelineDot';

const AnimalHeatTable = ({ animalHeat }) => {
  return (
    <Grid item xs={12}>
      <Timeline align="alternate">
        {animalHeat.map((item) => (
          <TimelineItem key={item.dateTime}>
            <TimelineOppositeContent>
              <Typography sx={{ color: 'text.secondary' }}>
                {item.heatDate}
              </Typography>
            </TimelineOppositeContent>
            <TimelineSeparator>
              <MTimelineDot color="primary" />
              <TimelineConnector />
            </TimelineSeparator>
            <TimelineContent>
              <Typography></Typography>
            </TimelineContent>
          </TimelineItem>
        ))}
      </Timeline>
    </Grid>
  );
};

const mapStateToProps = createStructuredSelector({
  animalHeat: selectCurrentSelectedAnimalHeatAsArray,
});

export default connect(mapStateToProps)(AnimalHeatTable);

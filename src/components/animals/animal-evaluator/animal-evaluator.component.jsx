import React from 'react'

const AnimalEvaluator = ({animal}) => {
    return(
        <div className="animal-evaluator">
            {animal? 
            <span>ANIMAL EVALUATION</span> :
            <span>one hot eval</span> }
        </div>
    )
}

export default AnimalEvaluator;
import React, { useState } from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { Formik, Form } from 'formik';
import * as Yup from 'yup';
import {
  addAnimalMedicationStart,
  deleteLatestAnimalMedicationStart,
} from '../../../redux/animals/animal.actions';
import {
  selectCurrentSelectedAnimal,
  selectCurrentSelectedAnimalMedicationsAsArray,
  selectUserCanModifyAnimal,
} from '../../../redux/animals/animal.selectors';
import TextField from '../../shared/formik-form/text-field/text-field';
import SubmitButton from '../../shared/formik-form/submit-button/ButtonSubmit';
import CheckboxField from '../../shared/formik-form/checkbox-field/checkbox-field';
import DateTimeField from '../../shared/formik-form/datetime-field/datetime-field.component';
import {
  GridPadding20,
  ColumnFlexEnd,
  TextCenter,
} from '../../../styles/shared/shared.styles';
import { Typography, Button } from '@material-ui/core';
import AnimalMedicationTimeline from '../animal-medication-timeline/animal-medication-timeline.component';
import { fDate, fDateYear } from '../../minimal-ui/utils/formatTime';
import { Box, IconButton } from '@material-ui/core';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import AnimalUndoPopup from '../animal-undo-popup/animal-undo-popup.component';

const FORM_VALIDATION = Yup.object().shape({
  medication: Yup.string().required('Medication is Required'),
  dosage: Yup.number()
    .typeError('Must be a number')
    .required('Dosage is Required'),
});

const INITIAL_FORM_STATE = {
  medication: '',
  dosage: '',
  medicationDate: fDateYear(new Date()),
};

const AnimalMedication = ({
  addMedication,
  animalDetails,
  animalMedications,
  animalAuth,
  deleteLatestMedication,
}) => {
  const [calender, setCalender] = useState(false);
  const [menu, setMenu] = useState(false);
  const [openUndo, setOpenUndo] = useState(false);
  const [isChecked, setIsChecked] = useState(true);

  const deleteAnimalLatestMedication = () => {
    deleteLatestMedication(animalDetails);
    setOpenUndo(false);
  };

  const addAnimalMedication = (values, props) => {
    addMedication(animalDetails, {
      ...values,
      dateTime: new Date(),
      medicationDate: fDate(values.medicationDate),
    });
    props.resetForm();
  };

  return (
    <div>
      <div>
        {animalMedications.length > 0 ? (
          <>
            <div>
              {!animalDetails?.animalEnd && (
                <ColumnFlexEnd align="right">
                  <IconButton onClick={() => setMenu(!menu)}>
                    <MoreVertIcon />
                  </IconButton>
                  {menu && animalAuth && (
                    <Button onClick={deleteAnimalLatestMedication}>
                      Undo last Medication
                    </Button>
                  )}
                </ColumnFlexEnd>
              )}
            </div>
            <AnimalMedicationTimeline />
          </>
        ) : (
          <GridPadding20>
            <TextCenter>
              <Typography variant="h6" paragraph>
                No medication to show
              </Typography>
            </TextCenter>
          </GridPadding20>
        )}
      </div>
      <AnimalUndoPopup
        record="weight"
        deleteRecord={deleteAnimalLatestMedication}
        popupOpen={openUndo}
        cancel={() => setOpenUndo(false)}
        onClick={() => setOpenUndo(false)}
      />
      {animalAuth && !animalDetails?.animalEnd && (
        <Box sx={{ maxWidth: 480, margin: 'auto' }}>
          <Formik
            initialValues={{
              ...INITIAL_FORM_STATE,
            }}
            validationSchema={FORM_VALIDATION}
            onSubmit={addAnimalMedication}
          >
            <Form>
              <Box m={1}>
                <Box py={1}>
                  <TextField label="Medication" name="medication" />
                </Box>
                <Box py={1}>
                  <TextField label="Dosage (ml)" name="dosage" />
                </Box>
                <Box py={1}>
                  <CheckboxField
                    label="Today"
                    onChange={(e) => setIsChecked(e.currentTarget.checked)}
                    checked={isChecked}
                    onClick={() => setCalender(!calender)}
                  />
                </Box>
                {calender && (
                  <Box py={1}>
                    <DateTimeField label="Date" name="medicationDate" />
                  </Box>
                )}
                <Box py={1}>
                  <SubmitButton type="submit">Create Medication</SubmitButton>
                </Box>
              </Box>
            </Form>
          </Formik>
        </Box>
      )}
    </div>
  );
};

const mapStateToProps = createStructuredSelector({
  animalDetails: selectCurrentSelectedAnimal,
  animalMedications: selectCurrentSelectedAnimalMedicationsAsArray,
  animalAuth: selectUserCanModifyAnimal,
});

const mapDispatchToProps = (dispatch) => ({
  addMedication: (animalInfo, medicationInfo) =>
    dispatch(addAnimalMedicationStart(animalInfo, medicationInfo)),
  deleteLatestMedication: (animalInfo) =>
    dispatch(deleteLatestAnimalMedicationStart(animalInfo)),
});

export default connect(mapStateToProps, mapDispatchToProps)(AnimalMedication);

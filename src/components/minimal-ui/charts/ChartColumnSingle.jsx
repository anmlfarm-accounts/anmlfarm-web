import { merge } from 'lodash';

import ReactApexChart from 'react-apexcharts';
//
import BaseOptionChart from './BaseOptionChart';

// ----------------------------------------------------------------------

export default function ChartColumnSingle({ title, values, dates }) {
  const CHART_DATA = [{ name: title, data: values }];
  const chartOptions = merge(BaseOptionChart(), {
    plotOptions: { bar: { columnWidth: '14%', borderRadius: 4 } },
    stroke: { show: false },
    xaxis: { categories: dates },
    tooltip: {
      y: {
        formatter(val) {
          return `$ ${val} thousands`;
        },
      },
    },
  });

  return (
    <ReactApexChart
      type="bar"
      series={CHART_DATA}
      options={chartOptions}
      height={320}
    />
  );
}

import { merge } from 'lodash';
import ReactApexChart from 'react-apexcharts';
//
import BaseOptionChart from './BaseOptionChart';

// ----------------------------------------------------------------------

export default function ChartLine({ title, values, dates }) {
  const CHART_DATA = [{ name: title, data: values }];
  const chartOptions = merge(BaseOptionChart(), {
    xaxis: {
      categories: dates,
    },
    tooltip: { x: { show: false }, marker: { show: false } },
  });

  return (
    <ReactApexChart
      type="line"
      series={CHART_DATA}
      options={chartOptions}
      height={320}
    />
  );
}

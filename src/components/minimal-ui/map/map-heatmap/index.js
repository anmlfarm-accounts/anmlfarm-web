import MapGL, { Source, Layer } from 'react-map-gl';
import { useEffect, useState } from 'react';
//
import { MapControlScale, MapControlGeolocate, MapControlNavigation, MapControlFullscreen } from '../controls';
import ControlPanel from './ControlPanel';

// ----------------------------------------------------------------------



const HEATMAP_LAYER = {
  'id': 'heat',
  'type': 'heatmap',
  'source': 'events',
  'maxzoom': 19,
  'paint': {
  // increase weight as diameter breast height increases
    'heatmap-weight': {
      'property': 'dbh',
      'type': 'exponential',
      'stops': [
        [1, 0],
        [62, 1]
      ]
    },
    // increase intensity as zoom level increases
    'heatmap-intensity': {
      'stops': [
        [11, 1],
        [15, 3]
      ]
    },
    // use sequential color palette to use exponentially as the weight increases
    'heatmap-color': [
      'interpolate',
      ['linear'],
      ['heatmap-density'],
      0,
      'rgba(236,222,239,0)',
      0.2,
      'rgb(208,209,230)',
      0.4,
      'rgb(166,189,219)',
      0.6,
      'rgb(103,169,207)',
      0.8,
      'rgb(28,144,153)'
    ],
    // increase radius as zoom increases
    'heatmap-radius': {
      'stops': [
        [11, 15],
        [20, 20]
      ]
    },
    // decrease opacity to transition into the circle layer
    'heatmap-opacity': {
      'default': 1,
      'stops': [
        [14, 1],
        [20, 0]
      ]
    }
  }
}

const CIRCLE_LAYER = {
  'id': 'point',
  'type': 'circle',
  'source': 'events',
  'minzoom': 18,
  'paint': {
  // increase weight as diameter breast height increases
    'circle-radius': {
      'property': 'dbh',
      'type': 'exponential',
      'stops': [
        [{ zoom: 15, value: 1 }, 5],
        [{ zoom: 15, value: 62 }, 10],
        [{ zoom: 22, value: 1 }, 20],
        [{ zoom: 22, value: 62 }, 50]
      ]
      },
      'circle-color': {
        'property': 'dbh',
        'type': 'exponential',
        'stops': [
          [0, 'rgba(236,222,239,0)'],
          [10, 'rgb(236,222,239)'],
          [20, 'rgb(208,209,230)'],
          [30, 'rgb(166,189,219)'],
          [40, 'rgb(103,169,207)'],
          [50, 'rgb(28,144,153)'],
          [60, 'rgb(1,108,89)']
        ]
      },
      'circle-stroke-color': 'white',
      'circle-stroke-width': 1,
      'circle-opacity': {
      'stops': [
        [14, 0],
        [15, 1]
      ]
    }
  }
}

// ----------------------------------------------------------------------

function filterFeaturesByDay(featureCollection, time) {
  if(!time || !featureCollection) return { type: 'FeatureCollection', features: [] };
  const date = time ? new Date(time) : new Date();
  const year = date.getFullYear();
  const month = date.getMonth();
  const day = date.getDate();
  const features = featureCollection.features.filter((feature) => {
    const featureDate = new Date(feature.properties.time);
    return featureDate.getFullYear() === year && featureDate.getMonth() === month && featureDate.getDate() === day;
  });
  return { type: 'FeatureCollection', features };
}

export default function MapHeatmap({ geodata, ...other }) {
  const [allDays, useAllDays] = useState(true);
  const [timeRange, setTimeRange] = useState([0, 0]);
  const [selectedTime, selectTime] = useState(0);
  const [eventData, setEventData] = useState(null);
  const [viewport, setViewport] = useState({
    latitude: 40,
    longitude: -100,
    zoom: 3,
    bearing: 0,
    pitch: 0
  });

  useEffect(() => {
    const { features } = geodata;
    if(!features || features.length === 0) return;
    const endTime = features[0].properties.time;
    const startTime = features[features.length - 1].properties.time;
    setViewport({
      latitude: features[features.length - 1].geometry.coordinates[1],
      longitude: features[features.length - 1].geometry.coordinates[0],
      zoom: 18
    })
    setTimeRange([startTime, endTime]);
    setEventData(geodata);
    selectTime(endTime);
  }, [geodata]);

  const data = allDays ? eventData : filterFeaturesByDay(eventData, selectedTime);
  const dataLast = 
    allDays ? 
    {
      type: 'FeatureCollection', 
      features: [ eventData?.features[eventData?.features.length - 1]]
    } : 
    {
      type: 'FeatureCollection', 
      features: [ filterFeaturesByDay(eventData, selectedTime)?.features[filterFeaturesByDay(eventData, selectedTime)?.features.length - 1]]
    };

  return (
    <>
      <MapGL {...viewport} onViewportChange={setViewport} {...other}>
        <MapControlScale />
        <MapControlNavigation />
        <MapControlFullscreen />
        <MapControlGeolocate />

        {data && (
          <Source type="geojson" data={data}>
            <Layer {...HEATMAP_LAYER} />
          </Source>
        )}
        {dataLast && (
          <Source type="geojson" data={dataLast}>
            <Layer {...CIRCLE_LAYER} />
          </Source>
        )}
      </MapGL>

      <ControlPanel
        startTime={timeRange[0]}
        endTime={timeRange[1]}
        allDays={allDays}
        selectedTime={selectedTime}
        onChangeTime={selectTime}
        onChangeAllDays={useAllDays}
      />
    </>
  );
}

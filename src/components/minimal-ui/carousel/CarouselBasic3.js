import faker from 'faker'
import Slider from 'react-slick';
import PropTypes from 'prop-types';
import { useRef } from 'react';
import { useTheme, styled } from '@material-ui/core/styles';
import { Box } from '@material-ui/core';
import { CarouselControlsPaging2, CarouselControlsArrowsBasic2 } from './controls';

// ----------------------------------------------------------------------

const RootStyle = styled('div')(({ theme }) => ({
  position: 'relative',
  '& .slick-list': {
    boxShadow: theme.customShadows.z16,
    borderRadius: theme.shape.borderRadiusMd
  }
}));

// ----------------------------------------------------------------------

CarouselItem.propTypes = {
  item: PropTypes.object
};

function CarouselItem({ item }) {
  const { image, title } = item;
  return <Box component="img" alt={title} src={image} sx={{width: '100%', height: 400, objectFit: 'cover' }} />;
}

export default function CarouselBasic3({images}) {

  const CAROUSELS = images.map((_, index) => {
    return {
      title: faker.name.title(),
      description: faker.lorem.paragraphs(),
      image: images[index].linkUrl??images[index]
    };
  });

  const theme = useTheme();
  const carouselRef = useRef();

  const settings = {
    speed: 500,
    dots: true,
    arrows: false,
    autoplay: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    rtl: Boolean(theme.direction === 'rtl'),
    ...CarouselControlsPaging2({
      sx: { mt: 3 }
    })
  };

  const handlePrevious = () => {
    carouselRef.current.slickPrev();
  };

  const handleNext = () => {
    carouselRef.current.slickNext();
  };

  return (
    <RootStyle>
      <Slider ref={carouselRef} {...settings}>
        {CAROUSELS.map((item, index) => (
          <CarouselItem key={index} item={item} />
        ))}
      </Slider>
      <CarouselControlsArrowsBasic2 onNext={handleNext} onPrevious={handlePrevious} />
    </RootStyle>
  );
}

import React from 'react';
import { CardFullWidth, PageTitle } from '../../../styles/shared/shared.styles';
import { EventText } from '../../../styles/event.feed/event.feed.styles';
import DOMPurify from "dompurify";
import { Delete } from '@material-ui/icons'

import './notification-item.styles.scss'
import { connect } from 'react-redux';
import { deleteNotificationStart } from '../../../redux/notifications/notification.actions';

const NotificationItem = ({notification, deleteNotification, active}) => {
  const {image, body, title, htmlBody, htmlLink} = notification;

  return (
    <div className={htmlBody || htmlLink ? `iframe`:'card'}>
      {htmlBody && <div className='frame' style={{margin:0, padding: 0}} width="100%" height="100%" dangerouslySetInnerHTML={{ __html: DOMPurify.sanitize(htmlBody) }} />}
      {htmlLink && !htmlBody && <iframe className='frame' id={notification.id ?? notification.notificationId} url={htmlLink??''} src={htmlLink??''}
      title="FRAME"
      width="100%"
      height="100%"
      allowFullScreen = {true}
      sanbox = "allow-forms
      allow-pointer-lock
      allow-popups
      allow-same-origin
      allow-scripts
      allow-top-navigation"
      allow="camera *; microphone *; accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
      frameBorder = {0}
    />}
      {!htmlBody && !htmlLink && <CardFullWidth>
        
        {image && <img src={image} title={title} alt={title} />}
        <PageTitle>{title}</PageTitle>
        <EventText>
          {body}
        </EventText>
      </CardFullWidth>
      }
      <span className='delete' onClick={()=>deleteNotification(notification)}> Clear Notification <Delete   /> </span>
    </div>
  );
};

const mapDispatchToProps = (dispatch) => ({
  deleteNotification: (notification) => dispatch(deleteNotificationStart(notification))
})

export default connect(null, mapDispatchToProps)(NotificationItem);

import React, { useEffect, useState } from 'react';
import NotificationItem from '../notification-item/notification-item.component';
import { Container } from '@material-ui/core';
import { createStructuredSelector } from 'reselect';
import { selectVisibleNotificationsArray } from '../../../redux/notifications/notification.selectors';
import { connect } from 'react-redux';

const NotificationFeed = ({
    visibleNotifications,
    ownedFarmNotifications,
    workingFarmNotifications,
    animalNotifications,
    userNotifications,
    publicNotifications,
}) => {

  const [notifications, setNotifications] = useState([]);

  useEffect(() => {
    if(visibleNotifications){
      let sorted = [...visibleNotifications];
      sorted.sort((a,b) => (a.id ?? a.notificationId) > (b.id ?? b.notificationId) ? 1 : -1);
      setNotifications(sorted);
    }
  }, [visibleNotifications])

  return (
    <Container maxWidth="md">
      <div className="feed">
          <div className="visible-notifications">
            {notifications ? (
            <div className="notification-collection">
                {notifications.map((notification) => (
                <NotificationItem key={notification.notificationId??notification.id} notification={notification} />
                ))}
            </div>
            ) : null}
          </div>
          <div className="user-notifications">
            {userNotifications ? (
            <div className="notification-collection">
                {userNotifications.map((evt) => (
                <NotificationItem key={evt.id} notification={evt} />
                ))}
            </div>
            ) : null}
          </div>
          <div className="public-notifications">
            {publicNotifications ? (
            <div className="notification-collection">
                {publicNotifications.map((evt) => (
                <NotificationItem key={evt.id} notification={evt} />
                ))}
            </div>
            ) : null}
          </div>
          <div className="farm-notifications">
            {ownedFarmNotifications ? (
            <div className="notification-collection">
                {ownedFarmNotifications.map((evt) => (
                <NotificationItem key={evt.id} notification={evt} />
                ))}
            </div>
            ) : null}
            {workingFarmNotifications ? (
            <div className="notification-collection">
                {ownedFarmNotifications.map((evt) => (
                <NotificationItem key={evt.id} notification={evt} />
                ))}
            </div>
            ) : null}
            {animalNotifications ? (
            <div className="notification-collection">
                {animalNotifications.map((evt) => (
                <NotificationItem key={evt.id} notification={evt} />
                ))}
            </div>
            ) : null}
          </div>
      </div>
    </Container>
  );
};

const mapStateToProps = createStructuredSelector({
    visibleNotifications: selectVisibleNotificationsArray
    // ownedFarmNotifications: selectO
    // workingFarmNotifications,
    // followingFarmNotifications,
    // animalNotifications,
    // userNotifications,
    // publicNotifications,
});

export default connect(mapStateToProps)(NotificationFeed);

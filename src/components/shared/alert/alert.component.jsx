import React from 'react';
import Alert from '@material-ui/lab/Alert';
import { Snackbar } from '@material-ui/core';

const AlertFeedback = ({ alert, setAlert }) => {
  const handleClose = () => {
    setAlert({ ...alert, isOpen: false });
  };
  return (
    <Snackbar
      open={alert.isOpen}
      autoHideDuration={3000}
      anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
      onClose={handleClose}
    >
      <Alert severity={alert.type} onClose={handleClose}>
        {alert.message}
      </Alert>
    </Snackbar>
  );
};

export default AlertFeedback;

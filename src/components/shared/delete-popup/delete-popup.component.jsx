import React from 'react';
import { GriDisplayFlex } from '../../../styles/shared/shared.styles';
import { Box, Button, Grid } from '@material-ui/core';
import Popup from '../../shared/popup/popup.component';
import { SmallGray } from '../../../styles/info.pages/info.page.styles';

const DeletePopup = ({ record, deleteRecord, popupOpen, cancel }) => {
  return (
    <div>
      <Box sx={{ maxWidth: 500, margin: 'auto' }}>
        <Popup title={`Delete ${record}`} popupOpen={popupOpen} cancel={cancel}>
          <>
            <Box my={2}>
              <SmallGray>
                Are you sure you want to delete the {record}?
              </SmallGray>
            </Box>
            <GriDisplayFlex container spacing={2}>
              <Grid item xs={6}>
                <Button
                  fullWidth
                  variant="contained"
                  color="default"
                  onClick={deleteRecord}
                >
                  Confirm
                </Button>
              </Grid>
              <Grid item xs={6}>
                <Button
                  fullWidth
                  variant="outlined"
                  color="default"
                  onClick={cancel}
                >
                  Cancel
                </Button>
              </Grid>
            </GriDisplayFlex>
          </>
        </Popup>
      </Box>
    </div>
  );
};

export default DeletePopup;

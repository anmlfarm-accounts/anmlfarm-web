import React from 'react';
import { useCallback, useState, useEffect, useContext } from 'react';
import CastContext from '../../../utils/cast/cast.context';
import { Cast } from '@material-ui/icons'

const ChromecastButton =({images})=> {
  const { connected, remotePlayerController } = useContext(CastContext);
  const [isMediaLoaded, setIsMediaLoaded] = useState(false);
  const [title, setTitle] = useState('No title');
  const [thumbnail, setThumbnail] = useState('');
  // const [playerState, setPlayerState] = useState('');
  const [playerUrl, setPlayerUrl] = useState('');

  function resetValues() {
    setIsMediaLoaded(false);
    setThumbnail('');
    setTitle('No title');
    // setPlayerState('');
    setPlayerUrl(window?.location?.href?.toString());
  }

  useEffect(() => {
    if (!connected) {
      resetValues();
    }
  }, [connected]);

  useEffect(() => {
    function onMediaLoadedChange(data) {
      setIsMediaLoaded(data.value);
    }
    if (remotePlayerController) {
      remotePlayerController.addEventListener(
        window.cast.framework.RemotePlayerEventType.IS_MEDIA_LOADED_CHANGED,
        onMediaLoadedChange
      );
    }
    return () => {
      if (remotePlayerController) {
        remotePlayerController.removeEventListener(
          window.cast.framework.RemotePlayerEventType.IS_MEDIA_LOADED_CHANGED,
          onMediaLoadedChange
        );
      }
    };
  }, [remotePlayerController, setIsMediaLoaded]);

  useEffect(() => {
    function onMediaInfoChanged(data) {
      // We make the check what we update so we dont update on every player changed event since it happens often
      const newTitle = 'title';
      const newThumbnail = 'th';
      const newUrl = window?.location?.href?.toString();
      if (playerUrl !== newUrl) {
        setPlayerUrl(newUrl);
      }
      if (title !== newTitle) {
        setTitle(newTitle);
      }
      if (thumbnail !== newThumbnail) {
        setThumbnail(newThumbnail);
      }
    }

    if (remotePlayerController) {
      remotePlayerController.addEventListener(
        window.cast.framework.RemotePlayerEventType.MEDIA_INFO_CHANGED,
        onMediaInfoChanged
      );
    }
    return () => {
      if (remotePlayerController) {
        remotePlayerController.removeEventListener(
          window.cast.framework.RemotePlayerEventType.MEDIA_INFO_CHANGED,
          onMediaInfoChanged
        );
      }
    };
  }, [remotePlayerController, setTitle, title, setPlayerUrl, playerUrl, thumbnail, setThumbnail]);
  
  const loadMedia = useCallback(() => {
      const image = images ? images[images.length] : 'https://media.istockphoto.com/vectors/error-page-dead-emoji-illustration-vector-id1095047472?k=6&m=1095047472&s=612x612&w=0&h=3pmxoo0x2rRQXv7z_3Ijm_tsMXMkJfImBdBsXmCJ_tQ=';
      const mimetype = images ? image.match(/[^:]\w+\/[\w-+\d.]+(?=;|,)/)[0] : 'images/jpeg';
    let mediaInfo = new window.chrome.cast.media.MediaInfo(
      image,
      mimetype
    );

    let request = new window.chrome.cast.media.LoadRequest(mediaInfo);

    const castSession = window.cast.framework.CastContext.getInstance().getCurrentSession();
    if (castSession || isMediaLoaded) {
      setIsMediaLoaded(true);
      return castSession.loadMedia(request);
    } else {
      window.cast.framework.CastContext.getInstance().requestSession().then((dat)=>{
        window.cast.framework.CastContext.getInstance().getCurrentSession().loadMedia(request);
        setIsMediaLoaded(true);
      }, (err) =>{
        alert("Could not create cast session");
        if(err)
        return Promise.reject('No CastSession has been created', err);
      });
    }
  },[images, isMediaLoaded, setIsMediaLoaded]);

    if(!window?.cast?.framework?.CastContext?.getInstance()) return null;

    return (
      <Cast fontSize="large" onClick={()=>loadMedia(window?.location?.href?.toString())}/>
    )
}

export default ChromecastButton;
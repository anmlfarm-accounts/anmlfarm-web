import React from 'react';
import './menu-drawer.styles.scss'

import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import DrawerListItem from '../drawer-list-item/drawer-list-item.component'

import home from './images/home.png'
import library from './images/library.png'
import profile from './images/profile.png'
import animal from './images/animal.png'
import cart from './images/cart.png'
import SwipeUpButton from '../swipe-up-button/swipe-up-button.component';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { selectCurrentUser } from '../../../redux/users/user.selectors';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';

class MenuDrawer extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            drawerOpen: false,
            listItems: [
                {
                    id: 1,
                    text: 'Home', 
                    linkToLocal: `/home`, 
                    iconImageUrl: home
                },
                {
                    id: 2,
                    text: 'Library', 
                    linkToLocal: `/library`, 
                    iconImageUrl: library
                },
                {
                    id: 3,
                    text: 'My Farms', 
                    linkToLocal: `/farms/`, 
                    iconImageUrl: animal
                },
                {
                    id: 4,
                    text: 'Market', 
                    linkToLocal: '/products', 
                    iconImageUrl: cart
                },
                {
                    id: 5,
                    text: 'My Profile', 
                    linkToLocal: '/profile/info', 
                    iconImageUrl: profile
                }
            ]
        }
    }

    toggleDrawer = (state) => (event) => {
        if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
          return;
        }
        if(state){
            this.setState({drawerOpen:state})
        }
    };


    render(){
        return(
            <div className='menu-drawer'>
                <div className='drawer-button'>
                    <SwipeUpButton callback={this.toggleDrawer(true)}/>
                </div>
                    <Drawer anchor='bottom' open={this.state.drawerOpen} onClose={()=>this.setState({drawerOpen:false})}>
                        <div
                            className='drawer-list'
                            role="presentation"
                            style={{
                                width: 'auto'
                            }}
                        >
                            <ClickAwayListener onClickAway={()=>this.setState({drawerOpen:false})}>
                                <List>
                                    {this.state.listItems && this.props.user ? this.state.listItems.map(({id, ...otherInfo}) => (
                                        <DrawerListItem key={id} id={id} {...otherInfo} />
                                    )): <DrawerListItem text={'Sign In'} iconImageUrl={profile} linkToLocal={'/profile/signin'}/>}
                                </List>
                            </ClickAwayListener>
                        </div>
                    </Drawer>
            </div>
        )
    }
}

const mapStateToProps = createStructuredSelector({
    user: selectCurrentUser
})

export default connect(mapStateToProps)(MenuDrawer);
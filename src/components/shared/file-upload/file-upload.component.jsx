import {
  Card,
  CardContent,
  CardHeader,
  FormControlLabel,
  Switch,
} from '@material-ui/core';
import React, { useCallback, useState } from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { startDocumentUpload } from '../../../redux/documents/document.actions';
import {
  selectCurrentAnimalDocuments,
  selectCurrentFarmDocuments,
  selectUploadingAnimalDocuments,
  selectUploadingFarmDocuments,
} from '../../../redux/documents/document.selectors';

import {
  selectUserCanModifyAnimal
} from '../../../redux/animals/animal.selectors';
import {
  selectUserCanModifyFarm,
} from '../../../redux/farms/farm.selectors';
import UploadMultiFile from '../../minimal-ui/upload/UploadMultiFile';

function FileUpload({
  forAnimal,
  forFarm,
  forUser,
  animalAuthorized,
  farmAuthorized,
  uploadDocument,
  animalDocuments,
  farmDocuments,
  animalUploadingDocuments,
  farmUploadingDocuments,
}) {
  const [preview, setPreview] = useState(false);
  const [files, setFiles] = useState([]);

  const authorized =
    (forAnimal && animalAuthorized) || (forFarm && farmAuthorized);

  const existingFiles = () => {
    if (forAnimal) return animalDocuments;
    if (forFarm) return farmDocuments;
  };

  const uploadingFiles = () => {
    if (forAnimal) return animalUploadingDocuments;
    if (forFarm) return farmUploadingDocuments;
  };

  const toBase64 = (file) =>
    new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = (error) => reject(error);
    });

  const handleDocumentUpload = async (evt) => {
    files.forEach(async (f) => {
      let b64file = await toBase64(f);
      let docObj = { data: b64file, ...f };
      docObj.name = f.name;
      docObj.path = f.path;
      docObj.size = f.size;
      docObj.lastModified = f.lastModified;
      docObj.lastModifiedDate = f.lastModifiedDate;
      if (forAnimal) docObj.animalIds = [forAnimal?.id ?? forAnimal?.animalId];
      if (forFarm) docObj.farmIds = [forFarm?.id ?? forFarm?.farmId];
      if (forUser) docObj.userIds = [forUser?.id ?? forUser?.uid];
      uploadDocument(docObj);
      handleRemove(f);
      handleRemove(docObj);
    });
  };

  const handleDropMultiFile = useCallback(
    (acceptedFiles) => {
      setFiles(
        acceptedFiles.map((file) =>
          Object.assign(file, {
            preview: URL.createObjectURL(file),
          }),
        ),
      );
    },
    [setFiles],
  );

  const handleRemoveAll = () => {
    setFiles([]);
  };

  const handleRemove = (file) => {
    const filteredItems = files.filter(
      (_file) => _file.preview !== file.preview,
    );
    setFiles(filteredItems);
  };

  return (
    <Card elevation={0}>
      <CardHeader
        title="Upload Files"
        action={
          <FormControlLabel
            control={
              <Switch
                checked={preview}
                onChange={(event) => setPreview(event.target.checked)}
              />
            }
            label="Show Preview"
          />
        }
      />
      <CardContent>
        <UploadMultiFile
          showPreview={preview}
          files={files}
          existingFiles={existingFiles()}
          uploadingFiles={uploadingFiles()}
          authorized={authorized}
          onDrop={handleDropMultiFile}
          onRemove={handleRemove}
          onRemoveAll={handleRemoveAll}
          onFileUpload={handleDocumentUpload}
        />
      </CardContent>
    </Card>
  );
}

const mapStateToProps = createStructuredSelector({
  animalDocuments: selectCurrentAnimalDocuments,
  animalUploadingDocuments: selectUploadingAnimalDocuments,
  animalAuthorized: selectUserCanModifyAnimal,
  farmDocuments: selectCurrentFarmDocuments,
  farmUploadingDocuments: selectUploadingFarmDocuments,
  farmAuthorized: selectUserCanModifyFarm,
});

const mapDispatchToProps = (dispatch) => ({
  uploadDocument: (payload) => dispatch(startDocumentUpload(payload)),
});

export default connect(mapStateToProps, mapDispatchToProps)(FileUpload);

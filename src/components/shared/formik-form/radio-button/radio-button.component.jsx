import React from 'react';
import FormControl from '@material-ui/core/FormControl';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import { useField } from 'formik';
import { GriDisplayFlex } from '../../../../styles/shared/shared.styles';

const RadioButtonField = ({ name, options, ...otherProps }) => {
  const [field, meta] = useField(name);

  const configButtonField = {
    ...field,
    ...otherProps,
  };

  const configFormControl = {};
  if (meta && meta.touched && meta.error) {
    configFormControl.error = true;
  }
  return (
    <FormControl {...configFormControl}>
      <RadioGroup {...configButtonField}>
        <GriDisplayFlex>
          {options.map((option) => (
            <FormControlLabel
              key={option}
              value={option}
              control={<Radio />}
              label={option}
            />
          ))}
        </GriDisplayFlex>
      </RadioGroup>
    </FormControl>
  );
};

export default RadioButtonField;

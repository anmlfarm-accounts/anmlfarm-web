import React from 'react';
import { CardContent } from '@material-ui/core';
import {
  Margin10TopBottom,
  Bold,
} from '../../../../styles/shared/shared.styles';
import { GridColumnCenter } from '../../../../styles/info.pages/info.page.styles';

const InfoBlock = ({ infoTitle, infoDescription }) => {
  return (
    // <Grid container>
    <GridColumnCenter>
      <CardContent>
        <Bold as="h2">{infoTitle}</Bold>
        <Margin10TopBottom>{infoDescription}</Margin10TopBottom>
      </CardContent>
    </GridColumnCenter>
    // </Grid>
  );
};

export default InfoBlock;

import React from 'react';
import { CardContent, Grid } from '@material-ui/core';
import {
  GridColumnCenter,
  Media,
} from '../../../../styles/info.pages/info.page.styles';
import CarouselBasic3 from '../../../minimal-ui/carousel/CarouselBasic3';

const InfoDescription = ({ images, children }) => {
  return (
    <Grid container>
      <GridColumnCenter item xs={12} sm={6}>
        {images && (images.length> 1) &&
        <CarouselBasic3 images={images} />}
        {images && (images.length === 1) && images[0]?.length > 0 &&
        <Media image={ images[0]} title="Image" />}
      </GridColumnCenter>
      <Grid item xs={12} sm={6}>
        <CardContent>{children}</CardContent>
      </Grid>
    </Grid>
  );
};

export default InfoDescription;

import React from 'react';
import { AccountTree } from '@material-ui/icons';
import { Link } from 'react-router-dom';
import {
  GridPadding20,
  GriDisplayFlexCenter,
} from '../../../../styles/shared/shared.styles';
import {
  GridShadow,
  LinkName,
  Number,
} from '../../../../styles/info.pages/info.page.styles';

const InfoLinkItem = ({ name, number, link }) => {
  return (
    <GridPadding20 item xs={12} md={6}>
      <GridShadow container>
        <GriDisplayFlexCenter container>
          <Link to={link}>
            <GriDisplayFlexCenter>
              <AccountTree fontSize="large" color="primary" />
              <LinkName>{name}</LinkName>
              <Number>{number}</Number>
            </GriDisplayFlexCenter>
          </Link>
        </GriDisplayFlexCenter>
      </GridShadow>
    </GridPadding20>
  );
};

export default InfoLinkItem;

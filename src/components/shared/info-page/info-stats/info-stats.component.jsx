import React from 'react';
import { CardContent, Grid } from '@material-ui/core';
import {
  Margin10TopBottom,
  Bold,
} from '../../../../styles/shared/shared.styles';
import {
  Media,
  GridColumnCenter,
} from '../../../../styles/info.pages/info.page.styles';

const InfoStats = ({ displayName, description, image }) => {
  return (
    <Grid container>
      <GridColumnCenter item xs={12} sm={6}>
        <CardContent>
          <Bold as="h2">{displayName} Statitics</Bold>
          <Margin10TopBottom>{description}</Margin10TopBottom>
        </CardContent>
      </GridColumnCenter>
      <GridColumnCenter item xs={12} sm={6}>
        <Media image={image} title="Farm Image" />
      </GridColumnCenter>
    </Grid>
  );
};

export default InfoStats;

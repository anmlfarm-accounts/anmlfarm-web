import React from 'react'
import NfcIcon from '@material-ui/icons/Nfc';

export default function TagWriter() {

    const writeTag = async () => {
        alert("You can now write the tag");
        if ("NDEFReader" in window) {
            try {
                /*global NDEFReader*/ 
                const ndef = new NDEFReader();
                const currentLocation = window?.location?.href?.toString();
                await ndef.write({
                    url: currentLocation,
                    records: [{ recordType: "url", data: currentLocation }]
                  }); 
                alert("> Message written" + currentLocation);
            } catch (error) {
                alert("Argh! Could not write! " + error);
            }
        }
    }

    return (  
        ("NDEFReader" in window) ? <NfcIcon color="primary" fontSize="large" onClick={writeTag} /> : null
    )
}

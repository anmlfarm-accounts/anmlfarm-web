import React from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import {
  FlexSpaceBetween,
  MainColor,
} from '../../../styles/shared/shared.styles';
import { Button } from '@material-ui/core';

const Popup = ({ popupOpen, title, children, cancel }) => {
  return (
    <div>
      <Dialog
        open={popupOpen}
        // onClose={cancel ?? undefined}
        aria-labelledby="form-dialog-title"
      >
        <FlexSpaceBetween>
          {title ? (
            <DialogTitle id="form-dialog-title">
              <MainColor>{title}</MainColor>
            </DialogTitle>
          ) : null}
          <Button size="small" color="primary" onClick={cancel}>
            X
          </Button>
        </FlexSpaceBetween>
        <DialogContent dividers>{children}</DialogContent>
      </Dialog>
    </div>
  );
};

export default Popup;

import React from 'react';
import { Avatar, Grid } from '@material-ui/core';
import {
  CardItem,
  ListImgWrap,
  ListDescription,
  ListInfo,
  ListNotifications,
  ListImg,
  ListImageDiv,
  ListName,
} from '../../../styles/list.pages/list.page.styles';
import {
  Bold,
  GriDisplayFlex,
  MainColor,
} from '../../../styles/shared/shared.styles';
import NotificationsNoneIcon from '@material-ui/icons/NotificationsNone';

const CardListItem = ({ image, name, title, children }) => {
  return (
    <CardItem>
      <GriDisplayFlex container>
        <ListImgWrap item xs={3} sm={4}>
          <ListImageDiv>
            {image ? (
              <ListImg src={image} alt={`${name}`} />
            ) : (
              <Avatar className="avatar">{name ? name[0] : '?'}</Avatar>
            )}
          </ListImageDiv>
        </ListImgWrap>
        <ListDescription item xs={7} sm={7}>
          <Grid>
            <ListName variant="h4">{name}</ListName>
            <ListInfo>
              <Bold>
                <MainColor>{title}</MainColor>
              </Bold>
              <div>{children}</div>
            </ListInfo>
          </Grid>
        </ListDescription>
        <ListNotifications item xs={2} sm={1}>
          <NotificationsNoneIcon />
        </ListNotifications>
      </GriDisplayFlex>
    </CardItem>
  );
};

export default CardListItem;

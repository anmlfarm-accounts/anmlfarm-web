import React from 'react';
import { CustomButtonDefault } from './custom-button.styles';

const CustomButton = ({ children, isGoogleSignIn, ...otherProps }) => (
  <CustomButtonDefault
    className={`${
      isGoogleSignIn ? 'google-sign-in' : ''
    } custom-button ${children}`}
    {...otherProps}
  >
    {children}
  </CustomButtonDefault>
);

export default CustomButton;

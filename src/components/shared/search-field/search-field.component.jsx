import { TextField } from '@material-ui/core';
import { Autocomplete } from '@material-ui/lab';
import React, { useState } from 'react';

export default function SearchField({
  valueUpdated,
  optionUpdated,
  values,
  label,
}) {
  const [active, setActive] = useState(false);

  const changeHandler = (event, newValue) => {
    if (valueUpdated && event.target) {
      valueUpdated(event.target?.value ?? '');
    }
    if (newValue && optionUpdated) {
      optionUpdated(newValue);
    }
  };

  return (
    <Autocomplete
      onClick={() => setActive(true)}
      fullWidth
      freeSolo
      options={active ? values?.map((option) => option.displayName) : []}
      renderInput={(params) => (
        <TextField
          onClick={() => setActive(true)}
          {...params}
          label={label ?? 'Search'}
        />
      )}
      sx={{ mb: 2 }}
      onChange={changeHandler}
    />
  );
}

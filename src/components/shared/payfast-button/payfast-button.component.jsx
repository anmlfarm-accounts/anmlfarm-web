import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { selectCurrentUser } from '../../../redux/users/user.selectors';
import { calculateOrderTotalItemPrice } from '../../../utils/orders/calculations';
import CustomButton from '../custom-button/custom-button.component';

const TEST_CONFIG = {
  actionUri: "https://sandbox.payfast.co.za​/eng/process",
  merchantId: "10000100",
  merchantKey: "46f0cd694581a",
  amount: 999999,
  itemName: "TEST"
}

const LIVE_CONFIG = {
  actionUri: "https://www.payfast.co.za/eng/process",
  merchantId: "17584058",
  merchantKey: "62kz1dhwi8usy",
  amount: 999,
  itemName: "MAX"
}


function PayFastButton({ order, user }) {

  const [config, setConfig] = useState(LIVE_CONFIG);

  useEffect(() => {
    if(config.amount !== calculateOrderTotalItemPrice(order))
    setConfig({
      ...config,
      itemName: `Order for ${user.displayName}`,
      amount: calculateOrderTotalItemPrice(order)
    })
    console.log(`${window.origin}/products/payment/success/${order.id}`)
  }, [order, setConfig, config, user])
 
  return (
    <form action={config.actionUri} method="post">
        <input type="hidden" name="merchant_id" value={config.merchantId} />
        <input type="hidden" name="merchant_key" value={config.merchantKey} />
        <input type="hidden" name="amount" value={config.amount} />
        <input type="hidden" name="item_name" value={config.itemName}/>
        <input type="hidden" name="email_confirmation" value="1" />
        <input type="hidden" name="confirmation_address" value={user?.email ?? 'accounts@anmlfarm.com'} />
        <input type="hidden" name="return_url" value={`https://www.anmlfarm.com/products/payment/success/${order.id}`} />
        <input type="hidden" name="cancel_url" value={`https://www.anmlfarm.com/products/payment/failure/${order.id}`} />
        <img src="https://www.payfast.co.za/assets/images/payfast_logo_colour.svg" alt="PayFast" />
        <CustomButton type="submit">PAY NOW</CustomButton>
    </form>);
}

const mapStateToProps = createStructuredSelector({
  user: selectCurrentUser
})

export default connect(mapStateToProps)(PayFastButton);
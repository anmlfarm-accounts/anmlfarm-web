import React, { useState } from 'react'
import { useEffect } from 'react'
import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'
import { updateUserAgent, updateUserStart } from '../../../redux/users/user.actions'
import { selectCurrentUser, selectRepCode } from '../../../redux/users/user.selectors'

function ProfileAgent({match, history, user, updateUser, updateRepCode, userRepCode}) {

    const [agentCode, setAgentCode] = useState(null)

    useEffect(() => {
        const repCode = match?.params?.agentCode?.toString().toUpperCase();
        setAgentCode(repCode)
        if(repCode && user) {
            updateUser({...user, repCode})
        }
        if(userRepCode !== repCode)
        updateRepCode(repCode)
    }, [match, updateUser, history, agentCode, updateRepCode, userRepCode, user])

    return (
        <div className='agent' value={userRepCode}></div>
    )
}

const mapStateToProps = createStructuredSelector({
    user: selectCurrentUser,
    userRepCode: selectRepCode
})

const mapDispatchToProps = dispatch => ({
    updateUser: user => dispatch(updateUserStart(user)),
    updateRepCode: agentCode => dispatch(updateUserAgent(agentCode))
})

export default connect(mapStateToProps, mapDispatchToProps)(ProfileAgent)
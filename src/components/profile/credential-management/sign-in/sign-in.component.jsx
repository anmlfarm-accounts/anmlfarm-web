import React, { useEffect } from 'react';
import {
  googleSignInStart,
  emailSignInStart,
  facebookSignInStart,
} from '../../../../redux/users/user.actions';
import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router-dom';
import { Formik, Form } from 'formik';
import * as Yup from 'yup';
import { Box, Container, Grid, Typography, Button } from '@material-ui/core';
import TextField from '../../../shared/formik-form/text-field/text-field';
import SubmitButton from '../../../shared/formik-form/submit-button/ButtonSubmit';
import {
  GriDisplayFlexEnd,
  GridPadding10,
  MainColor,
  Bold,
} from '../../../../styles/shared/shared.styles';
import { Icon } from '@iconify/react';
import googleFill from '@iconify/icons-eva/google-fill';
import facebookFill from '@iconify/icons-eva/facebook-fill';
import Divider from '../../../shared/divider/divider.component';

const FORM_VALIDATION = Yup.object().shape({
  email: Yup.string().email('Invalid email.').required('Required'),
  password: Yup.string().required('Password is Required'),
});

const INITIAL_FORM_STATE = {
  email: '',
  password: '',
};

const SignIn = ({
  currentUser,
  googleSignInStart,
  emailSignInStart,
  facebookSignInStart,
  history,
  error,
}) => {
  useEffect(() => {
    if (currentUser != null) {
      history.push('/');
    }
  }, [currentUser, history]);

  const onSubmit = (values) => {
    emailSignInStart(values);
  };

  return (
    <Box mt={7}>
      <Container maxWidth="sm">
        <GridPadding10>
          <GriDisplayFlexEnd>
            Don’t have an account? &nbsp;
            <Link underline="none" variant="subtitle2" to="/profile/signup">
              <Bold>
                <MainColor>Sign Up</MainColor>
              </Bold>
            </Link>
          </GriDisplayFlexEnd>
        </GridPadding10>
        {error && <span style={{ color: 'red' }}>{error.message}</span>}
        <Box sx={{ flexGrow: 1 }}>
          <Typography variant="h4" gutterBottom>
            Sign in
          </Typography>
        </Box>
        <Box py={2}>
          <Grid container>
            <Grid item xs={6}>
              <Box mr={1}>
                <Button
                  fullWidth
                  size="large"
                  color="inherit"
                  variant="outlined"
                  onClick={googleSignInStart}
                >
                  <Icon icon={googleFill} color="#DF3E30" height={24} />
                </Button>
              </Box>
            </Grid>
            <Grid item xs={6}>
              <Box ml={1}>
                <Button
                  fullWidth
                  size="large"
                  color="inherit"
                  variant="outlined"
                  disabled
                  // onClick={facebookSignInStart}
                >
                  <Icon icon={facebookFill} color="#1877F2" height={24} />
                </Button>
              </Box>
            </Grid>
          </Grid>
        </Box>
        <Divider>OR</Divider>
        <Formik
          initialValues={{
            ...INITIAL_FORM_STATE,
          }}
          validationSchema={FORM_VALIDATION}
          onSubmit={onSubmit}
        >
          <Form>
            <span>Sign in with your email and password</span>
            <Box m={1}>
              <Box py={1}>
                <TextField label="Email" name="email" type="email"/>
              </Box>
              <Box py={1}>
                <TextField label="Password" name="password" type="password" />
              </Box>
              <Box py={1}>
                <SubmitButton type="submit">Sign in</SubmitButton>
              </Box>
            </Box>
          </Form>
        </Formik>
      </Container>
    </Box>
  );
};

const mapStateToProps = (state) => ({
  currentUser: state.user.currentUser,
  error: state.user.error,
});

const mapDispatchToProps = (dispatch) => ({
  googleSignInStart: () => dispatch(googleSignInStart()),
  emailSignInStart: (emailAddressAndPassword) =>
    dispatch(emailSignInStart(emailAddressAndPassword)),
  facebookSignInStart: () => dispatch(facebookSignInStart()),
});

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(SignIn));

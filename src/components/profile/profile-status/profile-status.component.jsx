import React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableRow from '@material-ui/core/TableRow';
import {
  TableItem,
  TableItemStatus,
} from '../../../styles/shared/shared.styles';

const UserStatus = ({ user }) => {
  return (
    <Table>
      <TableBody>
        <TableRow>
          <TableItem align="right">Owned Farms:</TableItem>
          <TableItemStatus align="left">2</TableItemStatus>
        </TableRow>
        <TableRow>
          <TableItem align="right">Occupation:</TableItem>
          <TableItemStatus align="left">{user?.occupation}</TableItemStatus>
        </TableRow>
        
        {user?.repCode &&
        <TableRow>
          <TableItem align="right">Agent:</TableItem>
          <TableItemStatus align="left">{user?.repCode}</TableItemStatus>
        </TableRow>}
      </TableBody>
    </Table>
  );
};

export default UserStatus;

// Scripts for firebase and firebase messaging
importScripts('https://www.gstatic.com/firebasejs/8.2.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/8.2.0/firebase-messaging.js');

// Initialize the Firebase app in the service worker by passing the generated config
const firebaseConfig = {
    apiKey: "AIzaSyDoycCx1GYeeZKvaNFdo1uZOSQPmS5jqvQ",
    authDomain: "anmlfarm-web.firebaseapp.com",
    databaseURL: "https://anmlfarm-web-default-rtdb.europe-west1.firebasedatabase.app",
    projectId: "anmlfarm-web",
    storageBucket: "anmlfarm-web.appspot.com",
    messagingSenderId: "149496621239",
    appId: "1:149496621239:web:354464786c0ed0d8ecc532",
    measurementId: "G-3KFJ1FX256",
  };


const vapidKey = 'BLQV6E7zcuJ7uMAuZne3pbgMlh1gu_e-o0jekSyDme3X6LXGfWbrkP3WyPQ7V1wMwg1U_G9lgpdVUe3uoHlhYbM';
try{
  firebase.initializeApp(firebaseConfig);
  if(firebase.messaging.isSupported()){
    const messaging = firebase.messaging();
    messaging.onBackgroundMessage(function(payload) {  
      const notificationTitle = payload.notification.title;
      const notificationOptions = {
        body: payload.notification.body,
      };
      
      notificationChannel.postMessage(payload);

      self.registration.showNotification(notificationTitle,
        notificationOptions);
      
        self.skipWaiting();
    });
  }

}
catch {
  console.log("COULD NOT LOAD FIREBASE")
}
const notificationChannel = new BroadcastChannel('sw-messages');


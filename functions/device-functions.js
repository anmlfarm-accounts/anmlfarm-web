const functions = require("firebase-functions");
const admin = require('firebase-admin');
const geofire = require('geofire-common');

admin.initializeApp();

const reasons = [
    'UNKNOWN 0', 
    'UNKNOWN 1', 
    'UNKNOWN 3', 
    'UNKNOWN 3', 
    'UNKNOWN 4', 
    'UNKNOWN 5', 
    'UNKNOWN 6', 
    'UNKNOWN 7', 
    'UNKNOWN 8', 
    'UNKNOWN 9', 
    'UNKNOWN 10', 
    'UNKNOWN 11'
]

//Logs device data into firestore
exports.deviceLog = functions.https.onRequest(async (request, response) => {

    try {
        const {
            FW, 
            ICCID, 
            IMEI, 
            ProdId, 
            SerNo, 
            Records
        } = request.body;
    
        let deviceData = {
            serialNumber: SerNo,
            firmwareVersion: FW,
            simId: ICCID,
            imei: IMEI,
            productId: ProdId,
            lastUpdated: new Date(0)
        }
    
        const events = [];
    
        Records.forEach(record => {
            const {DateUTC, Fields, Reason, SeqNo} = record;
            let event = {
                type: 'device-event',
                dateUTC: DateUTC,
                reason: reasons[parseInt(Reason)],
                sequenceNumber: SeqNo
            };
    
    
            Fields.forEach(field => {
                
                const {Alt, GpsStat, Head, Lat, Long, Spd, DIn, GpsUTC} = field;
                if(Alt) event.altitude = Alt;
                if(GpsStat) event.gpsStatus = GpsStat;
                if(Head) event.heading = Head;
                if(Spd) event.speed = Alt;
                if(DIn) event.inputData = DIn.toString(2);
    
                if(Lat && Long){

                    const timestamp = GpsUTC ? new Date(GpsUTC) : new Date(0);

                    const geolocation = {
                        altitude: Alt,
                        latitude: Lat,
                        longitude: Long,
                        hash: geofire.geohashForLocation([Lat, Long]),
                        timestamp
                    }
                    event.geolocation = geolocation;
                    deviceData.geolocation = geolocation;
                    if(timestamp>deviceData.lastUpdated) deviceData.lastUpdated = timestamp;
                }
            });
            
            events.push(event);
        });
    
        const devicesSnapshot = await admin.firestore().collection('devices').where('serialNumber', "==", SerNo).limit(1).get();
        
        let savedDevice = null;
        if(devicesSnapshot.empty){
            const savedDeviceRef = await admin.firestore().collection('devices').add(deviceData);
            savedDevice = await savedDeviceRef.get();
        } else {
            savedDevice = await devicesSnapshot.docs[0].ref.update(deviceData);
            savedDevice = await devicesSnapshot.docs[0].ref.get();
        }
        
        events.forEach(async (event,index) => {
            admin.firestore().collection('events').add({...event, deviceIds: [savedDevice?.id ?? SerNo]});
        })
        
        return response.status(200).send("OK");
    } catch (error) {
        functions.logger.error("ERROR", error)
        return response.status(500).send(error);
    }
 })
const deviceFunctions = require('./device-functions');
const mailFunctions = require('./mail-functions')

exports.deviceLog = deviceFunctions.deviceLog;
exports.demoMail = mailFunctions.sendMail;